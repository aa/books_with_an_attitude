---
title: "The Riddle of The Real City"
---

<div class="year">2017</div>
<div class="link"><a class="link" href="<http://networkcultures.org/blog/publication/the-riddle-of-the-real-city-or-the-dark-knowledge-of-urbanism/>"><http://networkcultures.org/blog/publication/the-riddle-of-the-real-city-or-the-dark-knowledge-of-urbanism/></a></div>
<div class="category">Guests</div>

The Riddle of The Real City {lang=en}
===========================
<div class="summary">
xxxxxxxxxxxxxxxx
</div>

<div class="text">
Does our knowledge about city and urban planning have solid ground? Can historical research promote creative thinking? How can we theorise
about urban design and architecture in our age of the media? These questions have guided the creation of this multi-layered, richly
documented and illustrated triptych, in which the Dutch architectural theorist Wim Nijenhuis pursues a creative goal: to stimulate new ways of
thinking in architectural culture.

The book is composed as a cloud essay that serves to enrich the reader’s theoretical understanding of urban interventions. It is designed by Open Source Publishing using their tool html2print and is available as a printed book as well as an epub. 


**Download the book**: <http://networkcultures.org/wp-content/uploads/2017/06/the-riddle-def.epub>
**Order a printed copy with 1001 publishers**: <https://www.uitgeverij1001.nl/book_44.html>
</div>

The Riddle of The Real City {lang=fr}
===========================
<div class="summary">
Nos connaissances sur les villes et le développement urbain sont-elles fondées? La recherche historique peut-elle promouvoir la réflexion créative? A l'ère des médias, comment pouvons-nous créer des théories autour du développement urbain et de l'architecture? Voici les questions qui ont guidé la création de ce triptyque à multiples facettes, largement documenté et illustré, dans lequel le théoricien néerlandais en architecture Wim Nijenhuis poursuit un but créatif: l'idée de stimuler de nouvelles façons de penser la culture architecturale.
</div>

<div class="text">
Nos connaissances sur les villes et le développement urbain sont-elles fondées? La recherche historique peut-elle promouvoir la réflexion créative? A l'ère des médias, comment pouvons-nous créer des théories autour du développement urbain et de l'architecture? Voici les questions qui ont guidé la création de ce triptyque à multiples facettes, largement documenté et illustré, dans lequel le théoricien néerlandais en architecture Wim Nijenhuis poursuit un but créatif: l'idée de stimuler de nouvelles façons de penser la culture architecturale.
Le livre est composé comme un essai-nuage qui sert à enrichir la compréhension théorique du lecteur autour des interventions urbaines. Il a été mis-en-page par [Open Source Publishing](http://osp.kitchen/) en utilisant leur outil html2print. Il est disponible comme livre imprimé et comme epub. 

**Téléchargez le livre**: <http://networkcultures.org/wp-content/uploads/2017/06/the-riddle-def.epub>
**Commandez un exemplaire chez Uitgeverij 1001**: <https://www.uitgeverij1001.nl/book_44.html>
</div>

The Riddle of The Real City {lang=nl}
===========================
<div class="summary">
Is onze kennis over steden en stadsontwikkeling gegrond? Kan historisch onderzoek aanzetten tot creatief denken? Hoe kunnen we theorieën ontwikkelen over stadsontwerp en architectuur in een digitale context? Dit zijn de vragen die leidden tot de creatie van deze meerlagige, rijk gedocumenteerde en geïllustreerde tryptiek, een creatieve uitdaging voor Nederlands architectuurtheoreticus Wim Nijenhuis, om nieuwe manieren van denken te stimuleren binnen de architecturale cultuur.
</div>

<div class="text">
Is onze kennis over steden en stadsontwikkeling gegrond? Kan historisch onderzoek aanzetten tot creatief denken? Hoe kunnen we theorieën ontwikkelen over stadsontwerp en architectuur in een digitale context? Dit zijn de vragen die leidden tot de creatie van deze meerlagige, rijk gedocumenteerde en geïllustreerde tryptiek, een creatieve uitdaging voor Nederlands architectuurtheoreticus Wim Nijenhuis, om nieuwe manieren van denken te stimuleren binnen de architecturale cultuur. 

Het boek is samengesteld als een 'wolk'-essay om de lezer een beter theoretisch begrip te geven van stadinterventies. Het werd ontworpen door [Open Source Publishing](http://osp.kitchen/) met hun zelfgemaakte tool html2print. Het is beschikbaar als gedrukt exemplaar en als epub. 

**Download het boek**: <http://networkcultures.org/wp-content/uploads/2017/06/the-riddle-def.epub>
**Bestel een gedrukt exemplaar bij Uitgeverij 1001**: <https://www.uitgeverij1001.nl/book_44.html>
</div>
