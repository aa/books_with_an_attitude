---
title: "Interference: Reader"
---

<div class="year">2014</div>
<div class="link"><a class="link" href="https://interference.puscii.nl/program.html">https://interference.puscii.nl/program.html</a></div>
<div class="category">Guests</div>

Interference: Reader {lang=en}
===========================
<div class="summary">
Interference has been a 3-day gathering in Amsterdam bringing together a  broad range of theories and practices that shared a critical approach to society and technology.
As part of the event, a reader was prepared bundling a number of these perspectives as they were contributed by the event's participants.
</div>

<div class="text">
**Read the book online or download as pdf**: <https://interference.puscii.nl/reader.html>

*Interference* has been a 3-day gathering in Amsterdam bringing together a broad range of theories and practices that shared a critical approach to society and technology.
As part of the event, a reader was prepared bundling a number of these perspectives as they were contributed by the event's participants.

The reader has been designed via the CSS print styles applied to the webpage of the event's program, and a collection of workarounds were
used to tweak the limitations of browser interpretation so that the drupal page structure would be printed out in book-like format.
The resulting book has been risographed and velo-bound autonomously during the event, in an edition of 150, and distributed to the
participants to spread the echoes of the discussions that took place during the event.

*Design, Edition, Production*: Interference Crew
*Contributions*: A, N and D Collective, aharon, Janneke Belt and Paulan Korenhof, Critisticuffs, Michael Dizon, Kittens Editorial Collective, Maxigas, Hannes Mehnert, Luis Rodil-Fernández, Jonas Söderberg, XLTerrestrials
*Cover Design*: Anton Stuckardt
</div>

Interference: Reader {lang=fr}
===========================
<div class="summary">
Interference était un rassemblement de 3 jours à Amsterdam réunissant un large éventail de théories et pratiques autour d'une approche critique de la société et de la technologie.
Dans le cadre de l'événement, un lecteur a été préparé regroupant un certain nombre de points de vue émis par les participants de l'événement.
</div>

<div class="text">
**Lisez le livre en ligne ou téléchargez le pdf**: <https://interference.puscii.nl/reader.html>

*Interference* était un rassemblement de 3 jours à Amsterdam réunissant un large éventail de théories et pratiques autour d'une approche critique de la société et de la technologie.
Dans le cadre de l'événement, un lecteur a été préparé regroupant un certain nombre de points de vue émis par les participants de l'événement.

Le lecteur a été conçu via les styles d'impression CSS appliqués à la page Web du programme de l'événement, et une collection de détournements ont été utilisés pour modifier les limites de l'interprétation du navigateur afin que la structure drupale de la page soit imprimée sous forme de livre. Celui-ci a ensuite été risographié et velo-relié de façon autonome au cours de l'événement en une édition de 150 exemplaires, puis distribué aux participants pour diffuser les échos de leurs discussions lors du rassemblement. 

*Design, Edition, Production*: Interference Crew
*Contributions*: A, N and D Collective, aharon, Janneke Belt and Paulan Korenhof, Critisticuffs, Michael Dizon, Kittens Editorial Collective, Maxigas, Hannes Mehnert, Luis Rodil-Fernández, Jonas Söderberg, XLTerrestrials
*Cover Design*: Anton Stuckardt
</div>

Interference: Reader {lang=nl}
===========================
<div class="summary">
*Interference* was een 3-daagse ontmoeting in Amsterdam tussen theorieën en praktijken met een kritische benadering van technologie en maatschappij. Als live-onderdeel van het evenement werd een reader gecreëerd met bijdragen van deelnemers.
</div>

<div class="text">
**Lees het boek online of download als pdf**: <https://interference.puscii.nl/reader.html>

*Interference* was een 3-daagse ontmoeting in Amsterdam tussen theorieën en praktijken met een kritische benadering van technologie en maatschappij. Als live-onderdeel van het evenement werd een reader gecreëerd met bijdragen van deelnemers. 

De reader werd ontworpen met dezelfde CSS-stylesheets die ook voor de website van het evenement werden gebruikt. Via een aantal truuks omzeilden de grafisten de beperkingen van de browserlezing van de data, zodat de pagina-structuur van Drupal ook in boekformaat kan worden afgedrukt.
Het finale boek werd nog tijdens het evenement gedrukt in een oplage van 150 exemplaren. We gebruikten hiervoor een risograaf en velobinding. De reader werd verspreid onder de deelnemers van *Interference*. De ideeën die tijdens discussies werden gedeeld, kregen zo ook een echo na het evenement.

*Lay-out, Redactie, Productie*: Interference Crew
*Bijdragen*: A, N and D Collective, aharon, Janneke Belt and Paulan Korenhof, Critisticuffs, Michael Dizon, Kittens Editorial Collective, Maxigas, Hannes Mehnert, Luis Rodil-Fernández, Jonas Söderberg, XLTerrestrials
*Lay-out Cover*: Anton Stuckardt
</div>
