---
title: "Anna K"
---

<div class="year">2016</div>
<div class="link"><a class="link" href="http://kavan.land/">http://kavan.land/</a></div>
<div class="category">Guests</div>

Anna K {lang=en}
===========================
<div class="summary">
A literary project by Catherine Lenoble and graphic designers Alexandre Leray and Stéphanie Vilayphiou, that focusses on the life and work of the British novelist Anna Kavan and is composed of two editions: a printed book *Anna K* and the online creation [Kavan.land](http://kavan.land/).
</div>

<div class="text">
**Printed copy: 20€**
**Interested to order a copy? Please visit <http://www.editions-hyx.com/fr/anna-k>**

*Anna K* is a literary creation by author Catherine Lenoble in collaboration with the graphic designers Alexandre Leray and Stéphanie Vilayphiou, both members of the collective Open Source Publishing. This fiction project focusses on the life and work of the British novelist Anna Kavan and is composed of two editions: a printed book *Anna K* and the online creation [Kavan.land](http://kavan.land/). Published by [Éditions HYX](http://www.editions-hyx.com).
</div>

Anna K {lang=fr}
===========================
<div class="summary">
Un projet de fiction de Catherine Lenoble et des graphistes Alexandre Leray et Stéphanie Vilayphiou autour de la vie de la romancière britannique Anna Kavan. Ce projet de création littéraire comporte deux éditions : le livre *Anna K* et l'édition en ligne [Kavan.land](http://kavan.land/).
</div>

<div class="text">
**Exemplaire imprimé : 20€ **
**Envie de commander un exemplaire? Merci de visiter <http://www.editions-hyx.com/fr/anna-k>**

Anna K, est le dernier projet de fiction développé par les [Éditions HYX](http://www.editions-hyx.com) avec l'auteure Catherine Lenoble et les graphistes Alexandre Leray et Stéphanie Vilayphiou du collectif Open Source Publishing. Autour de la vie de la romancière britannique Anna Kavan, ce projet de création littéraire comporte deux éditions : le livre *Anna K* et l'édition en ligne [Kavan.land](http://kavan.land/)..
</div>

Anna K {lang=nl}
===========================
<div class="summary">
Een literaire creatie van Catherine Lenoble en ontwerpers Alexandre Leray en Stéphanie Vilayphiou, rond het leven en werk van de Britse romanschrijfster Anna Kavan. Het bestaat uit twee uitgaven: het gedrukte boek *Anna K* en de online creatie [Kavan.land](http://kavan.land/).
</div>

<div class="text">
**Gedrukt exemplaar: 20€**
**Zin om een exemplaar te bestellen? Surf dan naar <http://www.editions-hyx.com/fr/anna-k>**

*Anna K* is een literaire creatie van auteur Catherine Lenoble in samenwerking met grafisten Alexandre Leray en Stéphanie Vilayphiou, beiden leden van het collectief Open Source Publishing. Deze fictie rond het leven en werk van de Britse romanschrijfster Anna Kavan bestaat uit twee uitgaven: het gedrukte boek *Anna K* en de online creatie [Kavan.land](http://kavan.land/).. Uitgegeven door [Éditions HYX](http://www.editions-hyx.com).
</div>
