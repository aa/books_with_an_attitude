title: 
Do (Not) Repeat Yourself
----
year: 
2016
----
category: Guests
----
link: https://freeze.sh/_/2017/doornot
----
summary: *Do (Not) Repeat Yourself* est écrit par Michael Murtaugh.
Le texte a d'abord été publié dans [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf), édité par Olga Goriunova en 2014. 
----
text:

*Do (Not) Repeat Yourself* est écrit par Michael Murtaugh.
Le texte a d'abord été publié dans [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf), édité par Olga Goriunova en 2014.

Le livre est rédigé et mis en page par Christoph Haag, dans le cadre de son exploration continue du dialect situé mark-up .mdsh

**Lisez en ligne**: <https://freeze.sh/0007>
**Téléchargez les sources**: <https://gitlab.com/chch/memo/tree/196d666eb7709>


