title: 
Do (Not) Repeat Yourself
----
year: 
2016
----
category: Guests
----
link: https://freeze.sh/_/2017/doornot
----
summary: *Do (Not) Repeat Yourself* is geschreven door Michael Murtaugh.
De tekst verscheen aanvankelijk als deel van [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf), samengesteld door Olga Goriunova in 2014. 
----
text:

*Do (Not) Repeat Yourself* is geschreven door Michael Murtaugh.
De tekst verscheen aanvankelijk als deel van [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf), samengesteld door Olga Goriunova in 2014. 

Redactie en lay-out van het boek gebeurde door Christoph Haag, als deel van zijn onderzoek naar het gesitueerde mark-up dialect .mdsh

**Lees online**: <https://freeze.sh/0007>
**Download het bronmateriaal**: <https://gitlab.com/chch/memo/tree/196d666eb7709>



