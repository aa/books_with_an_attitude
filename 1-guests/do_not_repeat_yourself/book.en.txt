title: 
Do (Not) Repeat Yourself
----
year: 
2016
----
category: Guests
----
link: https://freeze.sh/_/2017/doornot
----
summary: *Do (Not) Repeat Yourself* was written by Michael Murtaugh.
The text was first published 2014 in [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf) edited by Olga Goriunova. 
----
text:

*Do (Not) Repeat Yourself* was written by Michael Murtaugh.
The text was first published 2014 in [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf) edited by Olga Goriunova. 

The text was edited and typeset by Christoph Haag as part of an ongoing exploration of the situated mark-up dialect .mdsh

**Read online**: <https://freeze.sh/0007>
**Download the sources**: <https://gitlab.com/chch/memo/tree/196d666eb7709>




