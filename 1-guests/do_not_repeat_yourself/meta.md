---
title: "Do (Not) Repeat Yourself"
---

<div class="year">2016</div>
<div class="link"><a class="link" href="https://freeze.sh/_/2017/doornot">https://freeze.sh/_/2017/doornot</a></div>
<div class="category">Guests</div>

Do (Not) Repeat Yourself {lang=en}
===========================
<div class="summary">
*Do (Not) Repeat Yourself* was written by Michael Murtaugh.
The text was first published 2014 in [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf) edited by Olga Goriunova.
</div>

<div class="text">
*Do (Not) Repeat Yourself* was written by Michael Murtaugh.
The text was first published 2014 in [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf) edited by Olga Goriunova. 

The text was edited and typeset by Christoph Haag as part of an ongoing exploration of the situated mark-up dialect .mdsh

**Read online**: <https://freeze.sh/0007>
**Download the sources**: <https://gitlab.com/chch/memo/tree/196d666eb7709>
</div>

Do (Not) Repeat Yourself {lang=fr}
===========================
<div class="summary">
*Do (Not) Repeat Yourself* est écrit par Michael Murtaugh.
Le texte a d'abord été publié dans [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf), édité par Olga Goriunova en 2014.
</div>

<div class="text">
*Do (Not) Repeat Yourself* est écrit par Michael Murtaugh.
Le texte a d'abord été publié dans [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf), édité par Olga Goriunova en 2014.

Le livre est rédigé et mis en page par Christoph Haag, dans le cadre de son exploration continue du dialect situé mark-up .mdsh

**Lisez en ligne**: <https://freeze.sh/0007>
**Téléchargez les sources**: <https://gitlab.com/chch/memo/tree/196d666eb7709>
</div>

Do (Not) Repeat Yourself {lang=nl}
===========================
<div class="summary">
*Do (Not) Repeat Yourself* is geschreven door Michael Murtaugh.
De tekst verscheen aanvankelijk als deel van [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf), samengesteld door Olga Goriunova in 2014.
</div>

<div class="text">
*Do (Not) Repeat Yourself* is geschreven door Michael Murtaugh.
De tekst verscheen aanvankelijk als deel van [*Fun and Sofware*](https://monoskop.org/images/1/14/Goriunova_Olga_ed_Fun_and_Software_Exploring_Pleasure_Paradox_and_Pain_in_Computing.pdf), samengesteld door Olga Goriunova in 2014. 

Redactie en lay-out van het boek gebeurde door Christoph Haag, als deel van zijn onderzoek naar het gesitueerde mark-up dialect .mdsh

**Lees online**: <https://freeze.sh/0007>
**Download het bronmateriaal**: <https://gitlab.com/chch/memo/tree/196d666eb7709>
</div>
