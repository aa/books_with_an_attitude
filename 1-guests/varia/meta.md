---
title: "Networks of Ones Own #1 : Varia"
---

<div class="year">2019</div>
<div class="link"><a class="link" href="https://networksofonesown.constantvzw.org/">https://networksofonesown.constantvzw.org/</a></div>
<div class="category">Guests</div>

Networks of Ones Own #1 : Varia {lang=en}
===========================
<div class="summary">
The second issue of Networks of One's Own was initiated by Varia, a Rotterdam based initiative which focuses on working with, on and through everyday technology. The making of this publication created a space to interconnect multiple ways of taking care, by specifically focusing on three projects: Bibliotecha, homebrewserver.club and Relearn.
</div>

<div class="text">
**Digital version of the publication:** https://networksofonesown.vvvvvvaria.org/
**Printed copy 13€ + sending:** please email info@varia.zone

The second issue of Networks of One's Own was initiated by Varia, a Rotterdam based initiative which focuses on working with, on and through everyday technology.

This publication is an occasion to revisit three very different projects that have been important for the emergence of Varia, and for its individual members. These projects give insights into how and why we engage with collaborative practices. While we were gathering in different formations to initiate, develop, organise or present these projects, Varia was being conceived, first as an idea, then as a group and later as a physical space.

As artists, designers and researchers working with and around free/libre open source technology, the authorship of the tools and research that we develop is shared, not just amongst us at Varia but also with a wider international network. Multiple agents are involved at different moments, with varying intensity and for a range of different reasons. This results in intricate interrelationships of ownership which complicate documentation and long-term maintenance. Relational projects need flexible support structures that can handle and hold fragmentation and clustering.

For the projects in this publication, a chain of interrelated events provided an infrastructure for ongoing reflection, sharing and collaborative attention to such projects. Our path crossed at Varia in Rotterdam but also during 35c3 in Leipzig, at the Computer Grrrls exhibition in Paris, at Constant worksessions in Brussels and Antwerp, in conversation with XPUB tutors and students or at AMRO in Linz. While a lot of work was done by bringing these projects to different meeting points, we felt there was a need for concentrated rumination and active documentation. By collapsing maintenance work with publishing, we found a form that enabled this collective care work to happen and to spend the necessary concentrated time together. The making of this publication created a space to interconnect multiple ways of taking care. As a result, we hope that the projects have become more clearly articulated and are now ready to be released back into the networks to form new constellations.
</div>

Networks of Ones Own #1 : Varia {lang=fr}
===========================
<div class="summary">
Le deuxième numéro de Networks of One's Own a été lancé par Varia, une initiative basée à Rotterdam qui se concentre sur le travail avec, sur et par la technologie de tous les jours. La réalisation de cette publication a créé un espace permettant d'interconnecter de multiples façons de prendre soin, en se concentrant spécifiquement sur trois projets : Bibliotecha, homebrewserver.club et Relearn.
</div>

<div class="text">
**Version digitale de la publication:** https://networksofonesown.vvvvvvaria.org/
**Exemplaire imprimé 13€ + envoi:** merci d'envoyer un email à info@varia.zone

Le deuxième numéro de Networks of One's Own a été lancé par Varia, une initiative basée à Rotterdam qui se concentre sur le travail avec, sur et par la technologie de tous les jours.

Cette publication est l'occasion de revenir sur trois projets très différents qui ont été importants pour l'apparition de Varia, et pour ses membres individuels. Ces projets donnent un aperçu de la manière dont nous nous engageons dans des pratiques de collaboration et des raisons pour lesquelles nous le faisons. Alors que nous nous réunissions en différentes formations pour initier, développer, organiser ou présenter ces projets, Varia était conçu, d'abord comme une idée, puis comme un groupe et plus tard comme un espace physique.

En tant qu'artistes, designers et chercheurs travaillant avec et autour de la technologie libre, la paternité des outils et des recherches que nous développons est partagée, non seulement entre nous à Varia mais aussi avec un réseau international plus large. De multiples acteurs sont impliqués à de différents moments, avec une intensité variable et pour une série de raisons différentes. Il en résulte des relations complexes de propriété qui compliquent la documentation et la maintenance à long terme. Les projets relationnels ont besoin de structures de soutien flexibles qui peuvent gérer et maintenir la fragmentation et le regroupement.

Pour les projets présentés dans cette publication, une chaîne d'événements interdépendants a fourni une infrastructure pour une réflexion, un partage et une attention collaborative continus à ces projets. Nos chemins se sont croisés à Varia à Rotterdam, mais aussi pendant 35c3 à Leipzig, à l'exposition Computer Grrrls à Paris, aux sessions de travail Constant à Bruxelles et à Anvers, en conversation avec des professeurs et des étudiants XPUB ou à AMRO à Linz. Bien que beaucoup de travail ait été réalisé en amenant ces projets à différents points de rencontre, nous avons ressenti le besoin d'une rumination concentrée et d'une documentation active. En associant le travail de maintenance à la publication, nous avons trouvé une forme qui permettait à ce travail de soins collectifs de se produire et de passer du temps concentré ensemble. La réalisation de cette publication a créé un espace permettant d'interconnecter de multiples façons de prendre soin. En conséquence, nous espérons que les projets sont devenus plus clairement articulés et sont maintenant prêts à être remis en réseau pour former de nouvelles constellations.
</div>

Networks of Ones Own #1 : Varia {lang=nl}
===========================
<div class="summary">
Het tweede nummer van Networks of One's Own is geïnitieerd door Varia, een Rotterdams initiatief dat zich richt op het werken met, rond en door middel van alledaagse technologie. Met deze publicatie werd een ruimte gecreëerd die meerdere manieren van zorg met elkaar verbindt, door specifiek te focussen op drie projecten: Bibliotecha, homebrewserver.club en Relearn.
</div>

<div class="text">
**Digitale versie van de publicatie:** https://networksofonesown.vvvvvvaria.org/
**Gedrukt exemplaar 13€ + verzending:** stuur een email naar info@varia.zone

Het tweede nummer van Networks of One's Own is geïnitieerd door Varia, een Rotterdams initiatief dat zich richt op het werken met, rond en door middel van alledaagse technologie.

Deze publicatie is een gelegenheid om drie zeer verschillende projecten die belangrijk zijn geweest voor de opkomst van Varia, en voor de individuele leden, opnieuw te bekijken. Deze projecten geven inzicht in het hoe en waarom van onze samenwerking. Terwijl we in verschillende formaties bijeenkwamen om deze projecten te initiëren, te ontwikkelen, te organiseren of te presenteren, werd Varia bedacht, eerst als een idee, daarna als een groep en later als een fysieke ruimte.

Als kunstenaars, ontwerpers en onderzoekers die werken met en rond free/libre open source technologie, wordt het auteurschap van de tools en het onderzoek dat we ontwikkelen gedeeld, niet alleen onder ons bij Varia maar ook met een breder internationaal netwerk. Meerdere agenten zijn op verschillende momenten betrokken, met wisselende intensiteit en om verschillende redenen. Dit resulteert in complexe eigendomsverhoudingen die de documentatie en het onderhoud op lange termijn bemoeilijken. Relationele projecten hebben flexibele ondersteuningsstructuren nodig die versnippering en clustering kunnen opvangen en in stand houden.

Voor de projecten in deze publicatie zorgde een reeks onderling samenhangende evenementen voor een infrastructuur die op lange termijn reflectie toeliet, uitwisseling en collectieve aandacht. Onze wegen kruisten bij Varia in Rotterdam, maar ook tijdens 35c3 in Leipzig, op de Computer Grrrls tentoonstelling in Parijs, tijdens Constant worksessions in Brussel en Antwerpen, in gesprek met XPUB docenten en studenten of bij AMRO in Linz. Veel werk werd verzet door deze projecten naar verschillende ontmoetingsplaatsen te brengen. Tegelijk vonden we dat er nood was aan geconcentreerde herwerking en actieve documentatie. In de combinatie van onderhoudswerk en publicatie vonden we een vorm die dit collectieve zorgwerk mogelijk maakte en brachten we de nodige geconcentreerde tijd samen door. Met deze publicatie is er een ruimte ontstaan om meerdere zorgwerkzaamheden met elkaar te verbinden. We hopen dat de projecten nu duidelijker gearticuleerd zijn en klaar om weer te reizen door de netwerken en nieuwe opstellingen te vormen.
</div>
