---
title: "Footfall Almanac 2019"
---

<div class="year">2019</div>

<div class="category">Guests</div>

Footfall Almanac 2019 {lang=en}
===========================
<div class="summary">
*The Footfall Almanac 2019* collects observations, objects and other traces to instigate a discussion on surveillance techniques currently
deployed in shopping malls, and during public major events in Brussels.
</div>

<div class="text">
**Download the book**: http://constantvzw.org/site/IMG/pdf/footfall-almanac-2019-lores.pdf
**Printed copy**: 10€ (excl shipping costs)
**Interested to order a copy? Please email info@constantvzw.org**

*The Footfall Almanac 2019* collects observations, objects and other traces to instigate a discussion on surveillance techniques currently
deployed in shopping malls, and during public major events in Brussels.

Wireless tracking of mobile phones has become a common method to monitor crowds without requiring explicit permission or active cooperation.
Private companies as well as civil agencies use it to keep a close eye on the movements of city dwellers through public spaces, the former to
forecast sales and the latter for crowd management purposes. 

The Footfall Almanac 2019 observes this encounter of actors and predictions in their shared technologies and terminologies.

Edited by: Kurt Tichy and Alex Zakkas
With contributions by: Femke Snelting and Dennis Pohl.
</div>

Footfall Almanac 2019 {lang=fr}
===========================
<div class="summary">
*The Footfall Almanac 2019* recueille des observations, des objets et d'autres traces afin de lancer une discussion sur les techniques de surveillance actuellement déployés dans les centres commerciaux et lors des grands événements publics à Bruxelles.
</div>

<div class="text">
**Téléchargez le livre** : http://constantvzw.org/site/IMG/pdf/footfall-almanac-2019-lores.pdf
**Exemplaire imprimé** : 10€ (excl frais de livraison)
**Envie de commander un exemplaire ? Merci d'envoyer un e-mail à info@constantvzw.org**

*The Footfall Almanac 2019* recueille des observations, des objets et d'autres traces afin de lancer une discussion sur les techniques de surveillance actuellement déployés dans les centres commerciaux et lors des grands événements publics à Bruxelles.

Le suivi sans fil des téléphones portables est devenu une méthode courante pour surveiller les foules sans avoir besoin d'une autorisation explicite ou d'une coopération active. Les entreprises privées ainsi que les organismes civils l'utilisent pour surveiller de près les déplacements des citadins dans les espaces publics, les premiers pour prédire des ventes et les derniers à des fins de gestion des foules. 

Le Footfall Almanac 2019 observe cette rencontre d'acteurs et de prévisions dans leurs technologies et terminologies communes. 

Edité par: Kurt Tichy et Alex Zakkas
Avec des contributions de: Femke Snelting et Dennis Pohl.
</div>

Footfall Almanac 2019 {lang=nl}
===========================
<div class="summary">
*The Footfall Almanac 2019* recueille des observations, des objets et d'autres traces afin de lancer une discussion sur les techniques de surveillance actuellement déployés dans les centres commerciaux et lors des grands événements publics à Bruxelles.
</div>

<div class="text">
**Download het boek** : http://constantvzw.org/site/IMG/pdf/footfall-almanac-2019-lores.pdf
**Gedrukt exemplaar** : 10€ (excl. verzendingskosten)
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org*

*The Footfall Almanac 2019* verzamelt waarnemingen, objecten en andere sporen, om een discussie op gang te brengen over bewakingstechnieken die momenteel in winkelcentra en tijdens grote openbare evenementen in Brussel worden gebruikt.

Het draadloos volgen van mobiele telefoons (wireless tracking) is een veelgebruikte methode om groepen mensen te monitoren zonder dat daarvoor expliciete toestemming of actieve medewerking nodig is. Zowel privé-bedrijven als burgerinstanties gebruiken het om de beweging van stedelingen in de openbare ruimte nauwlettend in de gaten te houden, de eerste om de verkoop te voorspellen en de tweede voor crowd-management-doeleinden.

Le Footfall Almanac 2019 observeert deze samenloop van actoren en voorspellingen in hun gemeenschappelijke technologieën en terminologieën.

Redactie: Kurt Tichy en Alex Zakkas
Met bijdragen van: Femke Snelting en Dennis Pohl.
</div>
