title: 
Footfall Almanac 2019
----
year: 
2019
----
category: Guests
----
link: 
----
summary: *The Footfall Almanac 2019* recueille des observations, des objets et d'autres traces afin de lancer une discussion sur les techniques de surveillance actuellement déployés dans les centres commerciaux et lors des grands événements publics à Bruxelles.
----
text:

**Download het boek** : http://constantvzw.org/site/IMG/pdf/footfall-almanac-2019-lores.pdf
**Gedrukt exemplaar** : 10€ (excl. verzendingskosten)
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org*

*The Footfall Almanac 2019* verzamelt waarnemingen, objecten en andere sporen, om een discussie op gang te brengen over bewakingstechnieken die momenteel in winkelcentra en tijdens grote openbare evenementen in Brussel worden gebruikt.

Het draadloos volgen van mobiele telefoons (wireless tracking) is een veelgebruikte methode om groepen mensen te monitoren zonder dat daarvoor expliciete toestemming of actieve medewerking nodig is. Zowel privé-bedrijven als burgerinstanties gebruiken het om de beweging van stedelingen in de openbare ruimte nauwlettend in de gaten te houden, de eerste om de verkoop te voorspellen en de tweede voor crowd-management-doeleinden.

Le Footfall Almanac 2019 observeert deze samenloop van actoren en voorspellingen in hun gemeenschappelijke technologieën en terminologieën.

Redactie: Kurt Tichy en Alex Zakkas
Met bijdragen van: Femke Snelting en Dennis Pohl.


