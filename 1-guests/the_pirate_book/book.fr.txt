title: 
The Pirate Book
----
year: 
2015
----
category: Guests
----
link: http://thepiratebook.net/
----
summary: Cette oeuvre offre une perspective étendue sur le piratage numérique, tout comme une variété de perspectives comparatives sur des cas récents et des faits historiques liés au piratage. Elle présente une compilation de textes sur des situations 'grassroots', dont les récits décrivent les stratégies développées pour partager, distribuer et expérimenter des contenus culturels en dehors des limites économiques, politiques ou légales. 
----
text:

**Téléchargez le livre**: <http://thepiratebook.net/wp-content/uploads/The_Pirate_Book.pdf>
**Commandez un exemplaire imprimé N&B*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461057.html>
**Commandez un exemplaire imprimé Couleur*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461066.html>

Cette oeuvre offre une perspective étendue sur le piratage numérique, tout comme une variété de perspectives comparatives sur des cas récents et des faits historiques liés au piratage. Elle présente une compilation de textes sur des situations 'grassroots', dont les récits décrivent les stratégies développées pour partager, distribuer et expérimenter des contenus culturels en dehors des limites économiques, politiques ou légales. 
Ces récits racontent les expériences d'individus en Inde, au Cuba, au Brésil, au Mexique, à Mali et en Chine. Le livre se décline en quatre parties, dont la première est une collection d'histoires autour du piratage qui remontent à l'invention de l'imprimerie et s'étendent à des sujets plus larges (les technologies d'antipiratage historiques et modernes, des cas géographiques spécifiques, la règlementation de la scène de Warez, ses chartes, sa culture structurelle et visuelle…).

*Edité par*: Nicolas Maigret & Maria Roszkowska
*Contributeurs*: Jota Izquierdo, Christopher Kirkley, Marie Lechner, Pedro Mizukami, Ernesto Oroza, Clément Renaud, Ishita Tiwary, Ernesto Van der Sar, Michaël Zumstein
*Mise-en-page*: Maria Roszkowska
*Relecture*: Philip Jan Nagel
*Traduction*: Themba Bhebhe
*Publié par*: Aksioma, Ljubljana
*Produit avec*: Pavillon Vendôme, Clichy





