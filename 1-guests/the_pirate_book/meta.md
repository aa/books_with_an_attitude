---
title: "The Pirate Book"
---

<div class="year">2015</div>
<div class="link"><a class="link" href="http://thepiratebook.net/">http://thepiratebook.net/</a></div>
<div class="category">Guests</div>

The Pirate Book {lang=en}
===========================
<div class="summary">
This work offers a broad view on media piracy as well as a variety of comparative perspectives on recent issues and historical facts regarding piracy. It contains a compilation of texts on grass­roots situations whose stories describe strategies developed to share, distribute and experience cultural content outside of the confines of local economies, politics or laws
</div>

<div class="text">
**Download the book**: <http://thepiratebook.net/wp-content/uploads/The_Pirate_Book.pdf>
**Order a printed copy B&W*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461057.html>
**Order a printed copy Colour*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461066.html>

This work offers a broad view on media piracy as well as a variety of comparative perspectives on recent issues and historical facts regarding piracy. It contains a compilation of texts on grass­roots situations whose stories describe strategies developed to share, distribute and experience cultural content outside of the confines of local economies, politics or laws.
These stories recount the experiences of individuals from India, Cuba, Brazil, Mexico, Mali and China. The book is structured in four parts and begins with a collection of stories on piracy dating back to the invention of the printing press and expanding to broader issues (historical and modern anti­piracy technologies, geographically­ specific issues, as well as the rules of the Warez scene, its charters, structure and visual culture…).

*Edited* by Nicolas Maigret & Maria Roszkowska
*Contributors*: Jota Izquierdo, Christopher Kirkley, Marie Lechner, Pedro Mizukami, Ernesto Oroza, Clément Renaud, Ishita Tiwary, Ernesto Van der Sar, Michaël Zumstein
*Design*: Maria Roszkowska
*Proofreading*: Philip Jan Nagel
*Translation*: Themba Bhebhe
*Published* by Aksioma, Ljubljana
*Produced with*: Pavillon Vendôme, Clichy
</div>

The Pirate Book {lang=fr}
===========================
<div class="summary">
Cette oeuvre offre une perspective étendue sur le piratage numérique, tout comme une variété de perspectives comparatives sur des cas récents et des faits historiques liés au piratage. Elle présente une compilation de textes sur des situations 'grassroots', dont les récits décrivent les stratégies développées pour partager, distribuer et expérimenter des contenus culturels en dehors des limites économiques, politiques ou légales.
</div>

<div class="text">
**Téléchargez le livre**: <http://thepiratebook.net/wp-content/uploads/The_Pirate_Book.pdf>
**Commandez un exemplaire imprimé N&B*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461057.html>
**Commandez un exemplaire imprimé Couleur*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461066.html>

Cette oeuvre offre une perspective étendue sur le piratage numérique, tout comme une variété de perspectives comparatives sur des cas récents et des faits historiques liés au piratage. Elle présente une compilation de textes sur des situations 'grassroots', dont les récits décrivent les stratégies développées pour partager, distribuer et expérimenter des contenus culturels en dehors des limites économiques, politiques ou légales. 
Ces récits racontent les expériences d'individus en Inde, au Cuba, au Brésil, au Mexique, à Mali et en Chine. Le livre se décline en quatre parties, dont la première est une collection d'histoires autour du piratage qui remontent à l'invention de l'imprimerie et s'étendent à des sujets plus larges (les technologies d'antipiratage historiques et modernes, des cas géographiques spécifiques, la règlementation de la scène de Warez, ses chartes, sa culture structurelle et visuelle…).

*Edité par*: Nicolas Maigret & Maria Roszkowska
*Contributeurs*: Jota Izquierdo, Christopher Kirkley, Marie Lechner, Pedro Mizukami, Ernesto Oroza, Clément Renaud, Ishita Tiwary, Ernesto Van der Sar, Michaël Zumstein
*Mise-en-page*: Maria Roszkowska
*Relecture*: Philip Jan Nagel
*Traduction*: Themba Bhebhe
*Publié par*: Aksioma, Ljubljana
*Produit avec*: Pavillon Vendôme, Clichy
</div>

The Pirate Book {lang=nl}
===========================
<div class="summary">
Dit werk biedt een brede kijk op media-piraterij. Het ontwikkelt een reeks vergelijkende standpunten op recente situaties en historische feiten van piraterij. Aan de hand van verhalen van grassroots-momenten worden strategieën beschreven die ontwikkeld werden om cutluur te kunnen delen, verspreiden en ervaren, buiten de grenzen van lokale economie, politiek en wetgeving om.
</div>

<div class="text">
**Download het boek**: <http://thepiratebook.net/wp-content/uploads/The_Pirate_Book.pdf>
**Bestel een gedrukt exemplaar Z&W*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461057.html>
**Bestel een gedrukt exemplaar Kleur*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461066.html>

Dit werk biedt een brede kijk op media-piraterij. Het ontwikkelt een reeks vergelijkende standpunten op recente situaties en historische feiten van piraterij. Aan de hand van verhalen van grassroots-momenten worden strategieën beschreven die ontwikkeld werden om cutluur te kunnen delen, verspreiden en ervaren, buiten de grenzen van lokale economie, politiek en wetgeving om.
De verhalen vertellen ervaringen van mensen uit Indië, Cuba, Brazilië, Mexico, Mali en China. Het book is opgedeeld in vier delen. Het eerste deel is een verzameling piraterij-verhalen, die teruggaan naar de uitvinding van de boekdrukkunst en vervolgens uitwijden naar bredere onderwerpen (ouden en moderne anti-piraterijtechnieken, specifieke geografische gevallen, de regels van de Warez scène, hun charters, structurele en visuele cultuur...).

*Redactie*: Nicolas Maigret & Maria Roszkowska
*Bijdragen*: Jota Izquierdo, Christopher Kirkley, Marie Lechner, Pedro Mizukami, Ernesto Oroza, Clément Renaud, Ishita Tiwary, Ernesto Van der Sar, Michaël Zumstein
*Lay-out*: Maria Roszkowska
*Eindredactie*: Philip Jan Nagel
*Vertaling*: Themba Bhebhe
*Uitgegeven door*: Aksioma, Ljubljana
*In samenwerking met*: Pavillon Vendôme, Clichy
</div>
