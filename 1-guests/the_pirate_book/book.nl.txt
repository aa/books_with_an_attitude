title: 
The Pirate Book
----
year: 
2015
----
category: Guests
----
link: http://thepiratebook.net/
----
summary: Dit werk biedt een brede kijk op media-piraterij. Het ontwikkelt een reeks vergelijkende standpunten op recente situaties en historische feiten van piraterij. Aan de hand van verhalen van grassroots-momenten worden strategieën beschreven die ontwikkeld werden om cutluur te kunnen delen, verspreiden en ervaren, buiten de grenzen van lokale economie, politiek en wetgeving om.
----
text:

**Download het boek**: <http://thepiratebook.net/wp-content/uploads/The_Pirate_Book.pdf>
**Bestel een gedrukt exemplaar Z&W*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461057.html>
**Bestel een gedrukt exemplaar Kleur*: <http://www.lulu.com/shop/aavv/the-pirate-book/paperback/product-22461066.html>

Dit werk biedt een brede kijk op media-piraterij. Het ontwikkelt een reeks vergelijkende standpunten op recente situaties en historische feiten van piraterij. Aan de hand van verhalen van grassroots-momenten worden strategieën beschreven die ontwikkeld werden om cutluur te kunnen delen, verspreiden en ervaren, buiten de grenzen van lokale economie, politiek en wetgeving om.
De verhalen vertellen ervaringen van mensen uit Indië, Cuba, Brazilië, Mexico, Mali en China. Het book is opgedeeld in vier delen. Het eerste deel is een verzameling piraterij-verhalen, die teruggaan naar de uitvinding van de boekdrukkunst en vervolgens uitwijden naar bredere onderwerpen (ouden en moderne anti-piraterijtechnieken, specifieke geografische gevallen, de regels van de Warez scène, hun charters, structurele en visuele cultuur...).

*Redactie*: Nicolas Maigret & Maria Roszkowska
*Bijdragen*: Jota Izquierdo, Christopher Kirkley, Marie Lechner, Pedro Mizukami, Ernesto Oroza, Clément Renaud, Ishita Tiwary, Ernesto Van der Sar, Michaël Zumstein
*Lay-out*: Maria Roszkowska
*Eindredactie*: Philip Jan Nagel
*Vertaling*: Themba Bhebhe
*Uitgegeven door*: Aksioma, Ljubljana
*In samenwerking met*: Pavillon Vendôme, Clichy





