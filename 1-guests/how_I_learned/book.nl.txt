title: 
How I learned to stop and love the draft
----
year: 
2016
----
category: Guests
----
link: https://freeze.sh/_/2016/hilxalsh
----
summary: *How I ...* is een boek van Christoph Haag. Uitgangspunt is zijn interesse om dingen publiek te maken als een creatieve uitdaging. Na een initiële micro-uitgave wacht deze publicatie op kleine debugs en belangrijk redactiewerk. Intussen: 
*Lees het Bronmateriaal*: <https://github.com/christop/memo/blob/d91bbc6/EDIT/160201_hilxalsh.mdsh>
----
text:

*How I ...* is een boek van Christoph Haag. Uitgangspunt is zijn interesse om dingen publiek te maken als een creatieve uitdaging. Na een initiële micro-uitgave wacht deze publicatie op kleine debugs en belangrijk redactiewerk. Intussen: 
*Lees het Bronmateriaal*: <https://github.com/christop/memo/blob/d91bbc6/EDIT/160201_hilxalsh.mdsh>


