---
title: "How I learned to stop and love the draft"
---

<div class="year">2016</div>
<div class="link"><a class="link" href="https://freeze.sh/_/2016/hilxalsh">https://freeze.sh/_/2016/hilxalsh</a></div>
<div class="category">Guests</div>

How I learned to stop and love the draft {lang=en}
===========================
<div class="summary">
*How I ...* is a book by Christoph Haag and part of his interest in making things public as a creative endeavour.
After a initial micro‑edition it's awaiting minor bug fixes and major revisions. Meanwhile:
*Read the Source*: <https://github.com/christop/memo/blob/d91bbc6/EDIT/160201_hilxalsh.mdsh>
</div>

<div class="text">
*How I ...* is a book by Christoph Haag and part of his interest in making things public as a creative endeavour.
After a initial micro‑edition it's awaiting minor bug fixes and major revisions. Meanwhile:
*Read the Source*: <https://github.com/christop/memo/blob/d91bbc6/EDIT/160201_hilxalsh.mdsh>
</div>

How I learned to stop and love the draft {lang=fr}
===========================
<div class="summary">
*How I ...* est un livre de Christoph Haag qui prend comme point de départ sa curiosité à rendre des choses publiques en tant que défi créatif. Après une micro-édition initiale, la publication attend des réparations de bug minimales tout comme des révisions majeures. Entretemps:
*Lisez la Source*: <https://github.com/christop/memo/blob/d91bbc6/EDIT/160201_hilxalsh.mdsh>
</div>

<div class="text">
*How I ...* est un livre de Christoph Haag qui prend comme point de départ sa curiosité à rendre des choses publiques en tant que défi créatif. Après une micro-édition initiale, la publication attend des réparations de bug minimales tout comme des révisions majeures. Entretemps:
*Lisez la Source*: <https://github.com/christop/memo/blob/d91bbc6/EDIT/160201_hilxalsh.mdsh>
</div>

How I learned to stop and love the draft {lang=nl}
===========================
<div class="summary">
*How I ...* is een boek van Christoph Haag. Uitgangspunt is zijn interesse om dingen publiek te maken als een creatieve uitdaging. Na een initiële micro-uitgave wacht deze publicatie op kleine debugs en belangrijk redactiewerk. Intussen: 
*Lees het Bronmateriaal*: <https://github.com/christop/memo/blob/d91bbc6/EDIT/160201_hilxalsh.mdsh>
</div>

<div class="text">
*How I ...* is een boek van Christoph Haag. Uitgangspunt is zijn interesse om dingen publiek te maken als een creatieve uitdaging. Na een initiële micro-uitgave wacht deze publicatie op kleine debugs en belangrijk redactiewerk. Intussen: 
*Lees het Bronmateriaal*: <https://github.com/christop/memo/blob/d91bbc6/EDIT/160201_hilxalsh.mdsh>
</div>
