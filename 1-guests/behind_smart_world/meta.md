---
title: "Behind the Smart World – saving, deleting and resurfacing of data"
---

<div class="year">2015</div>
<div class="link"><a class="link" href="https://freeze.sh/_/2016/btsw/">https://freeze.sh/_/2016/btsw/</a></div>
<div class="category">Guests</div>

Behind the Smart World – saving, deleting and resurfacing of data {lang=en}
===========================
<div class="summary">
In this publication researchers and artists unfold a critical look at our smart world in three parts: Saving Data, Deleting Data and Resurfacing Data. Each part begins with theoretical texts that address some of the concerns, followed by strategies that expose problematic power structures, creatively reveal how we lost control of our data and how we can deal again with our data.
</div>

<div class="text">
**Download the book**: [pdf](http://publications.servus.at/2016-Behind_the_Smart_World/pdf/), [epub](http://publications.servus.at/2016-Behind_the_Smart_World/epub/)
**Read on the web**: <http://publications.servus.at/2016-Behind_the_Smart_World/html/>
**Download the sources**: <https://github.com/lafkon/servus.btsw>

The smart world. Created by policymakers, the advertising world, creative industries, and persuasive UX-designers that portray to us a world of
shiny brand new technologies, apps that solve all our daily problems and smart cities collecting big data that will eventually solve all the problems of human kind. 
If we take a slightly more critical look at our smart world, though, our shiny gadgets become obsolete faster than ever, turning into toxic
e-waste; our apps and smart cities have turned into an effective all-encompassing surveillance apparatus, and we have no idea who is collecting our data, who accesses it, and where it is stored. There may be issues that the smart world can solve, but at the same time, it raises new problems concerning data breaches, data privacy, data ownership and electronic waste. 
In this publication researchers and artists unfold some of these issues in three parts: Saving Data, Deleting Data and Resurfacing Data. Each part begins with theoretical texts that address some of the concerns, followed by strategies of artists and activists that expose problematic power structures, creatively reveal how we lost control of our data and offer strategies to deal with our data in this smart world.

Edited by [Kairus.org](http://kairus.org) – Linda Kronman, Andreas Zingerle
Project Coordination by [Us(c)hi Reiter](http://www.firstfloor.org/ur)
Published by [servus.at](http://servus.at) as part of the AMRO Research Lab 2015.

With essays by Fieke Jansen ([Tactical Tech](https://tacticaltech.org)), [Ivar Veermäe](http://www.ivarveermae.com), [Emilio Vavarella](http://emiliovavarella.com), [Leo Selvaggio](http://leoselvaggio.com), [Marloes de Valk](http://pi.kuri.mu), [Research Team “Times of Waste”](http://www.ixdm.ch/portfolio/times-waste), [Stefan Tiefengraber](http://www.stefantiefengraber.com), [Dr. Michael Sonntag](http://www.fim.uni-linz.ac.at/staff/sonntag.htm) and interviews with [Audrey Samson](http://www.ideacritik.com) and [Michaela Lakova](http://mlakova.org).

The publication made it through a free software workflow based on markdown, LaTeX and the GNU Bourne-Again SHell. You are free to use,
study, change, improve it under the terms of the GNU General Public License.
</div>

Behind the Smart World – saving, deleting and resurfacing of data {lang=fr}
===========================
<div class="summary">
Dans cette publication, chercheurs et artistes développent un regard critique sur notre monde intelligent en trois parties: la Sauvegarde des Données, l'Effacement des Données et la Rééemergence des Données. Chaque partie est composée de textes théoriques suivis de stratégies qui exposent des structures de pouvoir problématiques, révèlent de façon créative comment nous avons perdu le contrôle sur nos données et montrent quelles attitudes nous pouvons développer vis-à-vis de celles-ci.
</div>

<div class="text">
**Téléchargez le livre**: [pdf](http://publications.servus.at/2016-Behind_the_Smart_World/pdf/), [epub](http://publications.servus.at/2016-Behind_the_Smart_World/epub/)
**Lisez en ligne**: <http://publications.servus.at/2016-Behind_the_Smart_World/html/>
**Téléchargez les sources**: <https://github.com/lafkon/servus.btsw>

Le monde intelligent; créé par les politiques, la publicité, les industries créatives et des développeurs UX persuasifs qui nous dessinent un monde régi par de nouvelles technologies brillantes, des applications capables de résoudre nos problèmes quotidiens et des villes intelligentes qui collectionnent nos données en promettant de résoudre tous les problèmes de l'humanité.
Si, par contre, on jette un coup d'oeil critique sur ce monde intelligent, on s'aperçoit que nos gadgets brillants deviennent obsolètes plus vite que jamais, se transformant en déchets toxiques; nos applications et nos villes intelligentes quant à elles constituent un appareil effectif de surveillance tout-puissant. Nous n'avons aucune idée qui collectionne nos données, qui y a accès ni où elles sont sauvegardées. S'il existe des cas que le monde intelligent arrive à résoudre, il pose dans le même temps de nouveaux problèmes sur la violation des données, leur privacité et leur propriété ainsi que sur les déchets électroniques.  
Dans cette publication, chercheurs et artistes développent un regard critique sur notre monde intelligent en trois parties: la Sauvegarde des Données, l'Effacement des Données et la Rééemergence des Données. Chaque partie est composée de textes théoriques suivis de stratégies qui exposent des structures de pouvoir problématiques, révèlent de façon créative comment nous avons perdu le contrôle sur nos données et montrent quelles attitudes nous pouvons développer vis-à-vis de celles-ci.

Edité par [Kairus.org](http://kairus.org) – Linda Kronman, Andreas Zingerle
Coordination du projet par [Us(c)hi Reiter](http://www.firstfloor.org/ur)
Publié par [servus.at](http://servus.at) dans le cadre du AMRO Research Lab 2015.

Avec des essais de Fieke Jansen ([Tactical Tech](https://tacticaltech.org)), [Ivar Veermäe](http://www.ivarveermae.com), [Emilio Vavarella](http://emiliovavarella.com), [Leo Selvaggio](http://leoselvaggio.com), [Marloes de Valk](http://pi.kuri.mu), [Research Team “Times of Waste”](http://www.ixdm.ch/portfolio/times-waste), [Stefan Tiefengraber](http://www.stefantiefengraber.com), [Dr. Michael Sonntag](http://www.fim.uni-linz.ac.at/staff/sonntag.htm) et des interviews avec [Audrey Samson](http://www.ideacritik.com) et [Michaela Lakova](http://mlakova.org).

Cette publication a été réalisée en utilisant markdown, LaTeX et le GNU Bourne-Again SHell. Vous êtes libres de l'utiliser, de l'étudier, de la modifier et de l'améliorer sous les conditions de la GNU General Public License.
</div>

Behind the Smart World – saving, deleting and resurfacing of data {lang=nl}
===========================
<div class="summary">
In deze publicatie bieden onderzoekers en kunstenaars een kritische kijk op onze 'intelligente' wereld. Ze doen dat in drie onderdelen: het Bewaren van Data, het Wissen van Data en het Opnieuw doen Verschijnen van Data. Elk onderdeel bestaat uit theoretische teksten, gevolgd door strategieën om problematische machtsstructuren te ontdekken, op een creatieve manier na te gaan hoe we controle verliezen over onze data en hoe we op een andere manier met onze data kunnen omgaan.
</div>

<div class="text">
**Download het boek**: [pdf](http://publications.servus.at/2016-Behind_the_Smart_World/pdf/), [epub](http://publications.servus.at/2016-Behind_the_Smart_World/epub/)
**Lees online**: <http://publications.servus.at/2016-Behind_the_Smart_World/html/>
**Download het bronmateriaal**: <https://github.com/lafkon/servus.btsw>

De intelligente wereld, ontworpen door beleidsmakers, de reclamewereld, creatieve industrieën en overtuigende UX-ontwerpers. Ze houden ons een beeld voor van een wereld met schitterende nieuwe technologieën, apps die al onze dagelijkse problemen oplossen en slimme steden die grote hoeveelheden data verzamelen waarmee ooit alle problemen van de mensheid zullen worden opgelost. 
Wanneer we de 'smart world' met een licht kritische blik bekijken, merken we al snel dat onze blinkende gadgets sneller verouderen dan ooit. Ze transformeren in toxisch e-afval. Onze apps en slimme steden vormen een efficiënt controle-apparaat, waar niet aan te ontsnappen valt. We hebben geen idee wie onze data verzamelt, wie ermee aan de slag gaat of waar ze worden bewaard. Er bestaan zeker situaties waarvoor de intelligente wereld oplossingen biedt, maar tegelijkertijd creëert ze nieuwe problemen, zoals datalekken, data privacy, eigendom van data en elektronisch afval.   
In deze publicatie bieden onderzoekers en kunstenaars een kritische kijk op onze 'intelligente' wereld. Ze doen dat in drie onderdelen: het Bewaren van Data, het Wissen van Data en het Opnieuw doen Verschijnen van Data. Elk onderdeel bestaat uit theoretische teksten, gevolgd door strategieën om problematische machtsstructuren te ontdekken, op een creatieve manier na te gaan hoe we controle verliezen over onze data en hoe we op een andere manier met onze data kunnen omgaan. 

Redactie door [Kairus.org](http://kairus.org) – Linda Kronman, Andreas Zingerle
Coördinatie van het project door [Us(c)hi Reiter](http://www.firstfloor.org/ur)
Uitgegeven door [servus.at](http://servus.at) in het kader van het AMRO Research Lab 2015.

Met essays van Fieke Jansen ([Tactical Tech](https://tacticaltech.org)), [Ivar Veermäe](http://www.ivarveermae.com), [Emilio Vavarella](http://emiliovavarella.com), [Leo Selvaggio](http://leoselvaggio.com), [Marloes de Valk](http://pi.kuri.mu), [Research Team “Times of Waste”](http://www.ixdm.ch/portfolio/times-waste), [Stefan Tiefengraber](http://www.stefantiefengraber.com), [Dr. Michael Sonntag](http://www.fim.uni-linz.ac.at/staff/sonntag.htm) en interviews met [Audrey Samson](http://www.ideacritik.com) en [Michaela Lakova](http://mlakova.org).

De publication werkt gemaakt in een free software workflow, met gebruik van markdown, LaTeX en de GNU Bourne-Again SHell. Je bent vrij om dit werk te gebruiken, te bestuderen, te veranderen, te verbeteren, volgens de voorwaarden van de GNU General Public License.
</div>
