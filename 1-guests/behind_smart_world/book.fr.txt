title: 
Behind the Smart World – saving, deleting and resurfacing of data
----
year: 
2015
----
category: Guests
----
link: https://freeze.sh/_/2016/btsw/
----
summary: Dans cette publication, chercheurs et artistes développent un regard critique sur notre monde intelligent en trois parties: la Sauvegarde des Données, l'Effacement des Données et la Rééemergence des Données. Chaque partie est composée de textes théoriques suivis de stratégies qui exposent des structures de pouvoir problématiques, révèlent de façon créative comment nous avons perdu le contrôle sur nos données et montrent quelles attitudes nous pouvons développer vis-à-vis de celles-ci.
----
text:

**Téléchargez le livre**: [pdf](http://publications.servus.at/2016-Behind_the_Smart_World/pdf/), [epub](http://publications.servus.at/2016-Behind_the_Smart_World/epub/)
**Lisez en ligne**: <http://publications.servus.at/2016-Behind_the_Smart_World/html/>
**Téléchargez les sources**: <https://github.com/lafkon/servus.btsw>

Le monde intelligent; créé par les politiques, la publicité, les industries créatives et des développeurs UX persuasifs qui nous dessinent un monde régi par de nouvelles technologies brillantes, des applications capables de résoudre nos problèmes quotidiens et des villes intelligentes qui collectionnent nos données en promettant de résoudre tous les problèmes de l'humanité.
Si, par contre, on jette un coup d'oeil critique sur ce monde intelligent, on s'aperçoit que nos gadgets brillants deviennent obsolètes plus vite que jamais, se transformant en déchets toxiques; nos applications et nos villes intelligentes quant à elles constituent un appareil effectif de surveillance tout-puissant. Nous n'avons aucune idée qui collectionne nos données, qui y a accès ni où elles sont sauvegardées. S'il existe des cas que le monde intelligent arrive à résoudre, il pose dans le même temps de nouveaux problèmes sur la violation des données, leur privacité et leur propriété ainsi que sur les déchets électroniques.  
Dans cette publication, chercheurs et artistes développent un regard critique sur notre monde intelligent en trois parties: la Sauvegarde des Données, l'Effacement des Données et la Rééemergence des Données. Chaque partie est composée de textes théoriques suivis de stratégies qui exposent des structures de pouvoir problématiques, révèlent de façon créative comment nous avons perdu le contrôle sur nos données et montrent quelles attitudes nous pouvons développer vis-à-vis de celles-ci.

Edité par [Kairus.org](http://kairus.org) – Linda Kronman, Andreas Zingerle
Coordination du projet par [Us(c)hi Reiter](http://www.firstfloor.org/ur)
Publié par [servus.at](http://servus.at) dans le cadre du AMRO Research Lab 2015.

Avec des essais de Fieke Jansen ([Tactical Tech](https://tacticaltech.org)), [Ivar Veermäe](http://www.ivarveermae.com), [Emilio Vavarella](http://emiliovavarella.com), [Leo Selvaggio](http://leoselvaggio.com), [Marloes de Valk](http://pi.kuri.mu), [Research Team “Times of Waste”](http://www.ixdm.ch/portfolio/times-waste), [Stefan Tiefengraber](http://www.stefantiefengraber.com), [Dr. Michael Sonntag](http://www.fim.uni-linz.ac.at/staff/sonntag.htm) et des interviews avec [Audrey Samson](http://www.ideacritik.com) et [Michaela Lakova](http://mlakova.org).

Cette publication a été réalisée en utilisant markdown, LaTeX et le GNU Bourne-Again SHell. Vous êtes libres de l'utiliser, de l'étudier, de la modifier et de l'améliorer sous les conditions de la GNU General Public License.
