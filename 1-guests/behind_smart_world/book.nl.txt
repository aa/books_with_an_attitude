title: 
Behind the Smart World – saving, deleting and resurfacing of data
----
year: 
2015
----
category: Guests
----
link: https://freeze.sh/_/2016/btsw/
----
summary: In deze publicatie bieden onderzoekers en kunstenaars een kritische kijk op onze 'intelligente' wereld. Ze doen dat in drie onderdelen: het Bewaren van Data, het Wissen van Data en het Opnieuw doen Verschijnen van Data. Elk onderdeel bestaat uit theoretische teksten, gevolgd door strategieën om problematische machtsstructuren te ontdekken, op een creatieve manier na te gaan hoe we controle verliezen over onze data en hoe we op een andere manier met onze data kunnen omgaan. 
----
text:

**Download het boek**: [pdf](http://publications.servus.at/2016-Behind_the_Smart_World/pdf/), [epub](http://publications.servus.at/2016-Behind_the_Smart_World/epub/)
**Lees online**: <http://publications.servus.at/2016-Behind_the_Smart_World/html/>
**Download het bronmateriaal**: <https://github.com/lafkon/servus.btsw>

De intelligente wereld, ontworpen door beleidsmakers, de reclamewereld, creatieve industrieën en overtuigende UX-ontwerpers. Ze houden ons een beeld voor van een wereld met schitterende nieuwe technologieën, apps die al onze dagelijkse problemen oplossen en slimme steden die grote hoeveelheden data verzamelen waarmee ooit alle problemen van de mensheid zullen worden opgelost. 
Wanneer we de 'smart world' met een licht kritische blik bekijken, merken we al snel dat onze blinkende gadgets sneller verouderen dan ooit. Ze transformeren in toxisch e-afval. Onze apps en slimme steden vormen een efficiënt controle-apparaat, waar niet aan te ontsnappen valt. We hebben geen idee wie onze data verzamelt, wie ermee aan de slag gaat of waar ze worden bewaard. Er bestaan zeker situaties waarvoor de intelligente wereld oplossingen biedt, maar tegelijkertijd creëert ze nieuwe problemen, zoals datalekken, data privacy, eigendom van data en elektronisch afval.   
In deze publicatie bieden onderzoekers en kunstenaars een kritische kijk op onze 'intelligente' wereld. Ze doen dat in drie onderdelen: het Bewaren van Data, het Wissen van Data en het Opnieuw doen Verschijnen van Data. Elk onderdeel bestaat uit theoretische teksten, gevolgd door strategieën om problematische machtsstructuren te ontdekken, op een creatieve manier na te gaan hoe we controle verliezen over onze data en hoe we op een andere manier met onze data kunnen omgaan. 

Redactie door [Kairus.org](http://kairus.org) – Linda Kronman, Andreas Zingerle
Coördinatie van het project door [Us(c)hi Reiter](http://www.firstfloor.org/ur)
Uitgegeven door [servus.at](http://servus.at) in het kader van het AMRO Research Lab 2015.

Met essays van Fieke Jansen ([Tactical Tech](https://tacticaltech.org)), [Ivar Veermäe](http://www.ivarveermae.com), [Emilio Vavarella](http://emiliovavarella.com), [Leo Selvaggio](http://leoselvaggio.com), [Marloes de Valk](http://pi.kuri.mu), [Research Team “Times of Waste”](http://www.ixdm.ch/portfolio/times-waste), [Stefan Tiefengraber](http://www.stefantiefengraber.com), [Dr. Michael Sonntag](http://www.fim.uni-linz.ac.at/staff/sonntag.htm) en interviews met [Audrey Samson](http://www.ideacritik.com) en [Michaela Lakova](http://mlakova.org).

De publication werkt gemaakt in een free software workflow, met gebruik van markdown, LaTeX en de GNU Bourne-Again SHell. Je bent vrij om dit werk te gebruiken, te bestuderen, te veranderen, te verbeteren, volgens de voorwaarden van de GNU General Public License.
