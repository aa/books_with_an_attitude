title: 
16 Case stories, re-imagining the practice of layout
----
year: 
2012
----
category: Guests
----
link: 
----
summary: 
----
text:

**Download het boek**: <https://gitlab.constantvzw.org/lgru/co-position-catalog/raw/master/facsimile/digital-catalog.pdf>

*16 Case stories* is een catalogus die werd samengesteld als een manier om anders te denken over gereedschappen voor lay-out. Het bestaat uit verhalen rond specifieke gevallen, die zich in verleden en heden afspelen, richting verbeelding en speculatie. Het ‘camera-ready copy’ van dit boek werd gemaakt met een pen plotter: dit betekende dat we ook de workflow moesten herdenken, inclusief het gebruik van een speciaal lettertype-stack op basis van gravure-gereedschappen, zodat we lijngebaseerde letterytypes konden gebruiken. 

*Gereedschappen*: Active Archives, Libre Office, Inkscape, Custom Herschey Inkscape plugin, Roland pen plotter, laser printer, lijm en schaar. 
*Drukwerk*: Risograaf en Roland Pen Plotter.
*Samenstelling en redactie*: Colm O'Neill, Femke Snelting, Gijs de Heij, Pierre Huyghebaert, Pierre Marchand
*Ontwerp*: Colm O'Neill, Pierre Huyghebaert


