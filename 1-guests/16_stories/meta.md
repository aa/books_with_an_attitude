---
title: "16 Case stories, re-imagining the practice of layout"
---

<div class="year">2012</div>

<div class="category">Guests</div>

16 Case stories, re-imagining the practice of layout {lang=en}
===========================
<div class="summary">
*16 Case stories* is a catalogue put together as part of an attempt to rethink lay-out tools, and features case stories that range
from historical to present, on to the imaginary and speculative.
</div>

<div class="text">
**Download the book**: <https://gitlab.constantvzw.org/lgru/co-position-catalog/raw/master/facsimile/digital-catalog.pdf>

*16 Case stories* is a catalogue put together as part of an attempt to rethink lay-out tools, and features case stories that range
from historical to present, on to the imaginary and speculative. The ‘camera-ready copy’ of this book was created on a pen plotter: this has
required a rethinking of the workflow, including a special font stack built upon engraving tools, to be able to use stroke based fonts. 

*Tools*: Active Archives, Libre Office, Inkscape, Custom Herschey Inkscape plugin, Roland pen plotter, laser printer, glue and scissors. 
*Printing*: Risograph and Roland Pen Plotter.
*Compiled and edited by*: Colm O'Neill, Femke Snelting, Gijs de Heij, Pierre Huyghebaert, Pierre Marchand
*Design*: Colm O'Neill, Pierre Huyghebaert
</div>

16 Case stories, re-imagining the practice of layout {lang=fr}
===========================
<div class="summary">
*16 Case stories* est un catalogue qui rend compte des pistes d’un coloque requestionnant les outils et pratiques de la mise en page. Sa réalisation même a nécessité de repenser le workflow de mise en page habituel.
</div>

<div class="text">
**Téléchargez le livre**: <https://gitlab.constantvzw.org/lgru/co-position-catalog/raw/master/facsimile/digital-catalog.pdf>

*16 Case stories* est un catalogue qui rend compte des pistes d’un coloque requestionnant les outils et pratiques de la mise en page. Sa réalisation même a nécessité de repenser le workflow de mise en page habituel. 

*Outils*: Active Archives, Libre Office, Inkscape, plugin Inkscape Herschey sur mesure, plotter Roland, imprimante laser, colle et ciseaux. *Impression*: Risograph et plotter Roland.
*Compilé et édité par*: Colm O'Neill, Femke Snelting, Gijs de Heij, Pierre Huyghebaert, Pierre Marchand
*Mise-en-page*: Colm O'Neill, Pierre Huyghebaert
</div>

16 Case stories, re-imagining the practice of layout {lang=nl}
===========================
<div class="summary">

</div>

<div class="text">
**Download het boek**: <https://gitlab.constantvzw.org/lgru/co-position-catalog/raw/master/facsimile/digital-catalog.pdf>

*16 Case stories* is een catalogus die werd samengesteld als een manier om anders te denken over gereedschappen voor lay-out. Het bestaat uit verhalen rond specifieke gevallen, die zich in verleden en heden afspelen, richting verbeelding en speculatie. Het ‘camera-ready copy’ van dit boek werd gemaakt met een pen plotter: dit betekende dat we ook de workflow moesten herdenken, inclusief het gebruik van een speciaal lettertype-stack op basis van gravure-gereedschappen, zodat we lijngebaseerde letterytypes konden gebruiken. 

*Gereedschappen*: Active Archives, Libre Office, Inkscape, Custom Herschey Inkscape plugin, Roland pen plotter, laser printer, lijm en schaar. 
*Drukwerk*: Risograaf en Roland Pen Plotter.
*Samenstelling en redactie*: Colm O'Neill, Femke Snelting, Gijs de Heij, Pierre Huyghebaert, Pierre Marchand
*Ontwerp*: Colm O'Neill, Pierre Huyghebaert
</div>
