# https://manual.calibre-ebook.com/db_api.html
# run with calibre-debug command installed by calibre
#

from calibre.library import db
import json
from pathlib import Path
from calibre.ebooks.metadata.book.base import Metadata, SIMPLE_GET
from xml.etree import ElementTree as ET 
import html5lib

def innerHTML (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + u"".join([ET.tostring(x, method="html", encoding="unicode") for x in elt])

"""
add_books

Add the specified books to the library. Books should be an iterable of 2-tuples, each 2-tuple of the form (mi, format_map) where mi is a Metadata object and format_map is a dictionary of the form {fmt: path_or_stream}, for example: {'EPUB': '/path/to/file.epub'}.


SIMPLE_GET:
frozenset({'manifest', 'rights', 'uuid', 'timestamp', 'publication_type', 'thumbnail', 'spine', 'series', 'lpath', 'formats', 'title_sort', 'title', 'guide', 'authors', 'rating', 'author_sort', 'cover_data', 'comments', 'languages', 'toc', 'tags', 'book_producer', 'author_sort_map', 'pubdate', 'last_modified', 'db_id', 'size', 'publisher', 'cover', 'series_index', 'user_categories', 'mime', 'author_link_map', 'device_collections', 'application_id', 'identifiers'})

"""

# print (SIMPLE_GET)

db = db('calibre').new_api

# for book_id in db.all_book_ids():
# 	print (book_id)
# 	d = db.get_metadata(book_id)
# 	print (d)

with open("books_with_an_attitude.json") as fin:
    books_meta = json.load(fin)

books_to_add = []

for book in books_meta:
    bookpath = Path(book['path'])
    files = [x for x in bookpath.iterdir() if x.is_file()]
    covers = [x for x in files if x.suffix.lower() == ".jpg"]
    pubs = [x for x in files if x.suffix.lower() in (".epub", ".pdf") and x.name != "meta.epub"]
    # print (bookpath, pubs)
    cover_data = None
    if covers:
        with open(covers[0], "rb") as fin:
            cover_data = ('jpeg', fin.read())
            
    # Make the "format map" (format files as dict with TYPE as keys)
    formats = {}
    if pubs:
        for p in pubs:
            if p.suffix.lower() == ".epub":
                formats["EPUB"] = str(p)
                # formats["EPUB"] = f"{book['path']}/{p.name}"
            elif p.suffix.lower() == ".pdf":
                formats["PDF"] = str(p)
    if not formats:
        formats["EPUB"] = str(bookpath / "meta.epub")

    # Make the metadata object    
    md = Metadata(book['title_en'])
    md.pubdate = book['year']
    # is comments the description ?!
    if cover_data is not None:
        md.cover_data = cover_data

    tags = ["Books with an attitude"]
    if book['category']:
        tags.append(book['category'])
    md.tags = tags
    
    # get the meta HTML
    with open(bookpath / "meta.html") as fin:
        t = html5lib.parse(fin.read(), namespaceHTMLElements=False)
        body = t.find("./body")
        ET.tostring(body, method="html")
        meta_html = innerHTML(body)
        # print (meta_html)

    md.comments = meta_html
    
    books_to_add.append((md, formats))


results = db.add_books(books_to_add)    
print ("RESULTS of add_books")
print (results)

"""
Title               : Dune, Depolitization and Decolonizing the Future
Title sort          : Salman Sayyid
Author(s)           : Salman Sayyid [Sayyid]
Timestamp           : 2022-01-22T17:36:24+00:00
Published           : 0101-01-01T00:00:00+00:00
Comments            : <div>
<p><a href="https://constantvzw.org/site/-Stitch-And-Split,74-.html">Stich and Split</a></p></div>
"""
