<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8"/>
<title>File:Book.pdf - Mondothèque</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"File","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":6,"wgPageName":"File:Book.pdf","wgTitle":"Book.pdf","wgCurRevisionId":8487,"wgRevisionId":8487,"wgArticleId":1636,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"File:Book.pdf","wgRelevantArticleId":1636,"wgRequestId":"7ba1f13883f562dd37972f70","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgRestrictionUpload":[],"egMapsDebugJS":false,"egMapsAvailableServices":["googlemaps3","openlayers","leaflet"],"sfgAutocompleteValues":[],"sfgAutocompleteOnAllChars":false,"sfgFieldProperties":[],"sfgCargoFields":[],"sfgDependentFields":[],"sfgShowOnSelect":[],"sfgScriptPath":"/wiki/extensions/SemanticForms","edgValues":null,"sfgEDSettings":null,"wgWikiEditorEnabledModules":{"toolbar":true,"dialogs":true,"preview":false,"publish":false}});mw.loader.implement("user.options",function($,jQuery,require,module){mw.user.options.set({"variant":"en"});});mw.loader.implement("user.tokens",function ( $, jQuery, require, module ) {
mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});/*@nomin*/;

});mw.loader.load(["ext.smw.style","mediawiki.page.startup","skins.foreground.js"]);});</script>
<link rel="stylesheet" href="/wiki/load.php?debug=false&amp;lang=en&amp;modules=ext.smw.style%7Cext.smw.tooltip.styles&amp;only=styles&amp;skin=foreground"/>
<link rel="stylesheet" href="/wiki/load.php?debug=false&amp;lang=en&amp;modules=mediawiki.action.view.filepage%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.sectionAnchor%7Cskins.foreground.styles&amp;only=styles&amp;skin=foreground"/>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/wiki/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=foreground"/>
<script async="" src="/wiki/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=foreground"></script>
<meta name="generator" content="MediaWiki 1.27.1"/>
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0"/>
<link rel="ExportRDF" type="application/rdf+xml" title="File:Book.pdf" href="/wiki/index.php?title=Special:ExportRDF/File:Book.pdf&amp;xmlmime=rdf"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/wiki/opensearch_desc.php" title="Mondothèque (en)"/>
<link rel="EditURI" type="application/rsd+xml" href="https://www.mondotheque.be/wiki/api.php?action=rsd"/>
<link rel="copyright" href="http://www.gnu.org/copyleft/fdl.html"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body class="mediawiki ltr sitedir-ltr ns-6 ns-subject page-File_Book_pdf rootpage-File_Book_pdf skin-foreground action-view">
<div id='navwrapper'><!-- START FOREGROUNDTEMPLATE -->
		<nav class="top-bar">
			<ul class="title-area">
				<li class="name">
					<h1 class="title-name">
					<a href="/wiki/index.php?title=Main_Page">
										
					<div class="title-name" style="display: inline-block;">Mondothèque</div>
					</a>
					</h1>
				</li>
				<li class="toggle-topbar menu-icon">
					<a href="#"><span>Menu</span></a>
				</li>
			</ul>

		<section class="top-bar-section">

			<ul id="top-bar-left" class="left">
				<li class="divider"></li>
									<li class="has-dropdown active"  id='p-'>
					<a href="#"></a>
													<ul class="dropdown">
															</ul>
																		<li class="has-dropdown active"  id='p-Skimming.E2.88.92D.C3.A9pouiller'>
					<a href="#">Skimming−Dépouiller</a>
													<ul class="dropdown">
								<li id="n-Agents-.2B-actors"><a href="/wiki/index.php?title=Property:Person">Agents + actors</a></li><li id="n-Le-trait.C3.A9-de-documentation"><a href="/wiki/index.php?title=Trait%C3%A9_de_documentation">Le traité de documentation</a></li><li id="n-La-Pyramide"><a href="/wiki/index.php?title=La_Pyramide">La Pyramide</a></li><li id="n-Cross-readings"><a href="/wiki/index.php?title=Cross-readings">Cross-readings</a></li><li id="n-Timelines"><a href="/wiki/index.php?title=Category:Timeline">Timelines</a></li><li id="n-Categories"><a href="/wiki/index.php?title=Special:Categories">Categories</a></li><li id="n-Disambiguation"><a href="/wiki/index.php?title=Special:DisambiguationPages">Disambiguation</a></li><li id="n-Recent-Files"><a href="/wiki/index.php?title=Special:NewFiles">Recent Files</a></li>							</ul>
																		<li class="has-dropdown active"  id='p-Prototypes.E2.88.92Projets'>
					<a href="#">Prototypes−Projets</a>
													<ul class="dropdown">
								<li id="n-Not-dissimilar"><a href="/wiki/index.php?title=Not_dissimilar">Not dissimilar</a></li><li id="n-Scraping-culture"><a href="/wiki/index.php?title=Scraping_culture">Scraping culture</a></li><li id="n-Location.2C-location.2C-location"><a href="/wiki/index.php?title=Location,_location,_location">Location, location, location</a></li><li id="n-Nous-ne-vivons-pas-dans-ce-monde-l.C3.A0"><a href="/wiki/index.php?title=Nous_ne_vivons_pas_dans_ce_monde_l%C3%A0">Nous ne vivons pas dans ce monde là</a></li><li id="n-.C3.80-la-recherche-de-l.27UDC"><a href="/wiki/index.php?title=%C3%80_la_recherche_de_l%27UDC">À la recherche de l'UDC</a></li><li id="n-Times-in-Brussels"><a href="/wiki/index.php?title=Times_in_Brussels">Times in Brussels</a></li><li id="n-L.E2.80.99Afrique-aux-.E2.80.B9noirs.E2.80.BA"><a href="/wiki/index.php?title=L%E2%80%99Afrique_aux_%E2%80%B9noirs%E2%80%BA">L’Afrique aux ‹noirs›</a></li>							</ul>
																							</ul>

			<ul id="top-bar-right" class="right">
				<li class="has-form">
					<form action="/wiki/index.php" id="searchform" class="mw-search">
						<div class="row">
						<div class="small-12 columns">
							<input type="search" name="search" placeholder="Search" title="Search Mondothèque [f]" accesskey="f" id="searchInput"/>							<button type="submit" class="button search">Search</button>
						</div>
						</div>
					</form>
				</li>
				<li class="divider show-for-small"></li>
				<li class="has-form">

				<li class="has-dropdown active"><a href="#"><i class="fa fa-cogs"></i></a>
					<ul id="toolbox-dropdown" class="dropdown">
						<li id="t-whatlinkshere"><a href="/wiki/index.php?title=Special:WhatLinksHere/File:Book.pdf" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/index.php?title=Special:RecentChangesLinked/File:Book.pdf" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/index.php?title=Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/wiki/index.php?title=File:Book.pdf&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/wiki/index.php?title=File:Book.pdf&amp;oldid=8487" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/wiki/index.php?title=File:Book.pdf&amp;action=info" title="More information about this page">Page information</a></li><li id="t-smwbrowselink"><a href="/wiki/index.php?title=Special:Browse/File:Book.pdf" rel="smw-browse">Browse properties</a></li>						<li id="n-recentchanges"><a href="/wiki/index.php?title=Special:RecentChanges" title="Special:RecentChanges">Recent changes</a></li>						
					</ul>
				</li>

											<li>
																	<a href="/wiki/index.php?title=Special:UserLogin&amp;returnto=File%3ABook.pdf">Log in</a>
																</li>

				
			</ul>
		</section>
		</nav>
		</div>		
		<div id="page-content">
		<div class="row">
				<div class="large-12 columns">
				<!--[if lt IE 9]>
				<div id="siteNotice" class="sitenotice panel radius">Mondothèque may not look as expected in this version of Internet Explorer. We recommend you upgrade to a newer version of Internet Explorer or switch to a browser like Firefox or Chrome.</div>
				<![endif]-->

												</div>
		</div>

		<div id="mw-js-message" style="display:none;"></div>

		<div class="row">
				<div id="p-cactions" class="large-12 columns">
											<!--RTL -->
<ul id="actions">
<li id="ca-view" class="selected"><a href="/wiki/index.php?title=File:Book.pdf" redundant="1">View</a></li><li id="ca-viewsource"><a href="/wiki/index.php?title=File:Book.pdf&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></li><li id="ca-history"><a href="/wiki/index.php?title=File:Book.pdf&amp;action=history" title="Past revisions of this page [h]" accesskey="h">History</a></li><li id="ca-nstab-image" class="selected"><a href="/wiki/index.php?title=File:Book.pdf" title="View the file page [c]" accesskey="c">File</a></li><li id="ca-talk" class="new"><a href="/wiki/index.php?title=File_talk:Book.pdf&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page [t]" accesskey="t">Discussion</a></li></ul>
						<!--RTL -->
											<h4 class="namespace label">File</h4>					<div id="content">
					<h2  id="firstHeading" class="title">Book.pdf</h2>
					<h3 id="tagline">From Mondothèque</h3>					<h5 id="siteSub" class="subtitle"></h5>
					<div id="contentSub" class="clear_both"></div>
					<div id="bodyContent" class="mw-bodytext">
						<div id="mw-content-text"><ul id="filetoc"><li><a href="#file">File</a></li>
<li><a href="#filehistory">File history</a></li>
<li><a href="#filelinks">File usage</a></li></ul><div class="fullImageLink" id="file"><a href="/wiki/images/6/61/Book.pdf"><img alt="" src="/wiki/resources/assets/file-type-icons/fileicon-pdf.png" width="120" height="120" /></a></div>
<div class="fullMedia"><a href="/wiki/images/6/61/Book.pdf" class="internal" title="Book.pdf">Book.pdf</a> &#8206;<span class="fileInfo">(file size: 42.84 MB, MIME type: <span class="mime-type">application/pdf</span>)</span>
</div>
<div id="mw-imagepage-content" lang="en" dir="ltr" class="mw-content-ltr"><p>prototype for the publication
</p>
<!-- 
NewPP limit report
Cached time: 20220126070119
Cache expiry: 86400
Dynamic content: false
[SMW] In‐text annotation parser time: 0.000 seconds
CPU time usage: 0.004 seconds
Real time usage: 0.002 seconds
Preprocessor visited node count: 1/1000000
Preprocessor generated node count: 4/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 1/40
Expensive parser function count: 0/100
ExtLoops count: 0/100
-->

<!-- 
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 - -total
-->
</div><h2 id="filehistory">File history</h2>
<div id="mw-imagepage-section-filehistory">
<p>Click on a date/time to view the file as it appeared at that time.
</p>
<table class="wikitable filehistory">
<tr><th></th><th>Date/Time</th><th>Dimensions</th><th>User</th><th>Comment</th></tr>
<tr><td>current</td><td class='filehistory-selected' style='white-space: nowrap;'><a href="/wiki/images/6/61/Book.pdf">00:08, 3 August 2016</a></td><td> <span style="white-space: nowrap;">(42.84 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr">Final version, ready to print!</td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160802230849%21Book.pdf">13:53, 2 August 2016</a></td><td> <span style="white-space: nowrap;">(37.96 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160802125316%21Book.pdf">11:58, 2 August 2016</a></td><td> <span style="white-space: nowrap;">(37.96 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160802105857%21Book.pdf">22:58, 1 August 2016</a></td><td> <span style="white-space: nowrap;">(37.97 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160801215826%21Book.pdf">11:22, 20 July 2016</a></td><td> <span style="white-space: nowrap;">(38.04 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160720102219%21Book.pdf">07:27, 20 July 2016</a></td><td> <span style="white-space: nowrap;">(38.04 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160720062724%21Book.pdf">11:22, 18 July 2016</a></td><td> <span style="white-space: nowrap;">(38.04 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160718102212%21Book.pdf">17:07, 17 July 2016</a></td><td> <span style="white-space: nowrap;">(37.99 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160717160755%21Book.pdf">17:13, 14 July 2016</a></td><td> <span style="white-space: nowrap;">(37.87 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160714161333%21Book.pdf">11:15, 8 July 2016</a></td><td> <span style="white-space: nowrap;">(37.86 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160708101555%21Book.pdf">15:31, 5 July 2016</a></td><td> <span style="white-space: nowrap;">(37.86 MB)</span></td><td><a href="/wiki/index.php?title=User:FS&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:FS (page does not exist)">FS</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:FS" title="User talk:FS">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/FS" title="Special:Contributions/FS">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160705143112%21Book.pdf">15:33, 29 June 2016</a></td><td> <span style="white-space: nowrap;">(38.14 MB)</span></td><td><a href="/wiki/index.php?title=User:Acastro" title="User:Acastro" class="mw-userlink">Acastro</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:Acastro&amp;action=edit&amp;redlink=1" class="new" title="User talk:Acastro (page does not exist)">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/Acastro" title="Special:Contributions/Acastro">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160629143301%21Book.pdf">14:19, 28 June 2016</a></td><td> <span style="white-space: nowrap;">(38.07 MB)</span></td><td><a href="/wiki/index.php?title=User:Acastro" title="User:Acastro" class="mw-userlink">Acastro</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:Acastro&amp;action=edit&amp;redlink=1" class="new" title="User talk:Acastro (page does not exist)">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/Acastro" title="Special:Contributions/Acastro">contribs</a>)</span></span></td><td dir="ltr">fixed index links</td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160628131952%21Book.pdf">19:36, 29 April 2016</a></td><td> <span style="white-space: nowrap;">(49.05 MB)</span></td><td><a href="/wiki/index.php?title=User:Acastro" title="User:Acastro" class="mw-userlink">Acastro</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:Acastro&amp;action=edit&amp;redlink=1" class="new" title="User talk:Acastro (page does not exist)">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/Acastro" title="Special:Contributions/Acastro">contribs</a>)</span></span></td><td dir="ltr"></td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160429183656%21Book.pdf">13:54, 14 March 2016</a></td><td> <span style="white-space: nowrap;">(8.66 MB)</span></td><td><a href="/wiki/index.php?title=User:Acastro" title="User:Acastro" class="mw-userlink">Acastro</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:Acastro&amp;action=edit&amp;redlink=1" class="new" title="User talk:Acastro (page does not exist)">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/Acastro" title="Special:Contributions/Acastro">contribs</a>)</span></span></td><td dir="ltr">fonts included. Correct order.</td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160314125412%21Book.pdf">12:05, 27 February 2016</a></td><td> <span style="white-space: nowrap;">(1.2 MB)</span></td><td><a href="/wiki/index.php?title=User:Acastro" title="User:Acastro" class="mw-userlink">Acastro</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:Acastro&amp;action=edit&amp;redlink=1" class="new" title="User talk:Acastro (page does not exist)">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/Acastro" title="Special:Contributions/Acastro">contribs</a>)</span></span></td><td dir="ltr">no imgs, but titles and html structure,</td></tr>
<tr><td></td><td  style='white-space: nowrap;'><a href="/wiki/images/archive/6/61/20160227110531%21Book.pdf">18:23, 26 February 2016</a></td><td> <span style="white-space: nowrap;">(8.36 MB)</span></td><td><a href="/wiki/index.php?title=User:Acastro" title="User:Acastro" class="mw-userlink">Acastro</a><span style="white-space: nowrap;"> <span class="mw-usertoollinks">(<a href="/wiki/index.php?title=User_talk:Acastro&amp;action=edit&amp;redlink=1" class="new" title="User talk:Acastro (page does not exist)">talk</a> | <a href="/wiki/index.php?title=Special:Contributions/Acastro" title="Special:Contributions/Acastro">contribs</a>)</span></span></td><td dir="ltr">prototype for the publication</td></tr>
</table>

</div>
<ul>
<li id="mw-imagepage-upload-disallowed">You cannot overwrite this file.</li>
</ul>
<h2 id="filelinks">File usage</h2>
<div id="mw-imagepage-nolinkstoimage">
<p>There are no pages that link to this file.
</p>
</div>
</div><div class="printfooter">
Retrieved from "<a dir="ltr" href="https://www.mondotheque.be/wiki/index.php?title=File:Book.pdf&amp;oldid=8487">https://www.mondotheque.be/wiki/index.php?title=File:Book.pdf&amp;oldid=8487</a>"</div>
						<div class="clear_both"></div>
					</div>
		    	<div class="group"><div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div></div>
		    					</div>
		    </div>
		</div>

			<footer class="row">
				<div id="footer">
										<div id="footer-left" class="large-8 small-12 columns">
					<ul id="footer-left">
													<li id="footer-lastmod"> This page was last modified on 3 August 2016, at 00:08.</li>
													<li id="footer-copyright">Content is available under <a class="external" rel="nofollow" href="http://www.gnu.org/copyleft/fdl.html">GNU Free Documentation License 1.3 or later</a> unless otherwise noted.</li>
													<li id="footer-privacy"><a href="/wiki/index.php?title=Mondoth%C3%A8que:Privacy_policy" title="Mondothèque:Privacy policy">Privacy policy</a></li>
													<li id="footer-about"><a href="/wiki/index.php?title=Mondoth%C3%A8que:About" title="Mondothèque:About">About Mondothèque</a></li>
													<li id="footer-disclaimer"><a href="/wiki/index.php?title=Mondoth%C3%A8que:General_disclaimer" title="Mondothèque:General disclaimer">Disclaimers</a></li>
															
					</ul>
					</div>	
					<div id="footer-right-icons" class="large-4 small-12 columns">
					<ul id="poweredby">
													<li class="copyright">								<a href="http://www.gnu.org/copyleft/fdl.html"><img src="/wiki/resources/assets/licenses/gnu-fdl.png" alt="GNU Free Documentation License 1.3 or later" width="88" height="31"/></a>															</li>
													<li class="poweredby">								<a href="//www.mediawiki.org/"><img src="/wiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/wiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /wiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>																<a href="https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki"><img src="/wiki/extensions/SemanticMediaWiki/includes/../res/images/smw_button.png" alt="Powered by Semantic MediaWiki" width="88" height="31"/></a>															</li>
											</ul>
					</div>								
				</div>
			</footer>

		</div>
		
		<script>(window.RLQ=window.RLQ||[]).push(function(){mw.loader.state({"user":"ready","user.groups":"ready"});mw.loader.load(["ext.smw.tooltips","mediawiki.action.view.postEdit","site","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest"]);});</script><script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":219});});</script>
		</body>
		</html>

