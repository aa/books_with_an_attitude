---
title: "Mondotheque::a radiating book"
---

<div class="year">2016</div>
<div class="link"><a class="link" href="https://www.mondotheque.be/wiki/index.php?title=The_radiated_book">https://www.mondotheque.be/wiki/index.php?title=The_radiated_book</a></div>
<div class="category">Projects</div>

Mondotheque::a radiating book {lang=en}
===========================
<div class="summary">
In 2013 a band of artists, archivists and activists set out to unravel the many implications of a statement that routinely compared the Mundaneum in Mons to "Google on paper". This publication creates a moment, an incision into their collaborative process. It is an invitation into the entanglements of knowledge infrastructures, geo-politics and local histories.
</div>

<div class="text">
**Download pdf**: https://www.mondotheque.be/wiki/index.php?title=File:Book.pdf
**Printed copy: 20€ (excl delivery)**
**Interested to order your copy? Please email info@constantvzw.org**

In 1919 the Mundaneum occupied half of the majestic Cinquantenaire building in Brussels. The ambitious project was imagined by Paul Otlet and Henri Lafontaine as a mix between documentation center, conference venue and educational display. "The Mundaneum is an Idea, an Institution, a Method, a Body of workmaterials and collections, a Building, a Network." (Paul Otlet, Monde)
In 2013 a band of artists, archivists and activists set out to unravel the many implications of a statement that routinely compared the Mundaneum to "Google on paper". Under the moniker Mondotheque they organised discussions, reflections and workshops in various locations. A Semantic MediaWiki functioned as a platform for writing, editing and bookdesign.
The publication of *Mondotheque::a radiating book* creates a moment, an incision into this collaborative process. It is an invitation into the entanglements of knowledge infrastructures, geo-politics and local histories.

ISBN: 978-9-0811-4595-4

**With contributions by:** Nicolas Malevé, Michael Murtaugh, Dick Reckard, Natacha Roussel, Alexia de Visscher, Sînziana Păltineanu, Femke Snelting, Robert M. Ochshorn, Dusan Barok, Marcell Mars and many others.
*With the support of: Vlaamse GemeenschapsCommissie*
</div>

Mondotheque::a radiating book {lang=fr}
===========================
<div class="summary">
En 2013, un groupe d’artistes, d’archivistes et de militants ont éssayé de démêler les nombreuses implications d’une déclaration qui comparait le Mundaneum à "Google sur papier". Ce livre est un moment incisif dans leur processus de collaboration. Il est une invitation à pénétrer dans les enchevêtrements des infrastructures du savoir, de la géopolitique et de l’histoire locale.
</div>

<div class="text">
**Téléchargez le pdf**: https://www.mondotheque.be/wiki/index.php?title=File:Book.pdf
**Exemplaire imprimé: 20€ (excl frais de livraison)**
**Envie de commander votre exemplaire? Merci d'envoyer un email à info@constantvzw.org**

En 1919, le Mundaneum occupait la moitié du majestueux bâtiment du Cinquantenaire à Bruxelles. Le projet ambitieux a été imaginé par Paul Otlet et Henri Lafontaine comme un mélange entre centre de documentation, lieu de conférence et exposition educative. "Le Mundaneum est une Idée, une Institution, une Méthode, un Corps matériel de travaux et collections, un Edifice, un Réseau". (Paul Otlet, Monde)
En 2013, un groupe d’artistes, d’archivistes et de militants ont éssayé de démêler les nombreuses implications d’une déclaration qui comparait le Mundaneum à "Google sur papier". Sous le nom de Mondothèque, ils ont organisé des discussions, des moments de réflexions et des ateliers dans divers endroits. Un WikiMedia Semantique a fonctionné comme plate-forme pour l’écriture, l’édition et le graphisme.
La publication de Mondothèque::un livre irradiant est une étape, un moment incisif dans ce processus de collaboration. Il est une invitation à pénétrer dans les enchevêtrements des infrastructures du savoir, de la géopolitique et de l’histoire locale.

ISBN: 978-9-0811-4595-4

**Avec des contributions de:** Nicolas Malevé, Michael Murtaugh, Dick Reckard, Natacha Roussel, Alexia de Visscher, Sînziana Păltineanu, Femke Snelting, Robert M. Ochshorn, Dusan Barok, Marcell Mars et pleins d'autres.
*Avec le soutien de: Vlaamse GemeenschapsCommissie*
</div>

Mondotheque::a radiating book {lang=nl}
===========================
<div class="summary">
In 2013 ging een groep kunstenaars, archivarissen en activisten op zoek naar de vele implicaties van een routinematige vergelijking tussen het Mundaneum en “Google op papier”. Dit boek creëert een moment, een incisie in dit collectieve proces. Het is een uitnodiging om de verwikkelingen rond kennisinfrastructuur, geopolitiek en lokale geschiedenis van dichtbij te bekijken.
</div>

<div class="text">
**Download de pdf**: https://www.mondotheque.be/wiki/index.php?title=File:Book.pdf
**Gedrukt exemplaar: 20€ (excl verzendkosten)**
**Zin om een boek te bestellen? Stuur dan een email naar info@constantvzw.org**

In 1919 vulde het Mundaneum de helft van de majestueuze Jubelpark in Brussel. Het ambitieuze project werd bedacht door Paul Otlet en Henri Lafontaine als iets tussen documentatiecentrum, conferentieruimte en educatieve tentoonstelling in. "Het Mundaneum is een Idee, een Instelling, een Methode, een Verzameling werkmateriaal en collecties, een Gebouw, een Netwerk." (Paul Otlet, Monde)
In 2013 ging een groep kunstenaars, archivarissen en activisten op zoek naar de vele implicaties van een routinematige vergelijking tussen het Mundaneum en “Google op papier”. Onder de naam Mondotheque organiseerden zij discussies, reflecties en workshops op verschillende locaties. Een Semantische MediaWiki fungeerde als een platform om samen te schrijven, te redigeren en het boek vorm te geven. 
De verschijning van de publicatie *Mondotheek::een irradiërend boek* creëert een moment, een incisie in dit collectieve proces. Het is een uitnodiging om de verwikkelingen rond kennisinfrastructuur, geopolitiek en lokale geschiedenis van dichtbij te bekijken.

ISBN: 978-9-0811-4595-4

**Met bijdragen van:** Nicolas Malevé, Michael Murtaugh, Dick Reckard, Natacha Roussel, Alexia de Visscher, Sînziana Păltineanu, Femke Snelting, Robert M. Ochshorn, Dusan Barok, Marcell Mars en vele anderen.
*Met de steun van: Vlaamse GemeenschapsCommissie*
</div>
