title: 
Mondotheque::a radiating book
----
year: 
2016
----
category: Projets
----
link: https://www.mondotheque.be/wiki/index.php?title=The_radiated_book
----
summary: En 2013, un groupe d’artistes, d’archivistes et de militants ont éssayé de démêler les nombreuses implications d’une déclaration qui comparait le Mundaneum à "Google sur papier". Ce livre est un moment incisif dans leur processus de collaboration. Il est une invitation à pénétrer dans les enchevêtrements des infrastructures du savoir, de la géopolitique et de l’histoire locale.
----

text:

**Téléchargez le pdf**: https://www.mondotheque.be/wiki/index.php?title=File:Book.pdf
**Exemplaire imprimé: 20€ (excl frais de livraison)**
**Envie de commander votre exemplaire? Merci d'envoyer un email à info@constantvzw.org**

En 1919, le Mundaneum occupait la moitié du majestueux bâtiment du Cinquantenaire à Bruxelles. Le projet ambitieux a été imaginé par Paul Otlet et Henri Lafontaine comme un mélange entre centre de documentation, lieu de conférence et exposition educative. "Le Mundaneum est une Idée, une Institution, une Méthode, un Corps matériel de travaux et collections, un Edifice, un Réseau". (Paul Otlet, Monde)
En 2013, un groupe d’artistes, d’archivistes et de militants ont éssayé de démêler les nombreuses implications d’une déclaration qui comparait le Mundaneum à "Google sur papier". Sous le nom de Mondothèque, ils ont organisé des discussions, des moments de réflexions et des ateliers dans divers endroits. Un WikiMedia Semantique a fonctionné comme plate-forme pour l’écriture, l’édition et le graphisme.
La publication de Mondothèque::un livre irradiant est une étape, un moment incisif dans ce processus de collaboration. Il est une invitation à pénétrer dans les enchevêtrements des infrastructures du savoir, de la géopolitique et de l’histoire locale.

ISBN: 978-9-0811-4595-4

**Avec des contributions de:** Nicolas Malevé, Michael Murtaugh, Dick Reckard, Natacha Roussel, Alexia de Visscher, Sînziana Păltineanu, Femke Snelting, Robert M. Ochshorn, Dusan Barok, Marcell Mars et pleins d'autres.
*Avec le soutien de: Vlaamse GemeenschapsCommissie*






