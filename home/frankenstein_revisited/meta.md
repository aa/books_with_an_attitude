---
title: "Frankenstein Revisited"
---

<div class="year">2016</div>
<div class="link"><a class="link" href="http://www.algolit.net/frankenstein/">http://www.algolit.net/frankenstein/</a></div>
<div class="category">Algolit</div>

Frankenstein Revisited {lang=en}
===========================
<div class="summary">
Exactly 200 years after Mary Shelly’s publication of ’Frankenstein; or, the Modern Prometheus’, on the invitation of Roland Fischer, curator of Mad Scientist, a yearly festival in Bern, a small group of Python lovers created a new version of the book using literary chatbots.
</div>

<div class="text">
**Download the book**: 
http://www.algolit.net/frankenstein/ressources/publication/entire-frankie.pdf
**Download the material**: http://www.algolit.net/frankenstein/ressources/frankie_the_publication_sources.zip
**Download the scripts of the bots**: http://www.algolit.net/frankenstein/ressources/frankensteinbots.zip
**Create your own PJ-machine**: https://github.com/sarahgarcin/pj-machine

Artificial intelligence can be as complex as it can be simple. Exactly 200 years after Mary Shelly’s publication of ’Frankenstein; or, the Modern Prometheus’, on the invitation of Roland Fischer, curator of Mad Scientist, a yearly festival in Bern, a small group of Python lovers started working on literary chatbots based on or inspired by this gothic novel. 
They engaged in the conversation around artificial intelligence by sharing ideas, works and reflections about the topic reframed in the dispositive of the novel: they talked about Frankenstein the text, the inventor and the monster. Using one of the oldest chat protocols (IRC) they created bots and went into dialogue with them, discovered their reactions, scrutinized their feelings during the interaction.
During the Mad Scientist Festival in Bern, they organised a workspace amongst the stuffed animals inside the Natural History Museum for a booksprint of 3 days. They produced a new Frankenstein, a publication in which the interaction between text, humans and machines is not fictional content but the result of a collective process executed using the PJ Machine, a ’publishing jockey machine’ with arcade buttons.

**With contributions by:** Piero Bisello (art historian & writer), Sarah Garcin (graphic designer and programmer), James Bryan Graves (computer scientist), Anne Laforet (artist & critic), Catherine Lenoble (writer) and An Mertens (artist & writer).
</div>

Frankenstein Revisited {lang=fr}
===========================
<div class="summary">
Deux cents ans après la publication du roman de Mary Shelley, "Frankenstein ; ou le Prométheus Moderne", un petit groupe d’amateurs de Python littéraire a créé une nouvelle version du livre en utilisant une série de chatbots.
</div>

<div class="text">
**Téléchargez le livre **: 
http://www.algolit.net/frankenstein/ressources/publication/entire-frankie.pdf
**Téléchargez les sources **: http://www.algolit.net/frankenstein/ressources/frankie_the_publication_sources.zip
**Téléchargez les scripts des chatbots **: http://www.algolit.net/frankenstein/ressources/frankensteinbots.zip
**Créez votre PJ-machine **: https://github.com/sarahgarcin/pj-machine

L’intelligence artificielle peut être complexe tout comme elle peut être simple. Deux cents ans après la publication du roman de Mary Shelley, "Frankenstein ; ou le Prométheus Moderne", un petit groupe d’amateurs de Python littéraire a développé une série de chatbots à base de ce roman gothique. 
Dans le cadre de Mad Scientist, un festival annuel organisé par Roland Fischer à Bern, ils ont utilisé l’IRC, un protocole de "chat" âgé mais vivant, pour publier une nouvelle version de Frankenstein, une publication dans laquelle l’interaction entre le texte, les humain.es et les machines ne constituent pas le contenu fictif, mais sont le résultat d’un processus collectif, avec l’aide de la PJ-Machine, une "publication jockey machine" décorée de joyeux boutons en couleur.

**Avec des contributions de:** Piero Bisello (historien de l'art & écrivain), Sarah Garcin (designer et développeur de logiciels), James Bryan Graves (ingénieur en informatique), Anne Laforet (artiste & critique), Catherine Lenoble (écrivain) and An Mertens (artiste & écrivain).
</div>

Frankenstein Revisited {lang=nl}
===========================
<div class="summary">
Exact 200 jaar na de publicatie van Mary Shelly’s ’Frankenstein; of de Moderne Prometheus’, op uitnodiging van Roland Fischer, curator van Mad Scientist, een jaarlijks festival in Bern, realiseerde een kleine groep literaire Pythonliefhebbers een nieuwe versie van het boek aan de hand van een reeks chatbots.
</div>

<div class="text">
**Download de publicatie**: http://www.algolit.net/frankenstein/ressources/publication/entire-frankie.pdf
**Download het materiaal**: http://www.algolit.net/frankenstein/ressources/frankie_the_publication_sources.zip
**Download de dcripts van de chatbots**: http://www.algolit.net/frankenstein/ressources/frankensteinbots.zip
**Maak je eigen PJ-machine**: https://github.com/sarahgarcin/pj-machine

Artifiële intelligentie kan complex zijn, maar ook heel eenvoudig. Exact 200 jaar na de publicatie van Mary Shelly’s ’Frankenstein; of de Moderne Prometheus’, op uitnodiging van Roland Fischer, curator van Mad Scientist, een jaarlijks festival in Bern, ontwierp een kleine groep literaire Pythonliefhebbers een reeks chatbots op basis van deze gotische roman.
Tijdens een boeksprint van drie dagen tussen de opgezette dieren van het Natuurhistorisch Museum in Bern, publiceerde de groep een nieuwe Frankenstein, een boekje waarin de interactie tussen de tekst, de mensen en de machines geen fictieve inhoud zijn, maar het resultaat van een collectief proces, dankzij de hulp van de PJ-Machine, ’een publication jockey machine’, mét grote kleurige knoppen.

**Met bijdragen van:** Piero Bisello (kunsthistoricus & schrijver), Sarah Garcin (grafisch ontwerper en software-ontwikkelaar), James Bryan Graves (computerwetenschapper), Anne Laforet (kunstenaar & criticus), Catherine Lenoble (schrijver) and An Mertens (kunstenaar & schrijver).
</div>
