title: 
The Death of the Authors, 1941
----
year: 
2013
----
category: Algolit
----
link: http://publicdomainday.constantvzw.org/
----
summary: *The Death of the Authors, 1941* is een generatieve roman gemaakt met Python en nltk, op basis van teksten van Virginia Woolf, James Joyce, Rabindranath Tagore, Elizabeth Von Arnim, Sherwood Anderson en Henri Bergson, enkele van de auteurs die we welkom heetten in het publiek domein op 1-1-12.
----
text:

**Download de boekjes**: http://publicdomainday.constantvzw.org/#1941
**Gedrukte exemplaren (set van 3): 15€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

*The Death of the Authors, 1941* is een generatieve roman gemaakt met Python en nltk, op basis van teksten van Virginia Woolf, James Joyce, Rabindranath Tagore, Elizabeth Von Arnim, Sherwood Anderson en Henri Bergson, enkele van de auteurs die we welkom heetten in het publiek domein op 1-1-12.
Telkens je het script activeert op <http://publicdomainday.constantvzw.org/#1941> creëer je een nieuwe versie van de roman. Als een hommage aan het leven en de dood van auteurs van wie de lichamen in hetzelfde jaar één werden met de aarde, gidst elke versie je door de vier seizoenen. Die zijn samengesteld uit thematisch geselecteerde zinnen van hun bevrijde teksten.  
De selectie van de teksten die we gebruikten voor deze publicatie, werd sterk beïnvloed door hun aanwezigheid online. Werk van beroemde auteurs is eenvoudiger terug te vinden, Engelse werken en vertalingen zijn vaak beschikbaar als gratis e-books, dankzij initiatieven als The Gutenberg Project.

*The Death of the Authors* is een reeks generatieve creaties op basis van werk van auteurs in het publieke domein. Elk jaar opnieuw viert Constant Publiek Domein Dag in samenwerking met Cinema Nova, CRIDS en De Koninklijke Bibliotheek van België. Op die dag presenteren we telkens een nieuw afgeleid werk, als een manier om je te inspireren je werken digitaal toegankelijk te maken en werken van anderen te herontdekken. 
Elke nieuwjaarsdag verwelkomen we de enorme rijkdom aan kennis, informatie en schoonheid die vrij toegankelijk wordt voor de mensheid. Als een gevolg van het auteursrecht dat afloopt 70 jaar na de dood van de auteur, komen elk jaar duizenden werken vrij in het publieke domein. Dat betekent dat hun inhoud niet langer het bezit is van of gecontroleerd wordt door iemand. De werken worden gemeenschappelijke schatten, die door eenieder vrij kunnen worden gebruikt voor eender welk doel.


