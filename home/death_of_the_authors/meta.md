---
title: "The Death of the Authors, 1941"
---

<div class="year">2013</div>
<div class="link"><a class="link" href="http://publicdomainday.constantvzw.org/">http://publicdomainday.constantvzw.org/</a></div>
<div class="category">Algolit</div>

The Death of the Authors, 1941 {lang=en}
===========================
<div class="summary">
*The Death of the Authors, 1941* is a generative novel made with Python and nltk, based on texts by Virginia Woolf, James Joyce, Rabindranath Tagore, Elizabeth Von Arnim, Sherwood Anderson and Henri Bergson, some of the authors we welcomed in the public domain on 1-1-12.
</div>

<div class="text">
**Download the books**: http://publicdomainday.constantvzw.org/#1941
**Printed copies (set of 3): 15€ (excl shipping costs)**
**Interested to order a copy? Please email info@constantvzw.org**

*The Death of the Authors, 1941* is a generative novel made with Python and nltk, based on texts by Virginia Woolf, James Joyce, Rabindranath Tagore, Elizabeth Von Arnim, Sherwood Anderson and Henri Bergson, some of the authors we welcomed in the public domain on 1-1-12.
Every time you launch the script on <http://publicdomainday.constantvzw.org/#1941>, a different novel is created. As a hommage to the life and demise of authors whose bodies merged with earth in 1941, each version takes you through four seasons that are composed of thematically selected sentences from their liberated texts.
The selection of the texts used for this publication is very much influenced by the availability of works online. Famous authors are easy to find, English works and translations are often available as free e-books, thanks to an initiative such as The Gutenberg Project.

*The Death of the Authors* is a series of generative narratives based on work by authors in the public domain. Each year in January, when Constant celebrates Public Domain Day in collaboration with Cinema Nova, CRIDS and The Royal Library of Belgium, we present a new derivative work, as a way to inspire you to make public works electronically available and rediscover them.
Every year on New Year's Day, due to the expiration of copyright protection terms on works produced by authors who died seven decades earlier, thousands of works enter the public domain - that is, their content is no longer owned or controlled by anyone, but it rather becomes a common treasure, available for anyone to freely use for any purpose.
</div>

The Death of the Authors, 1941 {lang=fr}
===========================
<div class="summary">
*The Death of the Authors, 1941* est un roman génératif créé avec Python et nltk, basé sur des textes de Virginia Woolf, James Joyce, Rabindranath Tagore, Elizabeth Von Arnim, Sherwood Anderson et Henri Bergson, quelques-uns des auteurs qui sont rentrés dans le domaine public le 1-1-12.
</div>

<div class="text">
**Téléchargez les livres**: http://publicdomainday.constantvzw.org/#1941
**Exemplaires imprimés (set de 3) : 15€ (excl frais de livraison)**
**Envie de commander un exemplaire ? Merci d'envoyer un e-mail à info@constantvzw.org**

*The Death of the Authors, 1941* est un roman génératif créé avec Python et nltk, basé sur des textes de Virginia Woolf, James Joyce, Rabindranath Tagore, Elizabeth Von Arnim, Sherwood Anderson et Henri Bergson, quelques-uns des auteurs qui sont rentrés dans le domaine public le 1-1-12.
Chaque fois qu’on lance le script sur <http://publicdomainday.constantvzw.org/#1941>, un roman différent est créé. En tant qu'hommage à la vie et à la disparition des auteurs dont les corps se sont unis à la terre en 1941, chaque version vous fait voyager dans les quatre saisons composées de phrases des textes libérés, sélectionnées de façon thématique.
La sélection des oeuvres utilisées pour cette publication a été fort influencée par la disponibilité digitale des oeuvres. Les auteurs connus sont faciles à trouver, des oeuvres en anglais et des traductions sont souvent téléchargeables en tant que e-books gratuits, grâce à des initiatives telle que The Gutenberg Project.

*The Death of the Authors* est une série de narratifs génératifs basés sur des oeuvres d’auteurs en domaine public. Chaque année, lorsque Constant célèbre la Journée de Domaine Public en collaboration avec Cinéma Nova, CRIDS et La Bibliothèque Royale de Belgique, nous présentons une nouvelle oeuvre dérivée. C’est une manière de vous inspirer à publier vos oeuvres ouvertement et digitalement, et à découvrir celles des autres. 
Comme cadeau de Nouvel An, nous recevons chaque année une manne de savoir, d’information et de beauté qui devient accessible à tout un chacun. Au premier janvier, des milliers d’oeuvres d’auteurs décédés il y a des dizaines d’années entrent dans le domaine public. En effet, les oeuvres, à l’expiration de leur période de protection du droit d’auteur, deviennent un trésor public, que chacun peut apprécier et utiliser librement.
</div>

The Death of the Authors, 1941 {lang=nl}
===========================
<div class="summary">
*The Death of the Authors, 1941* is een generatieve roman gemaakt met Python en nltk, op basis van teksten van Virginia Woolf, James Joyce, Rabindranath Tagore, Elizabeth Von Arnim, Sherwood Anderson en Henri Bergson, enkele van de auteurs die we welkom heetten in het publiek domein op 1-1-12.
</div>

<div class="text">
**Download de boekjes**: http://publicdomainday.constantvzw.org/#1941
**Gedrukte exemplaren (set van 3): 15€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

*The Death of the Authors, 1941* is een generatieve roman gemaakt met Python en nltk, op basis van teksten van Virginia Woolf, James Joyce, Rabindranath Tagore, Elizabeth Von Arnim, Sherwood Anderson en Henri Bergson, enkele van de auteurs die we welkom heetten in het publiek domein op 1-1-12.
Telkens je het script activeert op <http://publicdomainday.constantvzw.org/#1941> creëer je een nieuwe versie van de roman. Als een hommage aan het leven en de dood van auteurs van wie de lichamen in hetzelfde jaar één werden met de aarde, gidst elke versie je door de vier seizoenen. Die zijn samengesteld uit thematisch geselecteerde zinnen van hun bevrijde teksten.  
De selectie van de teksten die we gebruikten voor deze publicatie, werd sterk beïnvloed door hun aanwezigheid online. Werk van beroemde auteurs is eenvoudiger terug te vinden, Engelse werken en vertalingen zijn vaak beschikbaar als gratis e-books, dankzij initiatieven als The Gutenberg Project.

*The Death of the Authors* is een reeks generatieve creaties op basis van werk van auteurs in het publieke domein. Elk jaar opnieuw viert Constant Publiek Domein Dag in samenwerking met Cinema Nova, CRIDS en De Koninklijke Bibliotheek van België. Op die dag presenteren we telkens een nieuw afgeleid werk, als een manier om je te inspireren je werken digitaal toegankelijk te maken en werken van anderen te herontdekken. 
Elke nieuwjaarsdag verwelkomen we de enorme rijkdom aan kennis, informatie en schoonheid die vrij toegankelijk wordt voor de mensheid. Als een gevolg van het auteursrecht dat afloopt 70 jaar na de dood van de auteur, komen elk jaar duizenden werken vrij in het publieke domein. Dat betekent dat hun inhoud niet langer het bezit is van of gecontroleerd wordt door iemand. De werken worden gemeenschappelijke schatten, die door eenieder vrij kunnen worden gebruikt voor eender welk doel.
</div>
