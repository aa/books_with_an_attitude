---
title: "CIAO/Tot Later"
---

<div class="year">2011</div>
<div class="link"><a class="link" href="http://www.paramoulipist.be/?p=804">http://www.paramoulipist.be/?p=804</a></div>
<div class="category">Projects</div>

CIAO/Tot Later {lang=en}
===========================
<div class="summary">
*CIAO/Tot Later - the author’s cut* is a novel written by An Mertens, typeset by [Open Source Publishing](http://osp.kitchen/), accompanied by a digital story narrated through webpages.
</div>

<div class="text">
**Download the book**: http://www.constantvzw.org/verlag/spip.php?article114
**Surf the story**: http://www.adashboard.org/ciaocu/

**Printed copy: 10€ (excl shipping costs)**
**Interested to order a copy? Please email info@constantvzw.org**

*CIAO/Tot Later - the author’s cut* is a novel written by An Mertens, typeset by [Open Source Publishing](http://osp.kitchen/), accompanied by a digital story narrated through webpages. 

*With the support of: Vlaamse Gemeenschapscommissie*
</div>

CIAO/Tot Later {lang=fr}
===========================
<div class="summary">
*CIAO/Tot Later - the author’s cut* est un roman, écrit par An Mertens, mis en page par [Open Source Publishing](http://osp.kitchen/) et accompagné d'une histoire digitale racontée à travers des pages web.
</div>

<div class="text">
**Téléchargez le livre**: http://www.constantvzw.org/verlag/spip.php?article114
**Surfez dans le récit**: http://www.adashboard.org/ciaocu/

**Exemplaire imprimé : 10€ (excl frais de livraison)**
**Envie de commander un exemplaire ? Merci d'envoyer un e-mail à info@constantvzw.org**

*CIAO/Tot Later - the author’s cut* est un roman, écrit par An Mertens, mis en page par [Open Source Publishing](http://osp.kitchen/) et accompagné d'une histoire digitale racontée à travers des pages web.
*Avec le support de: Vlaamse Gemeenschapscommissie*
</div>

CIAO/Tot Later {lang=nl}
===========================
<div class="summary">
*CIAO/Tot Later - the author’s cut* is een roman, geschreven door An Mertens, in een lay-out van [Open Source Publishing](http://osp.kitchen/). Het is ook een digitaal verhaal verteld doorheen webpagina’s.
</div>

<div class="text">
**Download het boek**: http://www.constantvzw.org/verlag/spip.php?article114
**Surf in het verhaal**: http://www.adashboard.org/ciaocu/

**Gedrukt exemplaar: 10€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

*CIAO/Tot Later - the author’s cut* is een roman, geschreven door An Mertens, in een lay-out van [Open Source Publishing](http://osp.kitchen/). Het is ook een digitaal verhaal verteld doorheen webpagina’s.
*Met de steun van: Vlaamse Gemeenschapscommissie*
</div>
