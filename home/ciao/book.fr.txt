title: 
CIAO/Tot Later
----
year: 
2011
----
category: Projets
----
link: http://www.paramoulipist.be/?p=804
----
summary: *CIAO/Tot Later - the author’s cut* est un roman, écrit par An Mertens, mis en page par [Open Source Publishing](http://osp.kitchen/) et accompagné d'une histoire digitale racontée à travers des pages web.
----
text:

**Téléchargez le livre**: http://www.constantvzw.org/verlag/spip.php?article114
**Surfez dans le récit**: http://www.adashboard.org/ciaocu/

**Exemplaire imprimé : 10€ (excl frais de livraison)**
**Envie de commander un exemplaire ? Merci d'envoyer un e-mail à info@constantvzw.org**

*CIAO/Tot Later - the author’s cut* est un roman, écrit par An Mertens, mis en page par [Open Source Publishing](http://osp.kitchen/) et accompagné d'une histoire digitale racontée à travers des pages web.
*Avec le support de: Vlaamse Gemeenschapscommissie*

