title: 
CIAO/Tot Later
----
year: 
2011
----
category: Projecten
----
link: http://www.paramoulipist.be/?p=804
----
summary: *CIAO/Tot Later - the author’s cut* is een roman, geschreven door An Mertens, in een lay-out van [Open Source Publishing](http://osp.kitchen/). Het is ook een digitaal verhaal verteld doorheen webpagina’s.
----
text:

**Download het boek**: http://www.constantvzw.org/verlag/spip.php?article114
**Surf in het verhaal**: http://www.adashboard.org/ciaocu/

**Gedrukt exemplaar: 10€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

*CIAO/Tot Later - the author’s cut* is een roman, geschreven door An Mertens, in een lay-out van [Open Source Publishing](http://osp.kitchen/). Het is ook een digitaal verhaal verteld doorheen webpagina’s.
*Met de steun van: Vlaamse Gemeenschapscommissie*


