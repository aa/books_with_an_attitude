---
title: "Machine Research"
---

<div class="year">2017</div>
<div class="link"><a class="link" href="https://aprja.net//issue/view/8319">https://aprja.net//issue/view/8319</a></div>
<div class="category">Projects</div>

Machine Research {lang=en}
===========================
<div class="summary">
This publication is about research on machines, research with machines, and research as a machine. It thus explores machinic perspectives to suggest a situation where the humanities are put into a critical perspective by machine driven ecologies, ontologies and epistemologies of thinking and acting. It takes the form of a newspaper and was distributed during Transmediale 2017.
</div>

<div class="text">
**Download the newspaper**: <https://transmediale.de/sites/default/files/public/node/publication/field_pubpdf/fid/58150/machine_research.pdf>

This publication is about Machine Research – research on machines, research with machines, and research as a machine. It thus explores machinic perspectives to suggest a situation where the humanities are put into a critical perspective by machine driven ecologies, ontologies and epistemologies of thinking and acting. It aims to engage research and artistic practice that takes into account the new materialist conditions implied by nonhuman techno-ecologies. 

These include new ontologies and intelligence such as machine learning, machine reading and listening (Geoff Cox, Sam Skinner & Nathan Jones, Brian House), systems-oriented perspectives to broadcast communication and conflict (John Hill, Dave Young), the ethics and aesthetics of autonomous systems (Maya Indira Ganesh, Maja Bak Herrie), and other post-anthropocentric reconsiderations of materiality and infrastructure (Abelardo Gil-Fournier, Etherbox interview).

*Contributors*: Christian Ulrik Andersen, Geoff Cox, Maya Indira Ganesh, Abelardo Gil-Fournier, Maja Bak Herrie, John Hill, Brian House, Nathan Jones, Nicolas Malevé, Rosa Menkman, An Mertens, Martino Morandi, Michael Murtaugh, Søren Pold, Søren Rasmussen, Renée Ridgway, Jara Rocha, Roel Roscam Abbing, Sam Skinner, Femke Snelting.
*Newspaper edited* by all authors.
*Published* by Digital Aesthetics Research Centre, Aarhus University, transmediale, and Constant Association for Art and Media.
*Design* by Sarah Garcin and Angeline Ostinelli
</div>

Machine Research {lang=fr}
===========================
<div class="summary">
Cette publication est dédiée à la recherche autour des machines, avec les machines et en tant que machine. Il s'agit d'une exploration des perspectives machiniques pour suggérer une situation dans laquelle les humanités sont mises en perspective de façon critique au sein des écologies, ontologies et épistémologies de pensée et d'action incentivées par les machines. Ce journal a été distribué lors de Transmediale 2017.
</div>

<div class="text">
**Téléchargez le journal**: <https://transmediale.de/sites/default/files/public/node/publication/field_pubpdf/fid/58150/machine_research.pdf>

Cette publication est dédiée à la recherche autour des machines, avec les machines et en tant que machine. Il s'agit d'une exploration des perspectives machiniques pour suggérer une situation dans laquelle les humanités sont mises en perspective de façon critique au sein des écologies, ontologies et épistémologies de pensée et d'action incentivées par les machines. Elle se concentre sur les recherches et les pratiques artistiques qui prennent en compte les nouvelles conditions matérialistes que les techno-écologies non-humaines impliquent.
 
Il comprend de nouvelles ontologies et intelligences comme l'apprentissage algorithmique, la lecture et l'écoute algorithmique (Geoff Cox, Sam Skinner & Nathan Jones, Brian House), des perspectives orientées par les systèmes qui émettent la communication et le conflit (John Hill, Dave Young), l'éthique et l'esthétique de systèmes autonomes (Maya Indira Ganesh, Maja Bak Herrie), et d'autres considérations post-anthropocentriques concernant la materialité et l'infrastructure (Abelardo Gil-Fournier, Etherbox interview).

*Contributeurs*: Christian Ulrik Andersen, Geoff Cox, Maya Indira Ganesh, Abelardo Gil-Fournier, Maja Bak Herrie, John Hill, Brian House, Nathan Jones, Nicolas Malevé, Rosa Menkman, An Mertens, Martino Morandi, Michael Murtaugh, Søren Pold, Søren Rasmussen, Renée Ridgway, Jara Rocha, Roel Roscam Abbing, Sam Skinner, Femke Snelting.
*Journal édité* par tous les auteurs.
*Publié* par Digital Aesthetics Research Centre, Aarhus University, transmediale, et Constant Association pour les Arts et les Médias.
*Mise-en-page* par Sarah Garcin et Angeline Ostinelli
</div>

Machine Research {lang=nl}
===========================
<div class="summary">
Een publicatie over onderzoek naar machines, met machines en als machine. Machine-perspectieven worden onderzocht om zo een situatie te suggereren, waarin de mensheid vanuit kritisch oogpunt wordt beschouwd door ecologieën, ontologieën en epistemologieën van denken en handelen, die door machines worden aangevoerd. De publicatie heeft de vorm van een krant en werd verspreid tijdens Transmediale 2017.
</div>

<div class="text">
**Download de krant**: <https://transmediale.de/sites/default/files/public/node/publication/field_pubpdf/fid/58150/machine_research.pdf>

Een publicatie over onderzoek naar machines, met machines en als machine. Machine-perspectieven worden onderzocht om zo een situatie te suggereren, waarin de mensheid vanuit kritisch oogpunt wordt beschouwd door ecologieën, ontologieën en epistemologieën van denken en handelen, die door machines worden aangevoerd. Dit gebeurt aan de hand van onderzoek en artistieke praktijken die rekening houden met de nieuwe materiële voorwaarden waaruit niet-humane techno-ecologiën bestaan. 

Het gaat ondermeer over nieuwe ontologieën en intelligenties, zoals zelflerende machines, lezende en luisterende machines (Geoff Cox, Sam Skinner & Nathan Jones, Brian House), systeem-georiënteerde perspectieven om communicatie en conflict uit te zenden (John Hill, Dave Young), de ethiek en de esthetiek van autonome systemen (Maya Indira Ganesh, Maja Bak Herrie), en andere post-antropocentrische beschouwingen op materialiteit en infrastructuur (Abelardo Gil-Fournier, Etherbox interview).

*Met bijdragen van*: Christian Ulrik Andersen, Geoff Cox, Maya Indira Ganesh, Abelardo Gil-Fournier, Maja Bak Herrie, John Hill, Brian House, Nathan Jones, Nicolas Malevé, Rosa Menkman, An Mertens, Martino Morandi, Michael Murtaugh, Søren Pold, Søren Rasmussen, Renée Ridgway, Jara Rocha, Roel Roscam Abbing, Sam Skinner, Femke Snelting.
*Redactie krant*: door alle auteurs.
*Uitgegeven door*: Digital Aesthetics Research Centre, Aarhus University, transmediale, en Constant Vereniging voor Kunst en Media.
*Lay-out*: Sarah Garcin en Angeline Ostinelli
</div>
