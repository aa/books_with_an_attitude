---
title: "Openbaar Schrijver"
---

<div class="year">2011</div>
<div class="link"><a class="link" href="http://www.paramoulipist.be/?cat=158">http://www.paramoulipist.be/?cat=158</a></div>
<div class="category">Projects</div>

Openbaar Schrijver {lang=en}
===========================
<div class="summary">
A catalogue for a public writer's office in February 2010. With the help of a series of computerscripts, the graphical designers of the designer collective [Open Source Publishing](http://osp.kitchen/) created different style combinations. All used fonts are developed by the designers, which turns the catalogue also in a letter proof.
</div>

<div class="text">
**Download the stories**: http://www.adashboard.org/openbaarschrijver/cataloog2.pdf
**Download the catalogue**: http://ospublish.constantvzw.org/works/valentine-scripting

**Printed copy: 5€ (excl shipping costs)**
**Interested to order a copy? Please email info@constantvzw.org**

In February 2010 one could pay a visit to the writer's office of Ana Foor in Bibliotheek Sans Souci and confess a story. Ana Foor would transform the words in a story, poem or letter to a loved one. 

From the catalogue one could choose the style for the content. The catalogue is an exhaustive collection of all possibilities one can think of using six font styles (whiskeyjazz, libertinage, cimatics…), three types of content (letter, poem, story) and nine 'flavours' or decorations (champagne, coral, pink…). For each of the 162 combinaties you could also choose the support (small book, loose pages, digital message with code). 

The catalogue is the result of an experiment with Open Office, a daily digital writing tool. With the help of a series of computerscripts, the graphical designers of the designer collective [Open Source Publishing](http://osp.kitchen/) created the different style combinations. All used fonts are developed by the designers, which turns the catalogue also in a letter proof.

*With the support of: Bibliotheek Sans Souci & Gemeente Elsene*
</div>

Openbaar Schrijver {lang=fr}
===========================
<div class="summary">
Un catalogue pour l'installation d'une écrivaine publique en février 2010. A l'aide d'une série de scripts d'ordinateur les graphistes du collectif de graphisme [Open Source Publishing](http://osp.kitchen/) ont créé les différentes combinaisons de style. Toutes les typographies utilisées sont développées par les graphistes mêmes. Ainsi, ce catalogue est aussi un échantillon de leur travail.
</div>

<div class="text">
**Téléchargez les récits**: http://www.adashboard.org/openbaarschrijver/cataloog2.pdf
**Téléchargez le catalgoue**: http://ospublish.constantvzw.org/works/valentine-scripting

**Exemplaire imprimé : 5€ (excl frais de livraison)**
**Envie de commander un exemplaire? Merci d'envoyer un e-mail à info@constantvzw.org**

En février 2010 Ana Foor s'était installée en tant qu'écrivaine publique à la bibliothèque Sans Souci à Ixelles. Les visiteurs lui confiaient leurs expériences, qu'elle transformait en récit, poème ou lettre destinés à leurs aimés (partenaire, enfant, membre de la famille, ami(e)...).

Les visiteurs pouvaient choisir le style et la forme de leur message. Le catalogue était une collection exhaustive de toutes les possibilités imaginables de six typographies (whiskeyjazz, libertinage, cimatics…), trois types de contenu (lettre, poème, récit) et neuf ‘herbes’ ou décorations (champagne, corale, rose…). Pour chacune des 162 combinations la personne pouvait choisir le support (des pages, un petit livre ou un message digitalisé et verrouilé avec un code).

Le catalogue était le résultat d'une expérience avec Open Office, un outil d'écriture quotidien. A l'aide d'une série de scripts d'ordinateur les graphistes du collectif de graphisme [Open Source Publishing](http://osp.kitchen/) ont créé les différentes combinaisons de style. Toutes les typographies utilisées sont développées par les graphistes mêmes. Ainsi, ce catalogue est aussi un échantillon de leur travail. 

*Avec le soutien de: Bibliotheek Sans Souci & Gemeente Elsene*
</div>

Openbaar Schrijver {lang=nl}
===========================
<div class="summary">
Een catalogus voor een publiek schrijverskabinet in februari 2010. Met behulp van een serie computerscripts produceerden de grafisten van het ontwerpcollectief [Open Source Publishing](http://osp.kitchen/) de verschillende stijlcombinaties waaruit je kan kiezen. Alle gebruikte fonts zijn ontwikkeld door de grafisten zelf, en zo is deze staalkaart ook een letterproef.
</div>

<div class="text">
**Download de verhalen**: http://www.adashboard.org/openbaarschrijver/cataloog2.pdf
**Download de catalogus**: http://ospublish.constantvzw.org/works/valentine-scripting

**Gedrukt exemplaar: 5€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

In februari 2010 kon je een bezoek brengen aan het schrijverskabinet van de Bibliotheek Sans Souci en je verhaal toevertrouwen aan Ana Foor. Haar hart zweeg, terwijl jouw woorden vloeiden door haar pen voor een verhaal, gedicht of brief voor een geliefde persoon (partner, kind, familielid, vriend(in)...).

In de catalogus kon je stijl, inhoud en vorm kiezen voor je gevoel. Het is een exhaustieve verzameling van alle mogelijkheden die je kan bedenken met zes lettertypes (whiskeyjazz, libertinage, cimatics…), drie types van inhoud (brief, gedicht, verhaal) en negen ‘kruiden’ of ook ‘versiersels’ (champagne, koraal, roze…). Voor elk van de 162 combinaties kan je bovendien kiezen of je die op losse vellen aan je geliefde wil presenteren, in een kleine boekvorm of als digitale boodschap met code.

De catalogus was het resultaat van een experiment met Open Office, een dagdagelijks digitaal schrijfgereedschap. Met behulp van een serie computerscripts produceerden de grafisten van het ontwerpcollectief [Open Source Publishing](http://osp.kitchen/) de verschillende stijlcombinaties waaruit je kan kiezen. Alle gebruikte fonts zijn ontwikkeld door de grafisten zelf, en zo is deze staalkaart ook een letterproef. 

*Met de steun van: Bibliotheek Sans Souci & Gemeente Elsene*
</div>
