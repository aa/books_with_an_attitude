---
title: "Data Workers"
---

<div class="year">2019</div>
<div class="link"><a class="link" href="https://www.algolit.net/index.php/Data_Workers">https://www.algolit.net/index.php/Data_Workers</a></div>
<div class="category">Algolit</div>

Data Workers {lang=en}
===========================
<div class="summary">
Data Workers is the catalogue of an exhibition of algoliterary works organised at the Mundaneum in March 2019. It collects stories told from an ‘algorithmic storyteller point of view’. The exhibition was created by members of Algolit, a group from Brussels involved in artistic research on algorithms and literature.
</div>

<div class="text">
**Download the publication**: https://www.algolit.net/index.php/File:Data-workers.en.publication.pdf

**Download the sources**: https://git.vvvvvvaria.org/mb/data-workers-publication

**Printed copy: 10€ (excl shipping costs)**
**Order a physical copy by sending an email to info@constantvzw.org**

Companies create artificial intelligence (AI) systems to serve, entertain, record and learn about humans. The work of these machinic entities is usually hidden behind interfaces and patents. In the exhibition, algorithmic storytellers left their invisible underworld to become interlocutors. 

The data workers operate in different collectives. Each collective represents a stage in the design process of a machine learning model: there are the Writers, the Cleaners, the Informants, the Readers, the Learners and the Oracles. The boundaries between these collectives are not fixed; they are porous and permeable. At times, Oracles are also Writers. At other times Readers are also Oracles. Robots voice experimental literature, while algorithmic models read data, turn words into numbers, make calculations that define patterns and are able to endlessly process new texts ever after.

The exhibition foregrounded data workers who impact our daily lives, but are either hard to grasp and imagine or removed from the imagination altogether. It connected stories about algorithms in mainstream media to the storytelling that is found in technical manuals and academic papers. Robots were invited to engage in dialogue with human visitors and vice versa. In this way we might understand our respective reasonings, demystify each other's behaviour, encounter multiple personalities, and value our collective labour. 

It was also a tribute to the many machines that Paul Otlet and Henri La Fontaine imagined for their Mundaneum, showing their potential but also their limits. 

Texts: Cristina Cochior, Sarah Garcin, Gijs de Heij, An Mertens, François Zajéga, Louise Dekeuleneer, Florian Van de Weyer, Laetitia Trozzi, Rémi Forte, Guillaume Slizewicz.

Translations & proofreading: deepl.com, Michel Cleempoel, Elodie Mugrefya, Emma Kraak, Patrick Lennon.

Lay-out & cover: Manetta Berends
</div>

Data Workers {lang=fr}
===========================
<div class="summary">
Data Workers est le catalogue d'une exposition d'œuvres algolittéraires, organisée au Mundaneum à Mons en mars 2019. Elle exposait des histoires racontées d'un point de vue 'narratif algorithmique'. L'exposition était une création des membres d'Algolit, un groupe bruxellois impliqué dans la recherche artistique sur les algorithmes et la littérature.
</div>

<div class="text">
**Téléchargez le livre **: https://www.algolit.net/index.php/File:Data-workers.fr.publication.pdf

**Téléchargez les sources **: https://git.vvvvvvaria.org/mb/data-workers-publication

**Exemplaire imprimé : 10€ (excl frais de livraison)**
**Achetez un exemplaire imprimé en envoyant un e-mail à info@constantvzw.org**


Les entreprises créent des intelligences artificielles pour servir, divertir, enregistrer et connaître les humains. Le travail de ces entités machiniques est généralement dissimulé derrière des interfaces et des brevets. Dans l'exposition, les conteurs algorithmiques quittaient leur monde souterrain invisible pour devenir des interlocuteurs. 

Les 'data workers' opèrent dans des collectifs différents. Chaque collectif représente une étape dans le processus de conception d'un modèle d'apprentissage automatique : il y a les Écrivains, les Nettoyeurs, les Informateurs, les Lecteurs, les Apprenants et les Oracles. Les robots donnent leurs voix à la littérature expérimentale, les modèles algorithmiques lisent des données, transforment des mots en nombres, calculent des modèles et traitent en boucle de nouveaux textes et ceci à l'infini.

L'exposition mettait au premier plan les 'data workers' qui ont un impact sur notre vie quotidienne, mais qui sont difficiles à saisir ou à imaginer. Elle établissait un lien entre les récits sur les algorithmes dans les médias grand public et les histoires racontées dans les manuels techniques et les articles universitaires. Les robots étaient invités à dialoguer avec les visiteurs humains et vice versa. De cette façon, nous pourrions comprendre nos raisonnements respectifs, démystifier nos comportements, rencontrer nos personnalités multiples et valoriser notre travail collectif. 

C'était aussi un hommage aux nombreuses machines que Paul Otlet et Henri La Fontaine ont imaginées pour leur Mundaneum, en montrant leur potentiel mais aussi leurs limites. 

Textes: Cristina Cochior, Sarah Garcin, Gijs de Heij, An Mertens, François Zajéga, Louise Dekeuleneer, Florian Van de Weyer, Laetitia Trozzi, Rémi Forte, Guillaume Slizewicz.

Traductions & relectures: deepl.com, Michel Cleempoel, Elodie Mugrefya, Patrick Lennon, Emma Kraak.

Mise-en-page & couverture: Manetta Berends
</div>

Data Workers {lang=nl}
===========================
<div class="summary">
Data Workers is de catalogus van een tentoonstelling van algoliteraire werken die plaatsvond in maart 2019 in het Mundaneum. Het verzamelt verhalen die worden verteld vanuit een 'algoritmisch vertelstandpunt'. De tentoonstelling werd ontworpen door leden van Algolit, een Brusselse groep die zich bezighoudt met artistiek onderzoek naar algoritmes en literatuur.
</div>

<div class="text">
**Download de publicatie**: https://www.algolit.net/index.php/File:Data-workers.en.publication.pdf

**Download het bronmateriaal**: https://git.vvvvvvaria.org/mb/data-workers-publication

**Gedrukt exemplaar: 10€ (excl verzendingskosten)**
**Zin in een exemplaar? Stuur dan een e-mail naar info@constantvzw.org**

Bedrijven creëren artificiële intelligenties om mensen te dienen, te vermaken, ons gedrag te leren kennen en vast te leggen. Het werk van deze machinale wezens is meestal verborgen achter interfaces en patenten. In deze tentoonstelling verlieten algoritmische verhalenvertellers hun onzichtbare onderwereld om gesprekspartners te worden.

De datawerkers opereren in verschillende collectieven. Elk collectief vertegenwoordigt een fase in het ontwerpproces van een machine learning model. Zo zijn er de Schrijvers, de Schoonmakers, de Informanten, de Lezers, de Studenten en de Orakels. Robots zijn de stemmen in experimentele literatuur, algoritmische modellen lezen data, zetten woorden om in getallen, maken berekeningen, zoeken naar patronen en kunnen zo eindeloos nieuwe teksten verwerken.

De tentoonstelling bracht datawerkers op de voorgrond die ons dagelijks leven beïnvloeden, maar moeilijk te vatten of voor te stellen zijn. Het verbond verhalen over algoritmes in de reguliere media met verhalen in technische handleidingen en academische artikelen. Robots werden uitgenodigd om in dialoog te gaan met menselijke bezoekers en vice versa. Op deze manier konden we onze respectievelijke redeneringen begrijpen, elkaars gedrag demystifiëren, diverse persoonlijkheden ontmoeten en onze collectieve arbeid in de verf zetten.

De expo was ook een eerbetoon aan de vele machines die Paul Otlet en Henri La Fontaine voor hun Mundaneum bedachten. Ze toonde hun potentieel maar ook hun grenzen.

Teksten: Cristina Cochior, Sarah Garcin, Gijs de Heij, An Mertens, François Zajéga, Louise Dekeuleneer, Florian Van de Weyer, Laetitia Trozzi, Rémi Forte, Guillaume Slizewicz.

Vertalingen & proeflezen: deepl.com, Michel Cleempoel, Elodie Mugrefya, Emma Kraak, Patrick Lennon.

Lay-out & cover: Manetta Berends
</div>
