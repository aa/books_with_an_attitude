title: 
Spreekt U Sint-Gillis? Parlez-vous Saint-Gillois?
----
year: 
2015
----
category: Projets
----
link: http://parlezvous1060.be/p/publication/
----
summary: Ce livre récolte des mots, réels ou inventés, entendus dans le quartier Bosnie à Saint-Gilles, Bruxelles. Ce sont des mots extra-ordinaires qui se superposent au français et au néerlandais, ces mots que nous fusionnons, hybridons, inventons, 'acronymons’. Les mots restent disponibles dans une base de données sonore en ligne. L’ensemble des enregistrements audio dressent un portrait sonore de ce quartier, tel qu’il existait de septembre 2013 à septembre 2015. 
----
text:

**Téléchargez le livre**: http://parlezvous1060.be/media/publication.pdf
**Exemplaire imprimé : 10€ (excl frais de livraison)**
**Envie de commander un exemplaire? Merci d'envoyer un e-mail à info@constantvzw.org**

*Parlez-vous Saint-Gillois?* est un projet de récolte de mots, réels ou inventés, entendus dans le quartier Bosnie à Saint-Gilles, Bruxelles. Ce sont des mots EXTRA-ORDINAIRES qui se superposent au français et au néerlandais, ces mots que nous fusionnons, hybridons, inventons, 'acronymons’.
Les mots restent disponibles dans une base de données sonore en ligne. L’ensemble des enregistrements audio dressent un portrait sonore de ce quartier, tel qu’il existait de septembre 2013 à septembre 2015. 

La publication *Parlez-vous Saint-Gillois?* est à la fois un dictionnaire qui reprend les mots qui composent la base de données en ligne et un espace d’expérimentations qui s’appuie sur la langue parlée dans le quartier multiculturel Bosnie ainsi que sur les créations, les activités et les rencontres qui ont eu lieu pendant le projet. 

La graphiste Alexia De Visscher et la coordinatrice du projet Clémentine Delahaut ont réalisé un diagramme cartographique pour illustrer les relations existant entre les mots, les gens et les lieux à proximité. Peter Westenberg aborde dans l’introduction les questions de la multiculturalité et du plurilinguisme à Bruxelles et comment elles ont été envisagées dans le projet Saint-Gillois. Différents artistes et écrivains tels que Jara Rocha, Madeleine Aktypi, Rafaella Houlstan- Hasaert, Dr Lichic, Mathieu Berger, ont contribué à la publication.

ISBN: 978-9-0811-4594-7

*Avec le soutien de: La Région de Bruxelles-Capitale et la Commune de Saint-Gilles*


