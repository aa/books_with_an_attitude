---
title: "Spreekt U Sint-Gillis? Parlez-vous Saint-Gillois?"
---

<div class="year">2015</div>
<div class="link"><a class="link" href="http://parlezvous1060.be/p/publication/">http://parlezvous1060.be/p/publication/</a></div>
<div class="category">Projects</div>

Spreekt U Sint-Gillis? Parlez-vous Saint-Gillois? {lang=en}
===========================
<div class="summary">
This book collects the words, real or invented, that were captured in the neighbourhood Bosnia in Saint-Gilles. The extra-ordinary words are overlapping in French and Dutch, words we merge, hybridize, invent, “acronymise”. The audio recordings remain online in a sound database. The collection provides a sound portrait of this multicultural neighbourhood as it existed from September 2013 till September 2015.
</div>

<div class="text">
**Download the book**: http://parlezvous1060.be/media/publication.pdf
**Printed copy: 10€ (excl shipping costs)**
**Interested to order a copy? Please email info@constantvzw.org**

*Parlez-vous Saint-Gillois?* collected the words, real or invented, that were captured in the neighbourhood Bosnia in Saint-Gilles. The EXTRA-ORDINARY words are overlapping in French and Dutch, words we merge, hybridize, invent, “acronymise”. 
The audio recordings remain online in a sound database. The collection provides a sound portrait of this multicultural neighbourhood as it existed from September 2013 till September 2015. 

The publication *Parlez-vous Saint-Gillois?* is a dictionary with words from the database, as well as a space for experimentations based on the spoken language of the Bosnia neighbourhood, and the different creations, activities and encounters that took place during the project. 

Project coordinator Clémentine Delahaut and graphic designer Alexia De Visscher realised a cartographical diagramme that illustrates the relationships between the words, people and places nearby. In the introduction Peter Westenberg extends on the questiosn of multiculturality and plurilinguism in Brussels and how they have been appraoched in the project of Parlez-vous Saint-Gillois. Several artists and writers contributed to the publication, such as Jara Rocha, Madeleine Aktypi, Rafaella Houlstan-Hasaert, Dr Lichic, Mathieu Berger.

ISBN: 978-9-0811-4594-7

*With the support of: La Région de Bruxelles-Capitale and la Commune de Saint-Gilles*
</div>

Spreekt U Sint-Gillis? Parlez-vous Saint-Gillois? {lang=fr}
===========================
<div class="summary">
Ce livre récolte des mots, réels ou inventés, entendus dans le quartier Bosnie à Saint-Gilles, Bruxelles. Ce sont des mots extra-ordinaires qui se superposent au français et au néerlandais, ces mots que nous fusionnons, hybridons, inventons, 'acronymons’. Les mots restent disponibles dans une base de données sonore en ligne. L’ensemble des enregistrements audio dressent un portrait sonore de ce quartier, tel qu’il existait de septembre 2013 à septembre 2015.
</div>

<div class="text">
**Téléchargez le livre**: http://parlezvous1060.be/media/publication.pdf
**Exemplaire imprimé : 10€ (excl frais de livraison)**
**Envie de commander un exemplaire? Merci d'envoyer un e-mail à info@constantvzw.org**

*Parlez-vous Saint-Gillois?* est un projet de récolte de mots, réels ou inventés, entendus dans le quartier Bosnie à Saint-Gilles, Bruxelles. Ce sont des mots EXTRA-ORDINAIRES qui se superposent au français et au néerlandais, ces mots que nous fusionnons, hybridons, inventons, 'acronymons’.
Les mots restent disponibles dans une base de données sonore en ligne. L’ensemble des enregistrements audio dressent un portrait sonore de ce quartier, tel qu’il existait de septembre 2013 à septembre 2015. 

La publication *Parlez-vous Saint-Gillois?* est à la fois un dictionnaire qui reprend les mots qui composent la base de données en ligne et un espace d’expérimentations qui s’appuie sur la langue parlée dans le quartier multiculturel Bosnie ainsi que sur les créations, les activités et les rencontres qui ont eu lieu pendant le projet. 

La graphiste Alexia De Visscher et la coordinatrice du projet Clémentine Delahaut ont réalisé un diagramme cartographique pour illustrer les relations existant entre les mots, les gens et les lieux à proximité. Peter Westenberg aborde dans l’introduction les questions de la multiculturalité et du plurilinguisme à Bruxelles et comment elles ont été envisagées dans le projet Saint-Gillois. Différents artistes et écrivains tels que Jara Rocha, Madeleine Aktypi, Rafaella Houlstan- Hasaert, Dr Lichic, Mathieu Berger, ont contribué à la publication.

ISBN: 978-9-0811-4594-7

*Avec le soutien de: La Région de Bruxelles-Capitale et la Commune de Saint-Gilles*
</div>

Spreekt U Sint-Gillis? Parlez-vous Saint-Gillois? {lang=nl}
===========================
<div class="summary">
Dit boek verzamelt bijzondere woorden uit de wijk Bosnië in Sint-Gillis, Brussel. Het gaat om bestaande en verzonnen woorden, acroniemen en hybrides die het Franse en/of Nederlandse woordenboek overstijgen. De woorden zijn opgenomen in een online geluidsdatabank. Samen vormen ze een auditief portret van de wijk zoals die bestond tussen september 2013 tot september 2015.
</div>

<div class="text">
**Download het boek**: http://parlezvous1060.be/media/publication.pdf
**Gedrukt exemplaar: 10€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

*Spreekt U Sint-Gillis?* was een project dat bijzondere woorden verzamelde uit de wijk Bosnië in Sint-Gillis, Brussel. Het ging om bestaande en verzonnen woorden, acroniemen en hybrides die het Franse en/of Nederlandse woordenboek overstijgen.
De woorden zijn verzameld in een online geluidsdatabank. Samen vormen ze een auditief portret van de wijk zoals die bestond tussen september 2013 tot september 2015. 

De publicatie is tegelijkertijd een onorthodox woordenboek, een manier om meerdere facetten van het project te ontdekken, en ze biedt ruimte voor experiment en speculatie rondom gesproken taal in de multiculturele buurt Bosnië.

Grafisch vormgeefster Alexia De Visscher en coördinatrice Clémentine Delahaut realiseerden een diagram dat de relaties zichtbaar maakt tussen de woorden, de mensen en de plekken in de buurt. In de inleiding legt Peter Westenberg verband tussen het project en de meertaligheid en multiculturaliteit in Brussel. Verschillende kunstenaars en schrijvers, waaronder Jara Rocha, Madeleine Aktypi, Rafaella Houlstan­Hasaert, Dr Lichic, en Mathieu Berger droegen bij aan de publicatie. 

ISBN: 978-9-0811-4594-7

*Met de steun van: Het Brussels Hoofdstedelijk Gewest en de Gemeente Sint-Gillis*
</div>
