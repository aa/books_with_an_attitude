---
title: "The Techno-Galactic Guide to Software Observation"
---

<div class="year">2018</div>
<div class="link"><a class="link" href="http://constantvzw.org/site/-The-Technogalactic-Software-Observatory-.html">http://constantvzw.org/site/-The-Technogalactic-Software-Observatory-.html</a></div>
<div class="category">Worksession</div>

The Techno-Galactic Guide to Software Observation {lang=en}
===========================
<div class="summary">
The Techno-Galactic Guide to Software Observation gathers methods from the Techno-Galactic Software Observatory, a worksession that took place in June 2017. The guide was collectively edited by Carlin Wing, Martino Morandi, Peggy Pierrot, Anita Burato, Christoph Haag, Michael Murtaugh, Femke Snelting, Seda Gürses and designed by Christoph Haag.
</div>

<div class="text">
**Download pdf**: http://observatory.constantvzw.org/tgsoguide_1806051351.pdf + http://observatory.constantvzw.org/tgsoguide-COVER_1806051351.pdf
**Sources**: https://gitlab.constantvzw.org/ch/observatory.guide
**Printed copy: 5€** (excl delivery)
Interested to order your copy? Please email info@constantvzw.org

The Techno-Galactic Guide to Software Observation gathers methods from the Techno-Galactic Software Observatory, a worksession that took place in June 2017.

"The Techno-Galactic Guide to Software Observation is the obsessive fantasy of optimization turned on its head and stuck to the ceiling of a self-reflecting elevator. It is the ultimate book, with almost 300 pages of dos and don'ts, of forgotten histories and un-inevitable futures, of mindful agile actions and improvisational service architectures, of any and all things that you can and cannot imagine needing in a techno-galaxy."
☆☆☆☆☆☆

**Compiled by:** Peggy Pierrot, Martino Morandi, Anita Burato, Christoph Haag, Michael Murtaugh, Femke Snelting, Seda Gürses
**Contributors:** Manetta Berends, Željko Blace, Larisa Blazic, Freyja van den Boom, Anna Carvalho, Loup Cellard, Joana Chicau, Cristina Cochior, Pieter Heremans, Joak aka Joseph Knierzinger, Jogi Hofmüller, Becky Kazansky, Anne Laforet, Ricardo Lafuente, Michaela Lakova, Hans Lammerant, Silvio Lorusso, Mia Melvaer, An Mertens, Lidia Pereira, Donatella Portoghese, Luis Rodil-Fernandez, Natacha Roussel, Andrea di Serego Alighieri, Lonneke van der Velden, Ruben van de Ven, Kym Ward, Wendy Van Wynsberghe and Peter Westenberg.

ISBN: 978-9-0811-4596-1
</div>

The Techno-Galactic Guide to Software Observation {lang=fr}
===========================
<div class="summary">
De Techno-Galactische Gids voor Software Observatie verzamelt methodes die werden ontwikkeld tijdens de Techno-Galactic Software Observatory, een werksessie die plaatsvond in juni 2017. De publicatie werd collectief geredigeerd door Carlin Wing, Martino Morandi, Peggy Pierrot, Anita Burato, Michael Murtaugh, Femke Snelting, Seda Gürses en ontworpen door Christoph Haag.
</div>

<div class="text">
**Téléchargez le livre**: http://observatory.constantvzw.org/tgsoguide_1806051351.pdf + http://observatory.constantvzw.org/tgsoguide-COVER_1806051351.pdf
**Sources**: https://gitlab.constantvzw.org/ch/observatory.guide

**Exemplaire imprimé : 5€ (plus les frais de livraison)**
**Envie de commander un exemplaire? Merci d'envoyer un e-mail à info@constantvzw.org**

Le Guide Techno-Galactique de l’Observation du Logiciel rassemble des méthodes du Techno-Galactic Software Observatory, une session de travail qui a eu lieu en juin 2017.

"Le Guide Techno-Galactique de l’Observation du Logiciel est le fantasme obsessionnel de l’optimisation tourné sur sa tête et collé au plafond d’un ascenseur auto-réfléchissant. C’est le livre ultime, avec près de 300 pages de choses à faire et à ne pas faire, d’histoires oubliées et d’avenirs fatidiques, certains,prédéterminés , d’actes subtiles et la mise en place de services improvisés, de toutes les choses que vous pouvez et ne pouvez pas vous imaginer dans une techno-galaxie."
☆☆☆☆☆☆

**Edité par:** Peggy Pierrot, Martino Morandi, Anita Burato, Christoph Haag, Michael Murtaugh, Femke Snelting, Seda Gürses
**Avec des contributions de:** Manetta Berends, Željko Blace, Larisa Blazic, Freyja van den Boom, Anna Carvalho, Loup Cellard, Joana Chicau, Cristina Cochior, Pieter Heremans, Joak aka Joseph Knierzinger, Jogi Hofmüller, Becky Kazansky, Anne Laforet, Ricardo Lafuente, Michaela Lakova, Hans Lammerant, Silvio Lorusso, Mia Melvaer, An Mertens, Lidia Pereira, Donatella Portoghese, Luis Rodil-Fernandez, Natacha Roussel, Andrea di Serego Alighieri, Lonneke van der Velden, Ruben van de Ven, Kym Ward, Wendy Van Wynsberghe and Peter Westenberg.

ISBN: 978-9-0811-4596-1
</div>

The Techno-Galactic Guide to Software Observation {lang=nl}
===========================
<div class="summary">
De Techno-Galactische Gids voor Software Observatie verzamelt methodes die werden ontwikkeld tijdens de Techno-Galactic Software Observatory, een werksessie die plaatsvond in juni 2017. De publicatie werd collectief geredigeerd door Carlin Wing, Martino Morandi, Peggy Pierrot, Anita Burato, Michael Murtaugh, Femke Snelting, Seda Gürses en ontworpen door Christoph Haag.
</div>

<div class="text">
**Download de pdf**: http://observatory.constantvzw.org/tgsoguide_1806051351.pdf + http://observatory.constantvzw.org/tgsoguide-COVER_1806051351.pdf
**Bronbestanden**: https://gitlab.constantvzw.org/ch/observatory.guide
**Gedrukt exemplaar: 5€** (excl bezorgkosten)
Om een exemplaar te bestellen, email info@constantvzw.org

De Techno-Galactische Gids voor Software Observatie verzamelt methodes die werden ontwikkeld tijdens de Techno-Galactic Software Observatory, een werksessie die plaatsvond in juni 2017.

"De Techno-Galactische Gids voor Software Observatie zet de obsessieve fantasie van optimalisatie op zijn kop en plakt hem vast aan het plafond van een zelfreflecterende lift. Het is het ultieme boek, met bijna 300 pagina’s aan dos en don’ts, aan vergeten geschiedenissen en onafwendbare toekomsten, aan mindful agile actions en service architectuur improvisaties, aan alles wat je wel en niet kunt voorstellen nodig te hebben in een techno-galaxie."
☆☆☆☆☆☆

**Samengesteld door:** Peggy Pierrot, Martino Morandi, Anita Burato, Christoph Haag, Michael Murtaugh, Femke Snelting, Seda Gürses
**Met bijdragen van:** Manetta Berends, Željko Blace, Larisa Blazic, Freyja van den Boom, Anna Carvalho, Loup Cellard, Joana Chicau, Cristina Cochior, Pieter Heremans, Joak aka Joseph Knierzinger, Jogi Hofmüller, Becky Kazansky, Anne Laforet, Ricardo Lafuente, Michaela Lakova, Hans Lammerant, Silvio Lorusso, Mia Melvaer, An Mertens, Lidia Pereira, Donatella Portoghese, Luis Rodil-Fernandez, Natacha Roussel, Andrea di Serego Alighieri, Lonneke van der Velden, Ruben van de Ven, Kym Ward, Wendy Van Wynsberghe and Peter Westenberg.

ISBN: 978-9-0811-4596-1
</div>
