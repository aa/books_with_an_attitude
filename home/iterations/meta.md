---
title: "Iterations"
---

<div class="year">2020</div>
<div class="link"><a class="link" href="https://iterations.space/">https://iterations.space/</a></div>
<div class="category">Projects</div>

Iterations {lang=en}
===========================
<div class="summary">
Iterations was a series of residencies, exhibitions, research meetings and artistic exchanges committed to investigate the future of artistic collaboration in digitally networked contexts. Iterations is now also a publication that creates and inspires new concepts and openings for works yet to be developed.
</div>

<div class="text">
**Download the publication**: https://iterations.space/publication/

**Printed copy: 10€ (excl shipping costs)**
**Order a physical copy by sending an email to info@constantvzw.org**

Iterations was a series of residencies, exhibitions, research meetings and artistic exchanges committed to investigate the future of artistic collaboration in digitally networked contexts.
Iterations is now also a publication that creates and inspires new concepts and openings for works yet to be developed.

With contributions by: Kym Ward, Behuki, common ground, Rica Rickson, Collective Conditions, spideralex. An insert booklet invites the reader to engage in a practice of ’X-dexing’ to discover and process transversal content.

Editors: Jara Rocha and Manetta Berends
Design: Manetta Berends

Iterations is a project by Constant, Hangar, esc MKL
More info on the project: Iterations.space

copyleft FAL, Constant 2020
</div>

Iterations {lang=fr}
===========================
<div class="summary">
Itérations était une série de résidences, d’expositions, de réunions de recherche et d’échanges artistiques visant à étudier l’avenir de la collaboration artistique dans des contextes numériques. Itérations est maintenant aussi une publication qui propose et inspire de nouveaux concepts et de nouvelles ouvertures pour des œuvres encore à développer.
</div>

<div class="text">
**Téléchargez le livre **: https://iterations.space/publication/

**Exemplaire imprimé : 10€ (excl frais de livraison)**
**Achetez un exemplaire imprimé en envoyant un e-mail à info@constantvzw.org**


Itérations était une série de résidences, d’expositions, de réunions de recherche et d’échanges artistiques visant à étudier l’avenir de la collaboration artistique dans des contextes numériques.
Itérations est maintenant aussi une publication qui propose et inspire de nouveaux concepts et de nouvelles ouvertures pour des œuvres encore à développer.

Avec les contributions de : Kym Ward, Behuki, Common Ground, Rica Rickson, Collective Conditions, spideralex. Un livret d’insertion invite le lecteur à s’engager dans une pratique de "X-dexing", un outil pour pour découvrir et traiter des contenus de façon transversale.

Rédactrices : Jara Rocha et Manetta Berends
Conception graphique : Manetta Berends

Itérations est un projet de Constant, Hangar, esc MKL
Plus d’informations sur le projet : iterations.space

Licence : copyleft FAL, Constant 2020
</div>

Iteraties {lang=nl}
===========================
<div class="summary">
Iteraties was een serie residenties, tentoonstellingen, onderzoeksmeetings en artistieke uitwisselingen gewijd aan de toekomst van artistieke samenwerking in digitale genetwerkte contexten. Iteraties is nu ook een publicatie die nieuwe concepten voorstelt en inspiratie geeft aan nog te ontwikkelen toekomstig werk.
</div>

<div class="text">
**Download de publicatie**: https://iterations.space/publication/

**Gedrukt exemplaar: 10€ (excl verzendingskosten)**
**Zin in een exemplaar? Stuur dan een e-mail naar info@constantvzw.org**

Iteraties was een serie residenties, tentoonstellingen, onderzoeksmeetings en artistieke uitwisselingen gewijd aan de toekomst van artistieke samenwerking in digitale genetwerkte contexten. Iteraties is nu ook een publicatie die nieuwe concepten voorstelt en inspiratie geeft aan nog te ontwikkelen toekomstig werk.

Met bijdragen van: Kym Ward, Behuki, common ground, Rica Rickson, Collective Conditions, spideralex. Een bijgesloten brochure nodigt de lezer uit voor een ’X-dexing’ kruiselingse lezing van het boek, om transversale content te ontdekken en genereren.

Redactie: Jara Rocha and Manetta Berends
Ontwerp: Manetta Berends

Iterations is een project van Constant, Hangar, esc MKL

copyleft FAL, Constant 2020
</div>
