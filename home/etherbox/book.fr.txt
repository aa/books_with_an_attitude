title: 
Networks of Ones Own #1 : Etherbox
----
year: 
2018
----
category: Session de Travail
----
link: https://networksofonesown.constantvzw.org/
----
summary: Ce premier épisode de la série Networks Of One’s Own comprend une version du logiciel, des manuels, de la documentation et des essais et d’Etherbox, une constellation de pratiques et d’outils développés au travers et autour de Constant.
----
text:

**Créez votre Etherbox: https://networksofonesown.constantvzw.org/etherbox/manual.html#inside-the-box**
**Partagez votre expérience ou set-up: merci d'envoyer un mail à info@constantvzw.org**

Ce premier épisode de la série Networks Of One’s Own comprend une version du logiciel, des manuels, de la documentation et des essais et d’Etherbox, une constellation de pratiques et d’outils développés au travers et autour de Constant. Etherbox répond à des problématiques pratiques dans le cadre d'une collaboration dans les espaces physiques avec des outils numériques. En parallèle, il est devenu une plateforme de réflexion sur les technologies de réseau, sur la manière de documenter les processus artistiques et sur les processus de collaboration.

D'un point de vue pratique, Etherbox offre une réponse à plusieurs frustrations : la fragilité des ressources en ligne, le fait que le travail nécessaire pour maintenir les ressources en ligne après une session collective était souvent négligé ou simplement supposé se poursuivre à l'infini, les limites de la bande passante Internet lorsque l'on travaille avec plusieurs personnes utilisant etherpad-lite. L'objectif du projet était donc de présenter une infrastructure à la fois visible, adaptable et située. Depuis ses débuts en 2013, de nombreux groupes ont eu le plaisir d'écrire et de documenter ensemble, de remettre en question les configurations d'infrastructure ou simplement de partager des fichiers sans effort. Grâce à ces multiples expériences, Etherbox s'est développé comme outil pour faciliter les discussions et les pratiques collaboratives de manière indépendante des collaborateurs de Constant. Il est maintenant prêt à se transmuter en d'autres imaginations en réseau.

Networks Of One’s Own est une publication périodique para-nodale, écrite collectivement en réseau. Chaque épisode est considéré comme la « publication » d’un module de logiciel, contextualisée dans sa pratique spécifique. La série vise à documenter un ensemble d’outils, d’expériences, de modes de travail divers en termes de temporalité, granularité et persistance.


