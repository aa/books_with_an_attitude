---
title: "Networks of Ones Own #1 : Etherbox"
---

<div class="year">2018</div>
<div class="link"><a class="link" href="https://networksofonesown.constantvzw.org/">https://networksofonesown.constantvzw.org/</a></div>
<div class="category">Worksession</div>

Networks of Ones Own #1 : Etherbox {lang=en}
===========================
<div class="summary">
This first episode in the series Networks Of One's Own includes a software release, manuals, documentation and essays of Etherbox, a constellation of practices and tools developed in and around Constant.
</div>

<div class="text">
**Create your own Etherbox:** https://networksofonesown.constantvzw.org/etherbox/manual.html#inside-the-box
**Share your experience or set-up:** please email info@constantvzw.org

This first episode in the series Networks Of One's Own includes a software release, manuals, documentation and essays of Etherbox, a constellation of practices and tools developed in and around Constant. Etherbox responds to practical issues when collaborating in physical spaces with digital tools. In parallel, it became a platform to reflect on network technologies, on how to document artistic processes and on processes of collaboration.

On a practical level, Etherbox was a response to several frustrations: the fragility of online resources, the fact that the labour necessary to maintain resources online after a collective session was often overlooked or simply assumed to continue indefinitely, the limitations of Internet bandwidth when working with many people using etherpad-lite. The goal of the project was thus to present an infrastructure that is both visible and situated. Since it's early beginnings in 2013, many groups have enjoyed the pleasure of writing and documenting together, questioning infrastructure set-ups or simply sharing files effortlessly. Through these multiple experiences, Etherbox has grown up to facilitate discussions and collaborative practices independent from Constant collaborators. Now it is ready to transmutate into other networked imaginations.

Networks Of One's Own is a para-nodal1 periodic publication that is itself collectively written in a network. Each of the episodes is thought of as the 'release' of a specific software stack, contextualised in its specific practice. The series aims to document a set of tools, experiences, ways of working that are diverse in terms of their temporality, granularity and persistence.
</div>

Networks of Ones Own #1 : Etherbox {lang=fr}
===========================
<div class="summary">
Ce premier épisode de la série Networks Of One’s Own comprend une version du logiciel, des manuels, de la documentation et des essais et d’Etherbox, une constellation de pratiques et d’outils développés au travers et autour de Constant.
</div>

<div class="text">
**Créez votre Etherbox: https://networksofonesown.constantvzw.org/etherbox/manual.html#inside-the-box**
**Partagez votre expérience ou set-up: merci d'envoyer un mail à info@constantvzw.org**

Ce premier épisode de la série Networks Of One’s Own comprend une version du logiciel, des manuels, de la documentation et des essais et d’Etherbox, une constellation de pratiques et d’outils développés au travers et autour de Constant. Etherbox répond à des problématiques pratiques dans le cadre d'une collaboration dans les espaces physiques avec des outils numériques. En parallèle, il est devenu une plateforme de réflexion sur les technologies de réseau, sur la manière de documenter les processus artistiques et sur les processus de collaboration.

D'un point de vue pratique, Etherbox offre une réponse à plusieurs frustrations : la fragilité des ressources en ligne, le fait que le travail nécessaire pour maintenir les ressources en ligne après une session collective était souvent négligé ou simplement supposé se poursuivre à l'infini, les limites de la bande passante Internet lorsque l'on travaille avec plusieurs personnes utilisant etherpad-lite. L'objectif du projet était donc de présenter une infrastructure à la fois visible, adaptable et située. Depuis ses débuts en 2013, de nombreux groupes ont eu le plaisir d'écrire et de documenter ensemble, de remettre en question les configurations d'infrastructure ou simplement de partager des fichiers sans effort. Grâce à ces multiples expériences, Etherbox s'est développé comme outil pour faciliter les discussions et les pratiques collaboratives de manière indépendante des collaborateurs de Constant. Il est maintenant prêt à se transmuter en d'autres imaginations en réseau.

Networks Of One’s Own est une publication périodique para-nodale, écrite collectivement en réseau. Chaque épisode est considéré comme la « publication » d’un module de logiciel, contextualisée dans sa pratique spécifique. La série vise à documenter un ensemble d’outils, d’expériences, de modes de travail divers en termes de temporalité, granularité et persistance.
</div>

Networks of Ones Own #1 : Etherbox {lang=nl}
===========================
<div class="summary">
Deze eerste aflevering in de serie Networks Of One’s Own bevat een software release, handleidingen, documentatie en essays van Etherbox. Etherbox is een gereedschap ontwikkeld in en rond Constant.
</div>

<div class="text">
**Maak je eigen Etherbox:** https://networksofonesown.constantvzw.org/etherbox/manual.html#inside-the-box
**Deel je ervaring of set-up:** stuur een bericht naar info@constantvzw.org

Deze eerste aflevering in de serie Networks Of One’s Own bevat een software release, handleidingen, documentatie en essays van Etherbox. Etherbox is een gereedschap ontwikkeld in en rond Constant. Het biedt een antwoord op praktische vragen voor documentatie en het delen van bestanden, wanneer een groep mensen samenwerkt in een ruimte met digitaal materiaal. Daarnaast werd het ook een platform om na te denken over genetwerkte technologieën, het documenteren van artistieke processen en processen van samenwerken.

Heel concreet biedt Etherbox antwoord op een aantal frustraties: de kwetsbaarheid van online bronmateriaal, de gespeletenheid om digitaal materiaal te delen via servers in de VS, terwijl je elkaar koffie schenkt in dezelfde ruimte; het feit dat werkuren die nodig zijn om documentatie van collectieve sessies online te onderhouden vaak over het hoofd worden gezien, of dat er eenvoudigweg vanuit gegaan wordt dat archivering tot in het oneindige gebeurt, de beperkingen van een Internetverbinding wanneer je met veel mensen tegelijk op etherpad-lite werkt. Het doel van dit project was dus het ontwikkelen van een infrastructuur die tegelijk zichtbaar, manipuleerbaar en gesitueerd is. Sinds de eerste versies in 2013 vond een heel aantal groepen plezier in het samen schrijven en documenteren, het zoeken naar verschillende infrastructurele set-ups, of eenvoudigweg het delen van bestanden. Dankzij deze meervoudige ervaringen groeide Etherbox tot een gereedschap dat discussies en collectieve praktijken faciliteert, onafhankelijk van Constantleden. Nu is het klaar om te transformeren in andere genetwerkte verbeeldingen. 

Networks Of One’s Own is een periodieke paranodale publicatie die collectief geschreven wordt in een netwerk. Elke episode wordt gezien als de ’release’ van een softwarestack, die gecontextualiseerd wordt in zijn specifieke praktijk. De serie is gericht op het documenteren van een set van instrumenten, ervaringen, manieren van werken die divers zijn in termen van hun temporaliteit, granulariteit en persistentie.
</div>
