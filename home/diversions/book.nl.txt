title: 
DiVersies
----
year: 
2019
----
category: Werksessie
----
link: https://diversions.constantvzw.org/
----
summary: DiVersies experimenteert met online collecties van culturele instellingen als plek voor dekoloniale en intersectionele praktijken. Geïnspireerd door de manier waarop versioning in genetwerkte software wordt toegepast, onderzoekt deze publicatie hoe online collecties radicaal verschillende, en soms tegengestelde perspectieven kunnen verwelkomen.
----
text:

**Download de publicatie**: https://diversions.constantvzw.org/publication/diversions_v1.pdf

**Gedrukt exemplaar: 3€ (excl verzendingskosten)**
**Zin in een exemplaar? Stuur dan een e-mail naar info@constantvzw.org**

Diversies vond plaats van 4 tot 11 december 2016 in het Koninklijk Museum voor Kunst en Geschiedenis in Brussel en vertrok vanuit de manier waarop versies deel uitmaken van de dagelijkse software-praktijk. Met 35 deelnemers onderzochten we hoe we naast het gebruikelijke verhaal over samenwerking en consensus, ook divergerende vormen van geschiedschrijving mogelijk kunnen maken door plaats te bieden aan verschil. 

De werksessie mondde uit in een tentoonstelling in De Pianofabriek en deze publicatie in 2019.
Een tweede versie van deze publicatie en van de expo DiVersies zien het licht in juni 2020 in De Krook (Gent).

Redactie: Constant (Elodie Mugrefya, Femke Snelting)
Scenografie: Mia Melvær, Cristina Cochior
Grafisch ontwerp: OSP (Gijs de Heij, Sarah Magnan)

DiVersies is een initatief van Constant en wordt ontwikkeld in samenwerking met: UGent - Department of Educational Studies, Werkplaats immaterieel erfgoed, PACKED - Expertisecentrum Digitaal Erfgoed, RoSa - Kenniscentrum voor gender en feminisme. Met steun van: Vlaamse Overheid, Vlaamse Gemeenschapscommissie, Fédération Wallonie-Bruxelles Arts Numériques.
