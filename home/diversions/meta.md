---
title: "DiVersions"
---

<div class="year">2019</div>
<div class="link"><a class="link" href="https://diversions.constantvzw.org/">https://diversions.constantvzw.org/</a></div>
<div class="category">Worksession</div>

DiVersions {lang=en}
===========================
<div class="summary">
DiVersions experiments with online collections of cultural institutions as sites for decolonial and intersectional practice. Inspired by the way versioning functions in networked software tools, this publication explores how online collections can accommodate radically different, and sometimes opposing perspectives.
</div>

<div class="text">
**Download the publication:** https://diversions.constantvzw.org/publication/diversions_v1.pdf

**Printed copy: 3€ (excl shipping costs)**
**Order a physical copy by sending an email to info@constantvzw.org**

DiVersions took place in the Royal Museum of Art and History in Brussels from 4 to 11 December 2016 and was inspired by the way versions are inscribed in daily software-practice. With 35 participants we explored how parallel to their conventional narrative of collaboration and consensus, we could produce divergent histories through supporting difference. 

The worksession was followed by an exhibition in De Pianofabriek and this publication in 2019. A second version of this publication and of the exhibition will be hosted by De Krook (Ghent) in June 2020.

With contributions from: Rahel Aima, Anaïs Berck, Ž. Blaće, Cristina Cochior, Sarah Kaerts, Phil Langley, Marie Lécrivain, Nicolas Malevé, Elodie Mugrefya, Zoumana Meïté, Mia Melvær, Martino Morandi, Michael Murtaugh, Colm o'Neill, Kris Rutten, Amir Sarabadani, Femke Snelting, Saskia Willaert.

Editorial team: Constant (Elodie Mugrefya, Femke Snelting)
Scenography: Mia Melvær, Cristina Cochior
Graphic design: OSP (Gijs de Heij, Sarah Magnan)

DiVersions is an initiative of Constant and developed in partnership with: UGent - Department of Educational Studies, Werkplaats immaterieel erfgoed, PACKED - Expertisecentrum Digitaal Erfgoed, RoSa - Kenniscentrum voor gender en feminisme. With the support of: Vlaamse Overheid, Vlaamse Gemeenschapscommissie, Fédération Wallonie-Bruxelles Arts Numériques.
</div>

DiVersions {lang=fr}
===========================
<div class="summary">
DiVersions expérimente avec les collections en ligne d'institutions culturelles comme sites de pratiques décoloniales et intersectionnelles. Inspiré par la manière dont le versioning fonctionne dans les outils logiciels en réseau. Cette publication explorent comment les collections en ligne peuvent s'adapter à des perspectives radicalement différentes, et parfois opposées.
</div>

<div class="text">
**Téléchargez la publication :** https://diversions.constantvzw.org/publication/diversions_v1.pdf

**Exemplaire imprimé : 3€ (excl frais de livraison)**
**Achetez un exemplaire en envoyant un e-mail à info@constantvzw.org**

DiVersions a eu lieu du 4 décembre au 11 décembre 2016 au Musée Royal d’Art et Histoire à Bruxelles et s’inspirait de la façon dont les versions font partie de la pratique quotidienne des logiciels. Avec une groupe de 35 participant(e)s, nous avons exploré comment celles-ci peuvent rendre possible, au-delà des pratiques existantes de collaboration et consensus, des formes divergentes de récits historiques en offrant de l’espace à la différence. 

La session de travail a été suivie par une exposition au Pianofabriek et cette publication en octobre 2019. Une deuxième version de DiVersions sera accueillie par De Krook (Gand) en juin 2020 et sera suivie d'une nouvelle version de la publication.

Equipe éditoriale : Constant (Elodie Mugrefya, Femke Snelting)
Scenographie : Mia Melvær, Cristina Cochior
Graphisme : OSP (Gijs de Heij, Sarah Magnan)

DiVersions est une initiative de Constant et développée en partenariat avec UGent - Department of Educational Studies, Werkplaats immaterieel erfgoed, PACKED - Expertisecentrum Digitaal Erfgoed, RoSa - Kenniscentrum voor gender en feminisme.
Avec le soutien de : Vlaamse Overheid, Vlaamse Gemeenschapscommissie, Fédération Wallonie-Bruxelles Arts Numériques.
</div>

DiVersies {lang=nl}
===========================
<div class="summary">
DiVersies experimenteert met online collecties van culturele instellingen als plek voor dekoloniale en intersectionele praktijken. Geïnspireerd door de manier waarop versioning in genetwerkte software wordt toegepast, onderzoekt deze publicatie hoe online collecties radicaal verschillende, en soms tegengestelde perspectieven kunnen verwelkomen.
</div>

<div class="text">
**Download de publicatie**: https://diversions.constantvzw.org/publication/diversions_v1.pdf

**Gedrukt exemplaar: 3€ (excl verzendingskosten)**
**Zin in een exemplaar? Stuur dan een e-mail naar info@constantvzw.org**

Diversies vond plaats van 4 tot 11 december 2016 in het Koninklijk Museum voor Kunst en Geschiedenis in Brussel en vertrok vanuit de manier waarop versies deel uitmaken van de dagelijkse software-praktijk. Met 35 deelnemers onderzochten we hoe we naast het gebruikelijke verhaal over samenwerking en consensus, ook divergerende vormen van geschiedschrijving mogelijk kunnen maken door plaats te bieden aan verschil. 

De werksessie mondde uit in een tentoonstelling in De Pianofabriek en deze publicatie in 2019.
Een tweede versie van deze publicatie en van de expo DiVersies zien het licht in juni 2020 in De Krook (Gent).

Redactie: Constant (Elodie Mugrefya, Femke Snelting)
Scenografie: Mia Melvær, Cristina Cochior
Grafisch ontwerp: OSP (Gijs de Heij, Sarah Magnan)

DiVersies is een initatief van Constant en wordt ontwikkeld in samenwerking met: UGent - Department of Educational Studies, Werkplaats immaterieel erfgoed, PACKED - Expertisecentrum Digitaal Erfgoed, RoSa - Kenniscentrum voor gender en feminisme. Met steun van: Vlaamse Overheid, Vlaamse Gemeenschapscommissie, Fédération Wallonie-Bruxelles Arts Numériques.
</div>
