title: 
Xavan en Jaluka
----
year: 
2018
----
category: Death of the authors
----
link: 
----
summary: Een strip gemaakt door Pléoter Spilliaestenberg (Peter Westenberg : scenario + Léon Spilliaert: schilderijen/tekeningen.)
----
text:

**Download het boek:**. <http://www.books.constantvzw.org/home/xavan_jaluka/Xavan_JalukaNL.pdf>
**Gedrukt exemplaar**: 5€ (excl verzendingskosten)
**Zin om een exemplaar te bestellen?** Stuur dan een email naar info@constantvzw.org

Op vrijdag 17 Frifek van het jaar Zek, reduceert een Qualariaanse neutronenmoller de Grootzone Zendium tot niet veel meer dan een trilpudding, met dramatisch gevolg voor de verliefden Xavan en Jaluka.

In dit beeldverhaal remixt Peter Westenberg schilderijen en tekeningen van Léon Spilliaert.

Zeventig jaar na het overlijden van een auteur / kunstenaar vervallen de auteursrechten op zijn / haar oeuvre. De Publiek Domein Dag viert het nieuwe werk dat beschikbaar komt in het publiek domein. Ter gelegenheid daarvan brengt Vereniging voor Kunst en Media Constant elk jaar een remix van publiek domein auteurs, in de serie Death of the Authors

Xavan en Jaluka, Death of the Authors versie 1946 remixt het werk van de Belgische schilder Léon Spilliaert. Spilliaert werkte in Oostende aan het begin van de 20e eeuw. Zijn werk is somber, lyrisch en poëtisch. Hij overleed in 1946. Zijn werk mag nu rechtenvrij worden bewerkt, gepubliceerd, veranderd en verkocht.

De strip Xavan en Jaluka is een science-fiction liefdesverhaal dat zich afspeelt in de regio Zendium dat in kort maar hevig conflict verwikkeld is met de noorderbuur Qualaria. Spilliaert ontwaakt in dit fictieve verhaal.

Het scenario en de tekst werden geschreven door Peter Westenberg.
