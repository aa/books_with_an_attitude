---
title: "Xavan + Jaluka"
---

<div class="year">2018</div>

<div class="category">Death of the authors</div>

Xavan + Jaluka {lang=en}
===========================
<div class="summary">
A comic book by Pléoter Spilliaestenberg (Peter Westenberg: scenario + Léon Spilliaert: drawings/paintings)
</div>

<div class="text">
**Download the book:** 
**FR**: <http://www.books.constantvzw.org/home/xavan_jaluka/Xavan_JalukaFR.pdf>
**NL**: <http://www.books.constantvzw.org/home/xavan_jaluka/Xavan_JalukaNL.pdf>
**Printed copy**: 5€ (excl shipping costs)
**Interested to order a copy?** Please email info@constantvzw.org

Friday 17 Frifek of the year Zek, a neutrondemolisher reduces the Grand Zone of Zendium to little more then a pudding shake, with dramatic consequences for lovers Xavan and Jaluka.

This comic by Peter Westenberg remixes the work of Belgian painter Léon Spilliaert.

Seventy years after the death of an author or artist, copyrights do no longer apply to his / her work. Public Domain Day celebrates yearly the fact that new works are entering the Public Domain. On this occasion Constant presents every year a public domain remix in the series Death of the authors.

Léon Spilliaert lived and worked in Oostende in the beginning of the 20th century. His symbolist work is lyrical, sombre and poetic. He passed away in 1946. His work is no longer protected by copyright and can be re-apropriated, modified and published.

The strip is a love story in a science-fiction set in the region of Zendium which is in a short but intense conflict with neighboring region Qualaria. Spilliaert wakes up in a fictive future. 

French and Dutch versions available. No English (yet)
</div>

Xavan et Jaluka {lang=fr}
===========================
<div class="summary">
Une bande dessinée de Pléoter Spilliaestenberg (Peter Westenberg : scénario + Léon Spilliaert: dessins et peintures.)
</div>

<div class="text">
**Téléchargez le livre** : <http://www.books.constantvzw.org/home/xavan_jaluka/Xavan_JalukaFR.pdf>
**Exemplaire imprimé**: 5€ (excl frais de livraison)
**Envie de commander un exemplaire?** Merci d'envoyer un e-mail à info@constantvzw.org

Le vendredi 17 Frifek de l’année Zek, un Démolineutron qualarien réduit la Grande Zone de Zendium à la taille d’un pudding vibratoire, ce qui a des conséquences dramatiques pour les amoureux Xavan et Jaluka.

Cette bande dessinée de Peter Westenberg est un remix de dessins et peintures de Leon Spilliaert. 

Septante ans après la mort d’un auteur ou d'un artiste, les droits d’auteur cessent de s’appliquer à son œuvre. La Journée du Domaine Public célèbre les nouveaux travaux qui entrent ainsi dans le domaine public. Chaque année, Constant, Association pour l’Art et les Médias, présente à cette occasion un remix à partir des œuvres d'auteurs entrés dans le domaine public, dans la série Death of the Authors.

Xavan en Jaluka, Death of the Authors version 1946 est un remix des certains œuvres du peintre belge Léon Spilliaert. Spilliaert a travaillé à Ostende au début du 20ème siècle. Son travail est sombre, lyrique et poétique. Il est décédé en 1946. Son travail n'est plus soumis au droit d'auteur et donc peut maintenant être publié, modifié et vendu. 

La bande dessinée Xavan et Jaluka est une histoire d'amour de science-fiction. Elle se déroule dans la région de Zendium, qui connaît un conflit court, mais intense avec la région voisine de Qualaria. Spilliaert se réveille au milieu de cette histoire fictive.

Le scénario et le texte ont été écrits par Peter Westenberg.
</div>

Xavan en Jaluka {lang=nl}
===========================
<div class="summary">
Een strip gemaakt door Pléoter Spilliaestenberg (Peter Westenberg : scenario + Léon Spilliaert: schilderijen/tekeningen.)
</div>

<div class="text">
**Download het boek:**. <http://www.books.constantvzw.org/home/xavan_jaluka/Xavan_JalukaNL.pdf>
**Gedrukt exemplaar**: 5€ (excl verzendingskosten)
**Zin om een exemplaar te bestellen?** Stuur dan een email naar info@constantvzw.org

Op vrijdag 17 Frifek van het jaar Zek, reduceert een Qualariaanse neutronenmoller de Grootzone Zendium tot niet veel meer dan een trilpudding, met dramatisch gevolg voor de verliefden Xavan en Jaluka.

In dit beeldverhaal remixt Peter Westenberg schilderijen en tekeningen van Léon Spilliaert.

Zeventig jaar na het overlijden van een auteur / kunstenaar vervallen de auteursrechten op zijn / haar oeuvre. De Publiek Domein Dag viert het nieuwe werk dat beschikbaar komt in het publiek domein. Ter gelegenheid daarvan brengt Vereniging voor Kunst en Media Constant elk jaar een remix van publiek domein auteurs, in de serie Death of the Authors

Xavan en Jaluka, Death of the Authors versie 1946 remixt het werk van de Belgische schilder Léon Spilliaert. Spilliaert werkte in Oostende aan het begin van de 20e eeuw. Zijn werk is somber, lyrisch en poëtisch. Hij overleed in 1946. Zijn werk mag nu rechtenvrij worden bewerkt, gepubliceerd, veranderd en verkocht.

De strip Xavan en Jaluka is een science-fiction liefdesverhaal dat zich afspeelt in de regio Zendium dat in kort maar hevig conflict verwikkeld is met de noorderbuur Qualaria. Spilliaert ontwaakt in dit fictieve verhaal.

Het scenario en de tekst werden geschreven door Peter Westenberg.
</div>
