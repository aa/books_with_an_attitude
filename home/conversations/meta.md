---
title: "Conversations"
---

<div class="year">2015</div>
<div class="link"><a class="link" href="http://conversations.tools">http://conversations.tools</a></div>
<div class="category">Projects</div>

Conversations {lang=en}
===========================
<div class="summary">
Publication of an extensive collection of conversations between developers and designers involved in the wider ecosystem of Libre Graphics. Speaking to each other about tools for typography, lay-out and image processing they render a portrait of a community gradually understanding the interdependencies between Free Software and design.
</div>

<div class="text">
**Download the book**: http://conversations.tools
**Printed copy**: 15€ (excl shipping costs)
**Interested to order a copy? Please email info@constantvzw.org**

Publication of an extensive collection of conversations between developers and designers involved in the wider ecosystem of Libre Graphics. Speaking to each other about tools for typography, lay-out and image processing they render a portrait of a community gradually understanding the interdependencies between Free Software and design. *Conversations* was edited by Femke Snelting in collaboration with Christoph Haag.

**With**: Agnes Bewer, Alexandre Leray, An Mertens, Andreas Vox, Asheesh Laroia, Carla Boserman, Christina Clar, Chris Lilley, Christoph Haag, Claire Williams, Cornelia Sollfrank, Dave Crossland, Denis Jacquery, Dmytri Kleiner, Eleanor Greenhalgh, Eric Schrijver, Evan Roth, Femke Snelting, Franziska Kleiner, George Williams, Gijs de Heij, Harrisson, Ivan Monroy Lopez, John Haltiwanger, John Colenbrander, Juliane De Moerlooze, Julien Deswaef, Larisa Blazic, Ludivine Loiseau, Manuel Schmalstieg, Matthew Fuller, Michael Murtaugh, Michael Terry, Michele Walther, Miguel Arana Catania, momo3010, Nicolas Malevé, Pedro Amado, Peter Westenberg, Pierre Huyghebaert, Pierre Marchand, Sarah Magnan, Stéphanie Vilayphiou, Tom Lechner, Urantsetseg Ulziikhuu, Xavier Klein.

ISBN: 978-9-0811-4593-0
</div>

Conversations {lang=fr}
===========================
<div class="summary">
La publication d’une collection de conversations entre des développeurs et des artistes et graphistes impliqués dans l’écosystème du ’Libre Graphics’. Lors de discussions autour d’outils pour la typographie, de la mise en page et de l’image, ils tracent le portrait d’une communauté qui comprend progressivement les interdépendances entre les logiciels libres et le design.
</div>

<div class="text">
**Téléchargez le livre** : http://conversations.tools
**Exemplaire imprimé** : 15€ (excl frais de livraison)
**Envie de commander un exemplaire ? Merci d'envoyer un e-mail à info@constantvzw.org**

La publication d’une collection de conversations entre des développeurs et des artistes et graphistes impliqués dans l’écosystème du ’Libre Graphics’. Lors de discussions autour d’outils pour la typographie, de la mise en page et de l’image, ils tracent le portrait d’une communauté qui comprend progressivement les interdépendances entre les logiciels libres et le design. *Conversations* a été édité par Femke Snelting en collaboration avec Christoph Haag.

**Avec**: Agnes Bewer, Alexandre Leray, An Mertens, Andreas Vox, Asheesh Laroia, Carla Boserman, Christina Clar, Chris Lilley, Christoph Haag, Claire Williams, Cornelia Sollfrank, Dave Crossland, Denis Jacquery, Dmytri Kleiner, Eleanor Greenhalgh, Eric Schrijver, Evan Roth, Femke Snelting, Franziska Kleiner, George Williams, Gijs de Heij, Harrisson, Ivan Monroy Lopez, John Haltiwanger, John Colenbrander, Juliane De Moerlooze, Julien Deswaef, Larisa Blazic, Ludivine Loiseau, Manuel Schmalstieg, Matthew Fuller, Michael Murtaugh, Michael Terry, Michele Walther, Miguel Arana Catania, momo3010, Nicolas Malevé, Pedro Amado, Peter Westenberg, Pierre Huyghebaert, Pierre Marchand, Sarah Magnan, Stéphanie Vilayphiou, Tom Lechner, Urantsetseg Ulziikhuu, Xavier Klein.

ISBN: 978-9-0811-4593-0
</div>

Conversations {lang=nl}
===========================
<div class="summary">
Publicatie van een uitgebreide verzameling gesprekken tussen ontwikkelaars en ontwerpers die betrokken zijn bij het bredere ecosysteem van ’Libre Graphics’. Terwijl ze met elkaar spreken over digitale gereedschappen voor typografie, lay-out en beeldbewerking, schetsen ze een portret van een gemeenschap die geleidelijk aan inzicht krijgt in de wederkerige relaties tussen Vrije Software en design.
</div>

<div class="text">
**Download het boek**: http://conversations.tools
**Gedrukt exemplaar**: 15€ (excl. verzendingskosten)
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

Publicatie van een uitgebreide verzameling gesprekken tussen ontwikkelaars en ontwerpers die betrokken zijn bij het bredere ecosysteem van ’Libre Graphics’. Terwijl ze met elkaar spreken over digitale gereedschappen voor typografie, lay-out en beeldbewerking, schetsen ze een portret van een gemeenschap die geleidelijk aan inzicht krijgt in de wederkerige relaties tussen Vrije Software en design. *Conversations* werd geredigeerd door Femke Snelting in samenwerking met Christoph Haag.

**Met**: Agnes Bewer, Alexandre Leray, An Mertens, Andreas Vox, Asheesh Laroia, Carla Boserman, Christina Clar, Chris Lilley, Christoph Haag, Claire Williams, Cornelia Sollfrank, Dave Crossland, Denis Jacquery, Dmytri Kleiner, Eleanor Greenhalgh, Eric Schrijver, Evan Roth, Femke Snelting, Franziska Kleiner, George Williams, Gijs de Heij, Harrisson, Ivan Monroy Lopez, John Haltiwanger, John Colenbrander, Juliane De Moerlooze, Julien Deswaef, Larisa Blazic, Ludivine Loiseau, Manuel Schmalstieg, Matthew Fuller, Michael Murtaugh, Michael Terry, Michele Walther, Miguel Arana Catania, momo3010, Nicolas Malevé, Pedro Amado, Peter Westenberg, Pierre Huyghebaert, Pierre Marchand, Sarah Magnan, Stéphanie Vilayphiou, Tom Lechner, Urantsetseg Ulziikhuu, Xavier Klein.

ISBN: 978-9-0811-4593-0
</div>
