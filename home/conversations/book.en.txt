title: 
Conversations
----
year: 
2015
----
category: Projects
----
link: http://conversations.tools
----
summary: Publication of an extensive collection of conversations between developers and designers involved in the wider ecosystem of Libre Graphics. Speaking to each other about tools for typography, lay-out and image processing they render a portrait of a community gradually understanding the interdependencies between Free Software and design.
----
text:

**Download the book**: http://conversations.tools
**Printed copy**: 15€ (excl shipping costs)
**Interested to order a copy? Please email info@constantvzw.org**

Publication of an extensive collection of conversations between developers and designers involved in the wider ecosystem of Libre Graphics. Speaking to each other about tools for typography, lay-out and image processing they render a portrait of a community gradually understanding the interdependencies between Free Software and design. *Conversations* was edited by Femke Snelting in collaboration with Christoph Haag.

**With**: Agnes Bewer, Alexandre Leray, An Mertens, Andreas Vox, Asheesh Laroia, Carla Boserman, Christina Clar, Chris Lilley, Christoph Haag, Claire Williams, Cornelia Sollfrank, Dave Crossland, Denis Jacquery, Dmytri Kleiner, Eleanor Greenhalgh, Eric Schrijver, Evan Roth, Femke Snelting, Franziska Kleiner, George Williams, Gijs de Heij, Harrisson, Ivan Monroy Lopez, John Haltiwanger, John Colenbrander, Juliane De Moerlooze, Julien Deswaef, Larisa Blazic, Ludivine Loiseau, Manuel Schmalstieg, Matthew Fuller, Michael Murtaugh, Michael Terry, Michele Walther, Miguel Arana Catania, momo3010, Nicolas Malevé, Pedro Amado, Peter Westenberg, Pierre Huyghebaert, Pierre Marchand, Sarah Magnan, Stéphanie Vilayphiou, Tom Lechner, Urantsetseg Ulziikhuu, Xavier Klein.

ISBN: 978-9-0811-4593-0

