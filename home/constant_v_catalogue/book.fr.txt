title: 
Constant_V catalogue
----
year: 
2019
----
category: Projets
----
link: http://constantvzw.org/site/-Constant_V,196-.html?lang=fr
----
summary: Ce livret contient un aperçu des 3 premières années de Constant_V. Plus d'une vingtaine de projets réalisées entre 2016 et 2019, par une cinquantaine d'artistes, designers, hackers et collectifs. 
----
text:

**Téléchargez le livre**: https://gitlab.constantvzw.org/design/2018-19/tree/master/04-catalogue-constantvzw/pdf

**Exemplaire imprimé : seulement les frais de livraison**
**Envie de commander un exemplaire? Merci d'envoyer un e-mail à info@constantvzw.org**

Constant_V est une série d’installations à petite échelle dans la vitrine du bureau de Constant. Ces installations sont accompagnées parfois par des ateliers animés par le ou les artiste(s) afin de mettre l’accent pas seulement sur le produit fini, mais aussi sur le processus de création subsistent. Ce sont des occasions de découvrir les motivations, les techniques ou d’autres aspects d’une œuvre.

Constant_V transforme la vitrine de notre bureau en membrane en présentant des artistes et des projets reliés à Constant. Les œuvres montrées sont en préférence en cours de création, ce qui permet de donner de la visibilité au travail conceptuel, technique et collaborative qui se cache derrière le processus créatif. Constant_V présente des œuvres qui sont réalisées avec des logiciels libres et distribuées sous Licence Art Libre. De cette façon Constant_V offre aux passants un aperçu dans le monde de l’art F/LOSS.

Les œuvres numériques présentées sont aussi très différentes; elles vont de la vidéo à des œuvres interactives, de la conception graphique aux machines hackées et aux prototypes.

Artistes: Alex Leray, Alexia de Visscher, Algolit, André Castro, An Mertens, Anne Laforet, Antje Van Wichelen, Antonio Roberts, Axel Claes, Barbara Janssens, Catherine Lenoble, Claire Williams, Dick Reckard, Dyne.org, Ellef Prestsæter, esc medien kunst labor, Femke Snelting, Gijs de Heij, Hangar, Isabel Burr Raty, James Bryan Graves, Jara Rocha, Jonathan Poliart, Julien Deswaef, Katrien Oosterlinck, LibreObjet, Martin Lévêque, Mathieu Gabiot, Maxime Fuhrer, Michael Murtaugh, Mondotheque, Natacha Roussel, Nicolas Malevé, Ninon Mazeaud, Open Sound Lab, OSP, Pascale Barret, Peter Westenberg, Piero Bisello, Plus-Tôt Te Laat, Possible Bodies, Spec, Rafaella Houlstan-Hasaerts, Raphaël Bastide, Remi Huang, Samedies, Sarah Garcin, SICV, Stéfan Piat, Stéphane Cousot, Stéphanie Vilayphiou, Tim Vets, Wendy Van Wynsberghe.

Conception & réalisation: Anna Diop-Dubois

Relecture & traduction: An Mertens, Emma Kraak, Patrick Lennon, Femke Snelting, Donatella Portoghese

Graphisme: Deal (Quentin Jumelin & Morgane Le Ferec)

Impression: Graphius Bruxelles




