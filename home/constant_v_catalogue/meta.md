---
title: "Constant_V catalogue"
---

<div class="year">2019</div>
<div class="link"><a class="link" href="http://constantvzw.org/site/-Constant_V,196-.html">http://constantvzw.org/site/-Constant_V,196-.html</a></div>
<div class="category">Projects</div>

Constant_V catalogue {lang=en}
===========================
<div class="summary">

</div>

<div class="text">
**Download the book**: https://gitlab.constantvzw.org/design/2018-19/tree/master/04-catalogue-constantvzw/pdf

**Printed copy: shipping costs only**
**Interested to order a copy? Please email info@constantvzw.org**

Constant_V is a series of small-scale installations in the window of the Constant office. These installations are sometimes accompanied by a workshop led by the artist(s) in order to focus not only on the finished product, but also on the process of creation. They give the opportunity to discover the motivations, the techniques or other aspects of a work.

Constant_V uses our front window as a membrane; it showcases artists and projects connected to Constant. Constant_V prefers showing works that are in progress, giving visibility to the conceptual, technical and collaborative work that goes into the creation of an artwork. Constant_V shows works that are made with Free Software and / or distributed under open content licenses. This way Constant_V offers passers by insight in the world of F/LOSS arts.

Constant_V presents digital works of various types ranging from video to interactive works, from graphic design to hacked machines and prototypes.

Artists: Alex Leray, Alexia de Visscher, Algolit, André Castro, An Mertens, Anne Laforet, Antje Van Wichelen, Antonio Roberts, Axel Claes, Barbara Janssens, Catherine Lenoble, Claire Williams, Dick Reckard, Dyne.org, Ellef Prestsæter, esc medien kunst labor, Femke Snelting, Gijs de Heij, Hangar, Isabel Burr Raty, James Bryan Graves, Jara Rocha, Jonathan Poliart, Julien Deswaef, Katrien Oosterlinck, LibreObjet, Martin Lévêque, Mathieu Gabiot, Maxime Fuhrer, Michael Murtaugh, Mondotheque, Natacha Roussel, Nicolas Malevé, Ninon Mazeaud, Open Sound Lab, OSP, Pascale Barret, Peter Westenberg, Piero Bisello, Plus-Tôt Te Laat, Possible Bodies, Spec, Rafaella Houlstan-Hasaerts, Raphaël Bastide, Remi Huang, Samedies, Sarah Garcin, SICV, Stéfan Piat, Stéphane Cousot, Stéphanie Vilayphiou, Tim Vets, Wendy Van Wynsberghe.

Design & production: Anna Diop-Dubois

Proofreading & translation: An Mertens, Emma Kraak, Patrick Lennon, Femke Snelting, Donatella Portoghese

Graphic desing: Deal (Quentin Jumelin & Morgane Le Ferec)

Printing: Graphius Bruxelles
</div>

Constant_V catalogue {lang=fr}
===========================
<div class="summary">
Ce livret contient un aperçu des 3 premières années de Constant_V. Plus d'une vingtaine de projets réalisées entre 2016 et 2019, par une cinquantaine d'artistes, designers, hackers et collectifs.
</div>

<div class="text">
**Téléchargez le livre**: https://gitlab.constantvzw.org/design/2018-19/tree/master/04-catalogue-constantvzw/pdf

**Exemplaire imprimé : seulement les frais de livraison**
**Envie de commander un exemplaire? Merci d'envoyer un e-mail à info@constantvzw.org**

Constant_V est une série d’installations à petite échelle dans la vitrine du bureau de Constant. Ces installations sont accompagnées parfois par des ateliers animés par le ou les artiste(s) afin de mettre l’accent pas seulement sur le produit fini, mais aussi sur le processus de création subsistent. Ce sont des occasions de découvrir les motivations, les techniques ou d’autres aspects d’une œuvre.

Constant_V transforme la vitrine de notre bureau en membrane en présentant des artistes et des projets reliés à Constant. Les œuvres montrées sont en préférence en cours de création, ce qui permet de donner de la visibilité au travail conceptuel, technique et collaborative qui se cache derrière le processus créatif. Constant_V présente des œuvres qui sont réalisées avec des logiciels libres et distribuées sous Licence Art Libre. De cette façon Constant_V offre aux passants un aperçu dans le monde de l’art F/LOSS.

Les œuvres numériques présentées sont aussi très différentes; elles vont de la vidéo à des œuvres interactives, de la conception graphique aux machines hackées et aux prototypes.

Artistes: Alex Leray, Alexia de Visscher, Algolit, André Castro, An Mertens, Anne Laforet, Antje Van Wichelen, Antonio Roberts, Axel Claes, Barbara Janssens, Catherine Lenoble, Claire Williams, Dick Reckard, Dyne.org, Ellef Prestsæter, esc medien kunst labor, Femke Snelting, Gijs de Heij, Hangar, Isabel Burr Raty, James Bryan Graves, Jara Rocha, Jonathan Poliart, Julien Deswaef, Katrien Oosterlinck, LibreObjet, Martin Lévêque, Mathieu Gabiot, Maxime Fuhrer, Michael Murtaugh, Mondotheque, Natacha Roussel, Nicolas Malevé, Ninon Mazeaud, Open Sound Lab, OSP, Pascale Barret, Peter Westenberg, Piero Bisello, Plus-Tôt Te Laat, Possible Bodies, Spec, Rafaella Houlstan-Hasaerts, Raphaël Bastide, Remi Huang, Samedies, Sarah Garcin, SICV, Stéfan Piat, Stéphane Cousot, Stéphanie Vilayphiou, Tim Vets, Wendy Van Wynsberghe.

Conception & réalisation: Anna Diop-Dubois

Relecture & traduction: An Mertens, Emma Kraak, Patrick Lennon, Femke Snelting, Donatella Portoghese

Graphisme: Deal (Quentin Jumelin & Morgane Le Ferec)

Impression: Graphius Bruxelles
</div>

Constant_V catalogus {lang=nl}
===========================
<div class="summary">
Dit boekje bevat een overzicht van de eerste drie jaar van Constant_V. Tussen 2016 en 2019 werden meer dan twintig projecten gerealiseerd door een vijftigtal betrokken kunstenaars, ontwerpers, hackers en collectieven.
</div>

<div class="text">
**Download het boek**: https://gitlab.constantvzw.org/design/2018-19/tree/master/04-catalogue-constantvzw/pdf

**Gedrukt exemplaar: enkel verzendingskosten**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

Constant_V is een reeks kleinschalige installaties in de vitrine van Constant’s kantoor. Deze installaties gaan soms vergezeld van een workshop door de kunstenaar(s) waardoor niet alleen het eindproduct, maar ook het creatieproces centraal komt te staan.

Constant_V gebruikt het raam als een membraan: het toont kunstenaars en projecten die verbonden zijn aan Constant. Constant_V geeft voorkeur aan het tonen van werken in ontwikkeling, en geeft zichtbaarheid aan de conceptuele, technische en collectieve aspecten van het creatieproces. Constant_V toont werk dat gemaakt is met vrije software, en dat wordt gedistribueerd onder een open content licentie. Op deze manier geeft Constant_V voorbijgangers inzage in de wereld van F/LOSS kunst.

Constant_V toont uiteenlopende soorten digitaal werk: van video tot interactief werk, van grafisch ontwerp tot gehackte machines en protoypes.

Kunstenaars: Alex Leray, Alexia de Visscher, Algolit, André Castro, An Mertens, Anne Laforet, Antje Van Wichelen, Antonio Roberts, Axel Claes, Barbara Janssens, Catherine Lenoble, Claire Williams, Dick Reckard, Dyne.org, Ellef Prestsæter, esc medien kunst labor, Femke Snelting, Gijs de Heij, Hangar, Isabel Burr Raty, James Bryan Graves, Jara Rocha, Jonathan Poliart, Julien Deswaef, Katrien Oosterlinck, LibreObjet, Martin Lévêque, Mathieu Gabiot, Maxime Fuhrer, Michael Murtaugh, Mondotheque, Natacha Roussel, Nicolas Malevé, Ninon Mazeaud, Open Sound Lab, OSP, Pascale Barret, Peter Westenberg, Piero Bisello, Plus-Tôt Te Laat, Possible Bodies, Spec, Rafaella Houlstan-Hasaerts, Raphaël Bastide, Remi Huang, Samedies, Sarah Garcin, SICV, Stéfan Piat, Stéphane Cousot, Stéphanie Vilayphiou, Tim Vets, Wendy Van Wynsberghe.

Ontwikkeling & productie: Anna Diop-Dubois

Proeflezen & vertaling: An Mertens, Emma Kraak, Patrick Lennon, Femke Snelting, Donatella Portoghese

Grafisch ontwerp: Deal (Quentin Jumelin & Morgane Le Ferec)

Druk: Graphius Bruxelles
</div>
