title: 
Tracks in electr(on)ic fields · Empreintes dans les champs électr(on)iques · Sporen in het elektr(on)ische veld
----
year: 
2009
----
category: Verbindingen/Jonctions
----
link: https://gitlab.constantvzw.org/osp/work.tracks-in-electronic-fields-vj10
----
summary: Cette publication rassemble les textes et les images du 10e festival Verbindingen/Jonctions en 2007, un événement de 4 jours à La Bellone, autour des gestes transformés en données, codes qui mettent le corps en mouvement, prescriptions qui demandent à être interprétées.  
----
text:

**Exemplaire imprimé : 10€ (excl frais de livraison)**
**Envie de commander un exemplaire ? Merci d'envoyer un e-mail à info@constantvzw.org**

*Empreintes dans les champs électr(on)iques* est la publication rassemblant les textes et les images du 10e festival Verbindingen/Jonctions en 2007.
Ce livre contient des contributions en anglais, français et néerlandais. Il a été compilé et rédigé par Constant feat. Clémentine Delahaut, Laurence Rassel et Emma Sidgwick. Le graphisme réalisé par [Open Source Publishing](http://osp.kitchen/) a obtenu un des prix Fernand Baudin.

*Empreintes dans les champs électr(on)iques* était un festival de 4 jours à La Bellone, autour des gestes transformés en données, codes qui mettent le corps en mouvement, prescriptions qui demandent à être interprétées. Des identités réelles et contrefaites se répandent comme des virus à travers les réseaux par des services commerciaux d’information personnalisés et (in)contrôlés, qui habitent les frontières et prennent corps dans les rumeurs. 

Programme: <http://vj10.constantvzw.org/-About-VJ10-.html>


