title: 
Tracks in electr(on)ic fields · Empreintes dans les champs électr(on)iques · Sporen in het elektr(on)ische veld
----
year: 
2009
----
category: Verbindingen/Jonctions
----
link: https://gitlab.constantvzw.org/osp/work.tracks-in-electronic-fields-vj10
----
summary: This publication with texts and images documents the 10th Verbindingen/Jonctions in 2007, a four day festival in La Bellone about gestures transformed into data, codes that set bodies in motion, prescriptions that ask to be interpreted. 
----
text:

**Printed copy: 10€ (excl shipping costs)**
**Interested to order a copy? Please email info@constantvzw.org**

This publication with texts and images documents the 10th Verbindingen/Jonctions festival. It contains contributions in English, French and Dutch and was edited by Constant featuring Clementine Delahaut, Laurence Rassel and Emma Sidgwick. 
The design of this book was realized by [Open Source Publishing](http://osp.kitchen/) and won a 2009 Fernand Baudin prize.

*Tracks in electr(on)ic fields* was a four day festival in La Bellone about gestures transformed into data, codes that set bodies in motion, prescriptions that ask to be interpreted. Pretended or real identities spread like viruses as they are carried across networks by (un)controlable, commercialised and personalised information services, inhabiting borders and embodying rumours. 

Program: <http://vj10.constantvzw.org/-About-VJ10-.html>


