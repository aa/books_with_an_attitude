---
title: "Tracks in electr(on)ic fields · Empreintes dans les champs électr(on)iques · Sporen in het elektr(on)ische veld"
---

<div class="year">2009</div>
<div class="link"><a class="link" href="https://gitlab.constantvzw.org/osp/work.tracks-in-electronic-fields-vj10">https://gitlab.constantvzw.org/osp/work.tracks-in-electronic-fields-vj10</a></div>
<div class="category">Verbindingen/Jonctions</div>

Tracks in electr(on)ic fields · Empreintes dans les champs électr(on)iques · Sporen in het elektr(on)ische veld {lang=en}
===========================
<div class="summary">
This publication with texts and images documents the 10th Verbindingen/Jonctions in 2007, a four day festival in La Bellone about gestures transformed into data, codes that set bodies in motion, prescriptions that ask to be interpreted.
</div>

<div class="text">
**Printed copy: 10€ (excl shipping costs)**
**Interested to order a copy? Please email info@constantvzw.org**

This publication with texts and images documents the 10th Verbindingen/Jonctions festival. It contains contributions in English, French and Dutch and was edited by Constant featuring Clementine Delahaut, Laurence Rassel and Emma Sidgwick. 
The design of this book was realized by [Open Source Publishing](http://osp.kitchen/) and won a 2009 Fernand Baudin prize.

*Tracks in electr(on)ic fields* was a four day festival in La Bellone about gestures transformed into data, codes that set bodies in motion, prescriptions that ask to be interpreted. Pretended or real identities spread like viruses as they are carried across networks by (un)controlable, commercialised and personalised information services, inhabiting borders and embodying rumours. 

Program: <http://vj10.constantvzw.org/-About-VJ10-.html>
</div>

Tracks in electr(on)ic fields · Empreintes dans les champs électr(on)iques · Sporen in het elektr(on)ische veld {lang=fr}
===========================
<div class="summary">
Cette publication rassemble les textes et les images du 10e festival Verbindingen/Jonctions en 2007, un événement de 4 jours à La Bellone, autour des gestes transformés en données, codes qui mettent le corps en mouvement, prescriptions qui demandent à être interprétées.
</div>

<div class="text">
**Exemplaire imprimé : 10€ (excl frais de livraison)**
**Envie de commander un exemplaire ? Merci d'envoyer un e-mail à info@constantvzw.org**

*Empreintes dans les champs électr(on)iques* est la publication rassemblant les textes et les images du 10e festival Verbindingen/Jonctions en 2007.
Ce livre contient des contributions en anglais, français et néerlandais. Il a été compilé et rédigé par Constant feat. Clémentine Delahaut, Laurence Rassel et Emma Sidgwick. Le graphisme réalisé par [Open Source Publishing](http://osp.kitchen/) a obtenu un des prix Fernand Baudin.

*Empreintes dans les champs électr(on)iques* était un festival de 4 jours à La Bellone, autour des gestes transformés en données, codes qui mettent le corps en mouvement, prescriptions qui demandent à être interprétées. Des identités réelles et contrefaites se répandent comme des virus à travers les réseaux par des services commerciaux d’information personnalisés et (in)contrôlés, qui habitent les frontières et prennent corps dans les rumeurs. 

Programme: <http://vj10.constantvzw.org/-About-VJ10-.html>
</div>

Tracks in electr(on)ic fields · Empreintes dans les champs électr(on)iques · Sporen in het elektr(on)ische veld {lang=nl}
===========================
<div class="summary">
Dit boek is een weerslag van de 10de editie van Verbindingen/Jonctions in 2007, een 4-daags festival van 4 dagen in La Bellone, dat onderzocht hoe bewegingen worden omgezet in code, hoe code onze lichamen in beweging zet en hoe gecodeerde informatie uitnodigt tot interpretatie.
</div>

<div class="text">
**Gedrukt exemplaar: 10€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

*Sporen in het elektr(on)ische veld* is een publicatie met teksten en beelden die het 10de Verbindingen/Jonctions festival in 2007 documenteren.
Dit boek bevat bijdragen in het Engels, Frans en Nederlands en werd samengesteld door Constant feat. Clémentine Delahaut, Laurence Rassel en Emma Sidgwick. Het ontwerp van [Open Source Publishing](http://osp.kitchen/) won één van de Fernand Baudin prijzen.

*Sporen in het elektr(on)ische veld* was een festival van 4 dagen in La Bellone, dat onderzocht hoe bewegingen worden omgezet in code, code onze lichamen in beweging zet en hoe gecodeerde informatie uitnodigt tot interpretatie. We zetten onze identiteit om in code en leiden een leven met aliassen en logins. Identiteiten, fictief of authentiek, verspreiden zich als virussen in netwerken via (on)controleerbare commerciële en persoonlijke informatiediensten. Ze bewonen er grenzen en belichamen geruchten. 

Programma: <http://vj10.constantvzw.org/-About-VJ10-.html>
</div>
