---
title: "Are You Being Served?"
---

<div class="year">2015</div>
<div class="link"><a class="link" href="http://areyoubeingserved.constantvzw.org">http://areyoubeingserved.constantvzw.org</a></div>
<div class="category">Verbindingen/Jonctions</div>

Are You Being Served? {lang=en}
===========================
<div class="summary">
A collective notebook that reflects the last edition of the meetingdays Verbindingen/Jonctions in December 2013. Realised with a team of ’dedicated notetakers’ who documented the workshops and lectures using the collective platform Etherpad.
</div>

<div class="text">
**Printed copy: 15€ (excl shipping costs)**
**Interested to order a copy? Please email info@constantvzw.org**

During the last edition of the meetingdays Verbindingen/Jonctions in December 2013 we invited a team of ’dedicated notetakers’ to document the workshops and lectures using the collective platform Etherpad. 
The 14th edition of Verbindingen/Jonctions was dedicated to a feminist review of mesh- cloud- autonomous- and D.I.Y. servers during four intense meetingdays mixing lectures, screenings, performances, discussions and workshops.

Program: <http://vj14.constantvzw.org/r/about>

This collective publication ’Are You Being Served’ was realised by the team of dedicated notetakers: Marloes De Valk, Madeleine Aktipy, Anne Laforet, An Mertens; with the joyful support of Michaela Lakova, Reni Hofmüller and Femke Snelting. The design and the collective tool Ethertoff were provided by [Open Source Publishing](http://osp.kitchen/): Stephanie Vilayphiou and Eric Schrijver. 
The perfect work environment for this was offered by [De Pianofabriek KWP](http://www.pianofabriek.be/?moturl=1&lang=en).
</div>

Are You Being Served? {lang=fr}
===========================
<div class="summary">
Un carnet collectif réalisé lors de l’édition finale des journées de rencontre Verbindingen/Jonctions en décembre 2013 par une équipe de 'preneurs de notes dévoués' afin de documenter collectivement les ateliers et les conférences avec la plateforme Etherpad.
</div>

<div class="text">
**Exemplaire imprimé : 15€ (excl frais de livraison)**
**Envie de commander un exemplaire? Merci d'envoyer un e-mail à info@constantvzw.org**

Lors de l’édition finale des journées de rencontre Verbindingen/Jonctions en décembre 2013, on a invité une équipe de 'preneurs de notes dévoués' afin de documenter collectivement les ateliers et les conférences avec la plateforme Etherpad.
Verbindingen/Jonctions offrait pour cette quatorizème édition une perspective féministe des 'services' fournis par les serveurs. Bienvenu-e-s pour quatre jours intenses de conférences, projections, performances, discussions et ateliers. Programme: <http://vj14.constantvzw.org/r/about>

La publication collective ’Are You Being Served’ est réalisée par l'équipe de preneurs de notes suivante: Marloes De Valk, Madeleine Aktipy, Anne Laforet, An Mertens; avec le soutien joyeux de Michaela Lakova, Reni Hofmüller et Femke Snelting. La mise-en-page et l’outil collectif Ethertoff sont de la part de [Open Source Publishing](http://osp.kitchen/): Stephanie Vilayphiou et Eric Schrijver. 
L’environnement de travail approprié nous a été offert par [De Pianofabriek KWP](http://www.pianofabriek.be/?moturl=1&lang=fr).
</div>

Are You Being Served? {lang=nl}
===========================
<div class="summary">
Een collectief notaboek naar aanleiding van de allerlaatste editie van de ontmoetingsdagen Verbindingen/Jonctions in december 2013, waarbij een team van ’toegewijde verslaggevers’ live nota’s nam van de workshops en lezingen via Etherpad.
</div>

<div class="text">
**Gedrukt exemplaar: 15€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

Tijdens de allerlaatste editie van de ontmoetingsdagen Verbindingen/Jonctions in december 2013 nodigden we een team van ’toegewijde verslaggevers’ uit om live nota’s te nemen van de workshops en lezingen.
In de 14de aflevering van Verbindingen/Jonctions stelde Constant een   feministische kijk op servertechnologie centraal. Of je nu Cloud, Autonoom, of DIY bent, je bent van harte welkom op deze vier intense ontmoetingsdagen die je een mix van screenings, werksessies, performances, discussies en lezingen bieden.
Programma: <http://vj14.constantvzw.org/r/about>

Die documentatie werd verwerkt tot een heuse collectieve publicatie. ’Are You Being Served’ is een realisatie van de verslaggeefsters, Marloes De Valk, Madeleine Aktipy, Anne Laforet, An Mertens; met de enthousiaste steun van Michaela Lakova, Reni Hofmüller en Femke Snelting. Het ontwerp en de collectieve tool Ethertoff zijn van de hand van [Open Source Publishing](http://osp.kitchen/): Stephanie Vilayphiou en Eric Schrijver. 
De ideale werkomgeving hiervoor werd ons aangeboden door [De Pianofabriek KWP](http://www.pianofabriek.be/?moturl=1&lang=nl).
</div>
