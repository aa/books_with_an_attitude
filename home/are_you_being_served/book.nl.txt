title: 
Are You Being Served?
----
year: 
2015
----
category: Verbindingen/Jonctions
----
link: http://areyoubeingserved.constantvzw.org
----
summary: Een collectief notaboek naar aanleiding van de allerlaatste editie van de ontmoetingsdagen Verbindingen/Jonctions in december 2013, waarbij een team van ’toegewijde verslaggevers’ live nota’s nam van de workshops en lezingen via Etherpad.
----
text:

**Gedrukt exemplaar: 15€ (excl verzendingskosten)**
**Zin om een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

Tijdens de allerlaatste editie van de ontmoetingsdagen Verbindingen/Jonctions in december 2013 nodigden we een team van ’toegewijde verslaggevers’ uit om live nota’s te nemen van de workshops en lezingen.
In de 14de aflevering van Verbindingen/Jonctions stelde Constant een   feministische kijk op servertechnologie centraal. Of je nu Cloud, Autonoom, of DIY bent, je bent van harte welkom op deze vier intense ontmoetingsdagen die je een mix van screenings, werksessies, performances, discussies en lezingen bieden.
Programma: <http://vj14.constantvzw.org/r/about>

Die documentatie werd verwerkt tot een heuse collectieve publicatie. ’Are You Being Served’ is een realisatie van de verslaggeefsters, Marloes De Valk, Madeleine Aktipy, Anne Laforet, An Mertens; met de enthousiaste steun van Michaela Lakova, Reni Hofmüller en Femke Snelting. Het ontwerp en de collectieve tool Ethertoff zijn van de hand van [Open Source Publishing](http://osp.kitchen/): Stephanie Vilayphiou en Eric Schrijver. 
De ideale werkomgeving hiervoor werd ons aangeboden door [De Pianofabriek KWP](http://www.pianofabriek.be/?moturl=1&lang=nl).



