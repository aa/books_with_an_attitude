title: 
Promiscuous Pipelines
----
year: 
2016
----
category: Session de Travail
----
link: https://gitlab.constantvzw.org/promiscuouspipelines/pppp
----
summary: Dans le monde du logiciel libre, le terme "pipe" est utilisé pour indiquer un procédé dans lequel plusieures modules sont connectés les uns aux autres. Les résultats d’un logiciel sont traités comme l’entrée du programme suivant. Cette publication suit la séance de travail Promiscuous Pipelines que Constant a organisée en 2015 avec FoAM, dans laquelle nous avons utilisé le principe de "pipe" pour établir des connections inhabituelles et peut-être atteindre le contraire d’un système axée sur la productivité.
----
text:

Dans le monde du logiciel libre, le terme "pipe" est utilisé pour indiquer un procédé dans lequel plusieures modules sont connectés les uns aux autres. Les résultats d’un logiciel sont traités comme l’entrée du programme suivant. Lors de la séance de travail Promiscuous Pipelines que Constant a organisée l’été dernier avec FoAM, nous avons utilisé le principe de "pipe" pour établir des connections inhabituelles et peut-être atteindre le contraire d’un système axée sur la productivité.
Bien sûr, nous avons abordé de façon similaire les réflexions, expériences et résultats issus de la séance de travail. La publication est l’hybride entre une publication mise en page et un dossier de fichiers. Une expérience en désordre, ou une rencontre entre des logiciels sur mesure, des prises de notes, du contenu brut et la rédaction collective.

**Aves des contributions de:**Active Archives, Agustina Andreoletti, An Mertens, Anne Laforet, Bash, bolwerK, Christoph Haag, Dymaxion, Eleanor Saitta, Etherpash, Etherpad, Femke Snelting, Flickr, ffmpeg, Foam, Gijs de Heij, Gottfried Haider, ImageMagick, Marthe Van Dessel, Martino Morandi, Michael Murtaugh, Microraptor/GEGL, Nik Gaffney, OpenCV, Øyvind Kolås, Pandoc, Pierre Marchand, Python, QueerOS, Simon Yuill, Sophie Toupin, Sophie Toupin, Unitary Networking, Wænd, Wendy Van Wijnsberghe, YouTube, Zeljko Blace et pleins d'autres personnes, outils et ressources.
*Avec le soutien de: FoAM*





