---
title: "Promiscuous Pipelines"
---

<div class="year">2016</div>
<div class="link"><a class="link" href="https://gitlab.constantvzw.org/promiscuouspipelines/pppp">https://gitlab.constantvzw.org/promiscuouspipelines/pppp</a></div>
<div class="category">Worksession</div>

Promiscuous Pipelines {lang=en}
===========================
<div class="summary">
In computation, a ’pipe’ is a method that enables various software modules to connect to each other, where the output of one program is treated as the input for the next program. This publication follows the Promiscuous Pipelines worksession that Constant organised with FoAM in 2015, in which we used the principle of ’pipes’ to inspire uncommon junctions and perhaps achieve the opposite of a productivity-oriented systems.
</div>

<div class="text">
In computation, a ’pipe’ is a method that enables various software modules to connect to each other, where the output of one program is treated as the input for the next program. At the Promiscuous Pipelines worksession that Constant organised with FoAM in 2015 we used the principle of ’pipes’ to inspire uncommon junctions and perhaps achieve the opposite of a productivity-oriented systems.
The publication of reflections, experiments and other outcomes of this session was of course done in the same spirit. This publication is a hybrid between a formatted, laid-out publication and a folder of ’stuff’, an untidy experiment linking up bespoke software, live notes, raw materials and collaborative editing.

**With contributions by:**Active Archives, Agustina Andreoletti, An Mertens, Anne Laforet, Bash, bolwerK, Christoph Haag, Dymaxion, Eleanor Saitta, Etherpash, Etherpad, Femke Snelting, Flickr, ffmpeg, Foam, Gijs de Heij, Gottfried Haider, ImageMagick, Marthe Van Dessel, Martino Morandi, Michael Murtaugh, Microraptor/GEGL, Nik Gaffney, OpenCV, Øyvind Kolås, Pandoc, Pierre Marchand, Python, QueerOS, Simon Yuill, Sophie Toupin, Sophie Toupin, Unitary Networking, Wænd, Wendy Van Wijnsberghe, YouTube, Zeljko Blace and many more people, tools and resources.
*With the support of: FoAM*
</div>

Promiscuous Pipelines {lang=fr}
===========================
<div class="summary">
Dans le monde du logiciel libre, le terme "pipe" est utilisé pour indiquer un procédé dans lequel plusieures modules sont connectés les uns aux autres. Les résultats d’un logiciel sont traités comme l’entrée du programme suivant. Cette publication suit la séance de travail Promiscuous Pipelines que Constant a organisée en 2015 avec FoAM, dans laquelle nous avons utilisé le principe de "pipe" pour établir des connections inhabituelles et peut-être atteindre le contraire d’un système axée sur la productivité.
</div>

<div class="text">
Dans le monde du logiciel libre, le terme "pipe" est utilisé pour indiquer un procédé dans lequel plusieures modules sont connectés les uns aux autres. Les résultats d’un logiciel sont traités comme l’entrée du programme suivant. Lors de la séance de travail Promiscuous Pipelines que Constant a organisée l’été dernier avec FoAM, nous avons utilisé le principe de "pipe" pour établir des connections inhabituelles et peut-être atteindre le contraire d’un système axée sur la productivité.
Bien sûr, nous avons abordé de façon similaire les réflexions, expériences et résultats issus de la séance de travail. La publication est l’hybride entre une publication mise en page et un dossier de fichiers. Une expérience en désordre, ou une rencontre entre des logiciels sur mesure, des prises de notes, du contenu brut et la rédaction collective.

**Aves des contributions de:**Active Archives, Agustina Andreoletti, An Mertens, Anne Laforet, Bash, bolwerK, Christoph Haag, Dymaxion, Eleanor Saitta, Etherpash, Etherpad, Femke Snelting, Flickr, ffmpeg, Foam, Gijs de Heij, Gottfried Haider, ImageMagick, Marthe Van Dessel, Martino Morandi, Michael Murtaugh, Microraptor/GEGL, Nik Gaffney, OpenCV, Øyvind Kolås, Pandoc, Pierre Marchand, Python, QueerOS, Simon Yuill, Sophie Toupin, Sophie Toupin, Unitary Networking, Wænd, Wendy Van Wijnsberghe, YouTube, Zeljko Blace et pleins d'autres personnes, outils et ressources.
*Avec le soutien de: FoAM*
</div>

Promiscuous Pipelines {lang=nl}
===========================
<div class="summary">
In software wordt de term ’pipe’ gebruikt om een methode aan te duiden waarbij verschillende modules met elkaar worden verbonden. De uitvoer van één programma wordt behandeld als de input van het volgende programma. Deze publicatie volgt op de de werksessie Promiscuous Pipelines die Constant in 2015 samen met FoAM organiseerde, en waarin we het principe van ’pipes’ gebruikten om ongewone verbindingen te maken en misschien wel het tegenovergestelde te bereiken van een op productiviteit georiënteerd systeem.
</div>

<div class="text">
In software wordt de term ’pipe’ gebruikt om een methode aan te duiden waarbij verschillende modules met elkaar worden verbonden. De uitvoer van één programma wordt behandeld als de input van het volgende programma. Tijdens de werksessie Promiscuous Pipelines die Constant de afgelopen zomer samen met FoAM organiseerde, gebruikten we het principe van ’pipes’ om ongewone verbindingen te maken en misschien wel het tegenovergestelde te bereiken van een op productiviteit georiënteerd systeem.
Uiteraard benaderden we de reflecties, experimenten en resultaten van deze werksessie vanuit dezelfde methode. De publicatie is een hybride tussen een opgemaakte, gedrukte publicatie en een map met ’files’. Een onopgeruimd experiment waarbij op maat gemaakte software, live-notities, ruwe inhoud en collectieve redactie samenkomen.

**Met bijdragen van:**Active Archives, Agustina Andreoletti, An Mertens, Anne Laforet, Bash, bolwerK, Christoph Haag, Dymaxion, Eleanor Saitta, Etherpash, Etherpad, Femke Snelting, Flickr, ffmpeg, Foam, Gijs de Heij, Gottfried Haider, ImageMagick, Marthe Van Dessel, Martino Morandi, Michael Murtaugh, Microraptor/GEGL, Nik Gaffney, OpenCV, Øyvind Kolås, Pandoc, Pierre Marchand, Python, QueerOS, Simon Yuill, Sophie Toupin, Sophie Toupin, Unitary Networking, Wænd, Wendy Van Wijnsberghe, YouTube, Zeljko Blace en veel andere mensen, gereedschappen en bronnen.
*Met de steun van: FoAM*
</div>
