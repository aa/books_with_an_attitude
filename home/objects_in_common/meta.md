---
title: "Peggy: An Object in Common"
---

<div class="year">2016</div>

<div class="category">Worksession</div>

Peggy: An Object in Common {lang=en}
===========================
<div class="summary">
Peggy is a publication and a piece of furniture of which you find the plans in the publication. Here she is presented with reflections on the fablab and maker culture that were gathered during the collective residencies in Madrid in 2015-2016.
</div>

<div class="text">
**Download the publication:** http://constantvzw.org/documents/oic/Peggy.publication.pdf

**Printed copy: 5€ (excl shipping costs)**
**Order a riso-printed physical copy by sending an email to info@constantvzw.org**

From October 2015 until December 2016 a worksession, an exhibition and lots more ran under the header of *Objects in Common*. The fablab and maker culture, conversion of digital objects to the physical realm, and the issues that come along - from geopolitical to licensing to infrastructure - were part of this research thread.
All of this was condensed into a publication.
</div>

Peggy: Un Objet en Commun {lang=fr}
===========================
<div class="summary">
Peggy est une publication et un meuble, dont vous trouvez les plans dans la même publication. Elle est accompagnée de questions et de réflexions autour de la culture des fablabs et de la communauté des makers, collectionnées lors de résidences collectives à Madrid en 2015 et 2016.
</div>

<div class="text">
**Téléchargez la publication :** http://constantvzw.org/documents/oic/Peggy.publication.pdf

**Exemplaire imprimé : 5€ (excl frais de livraison)**
**Achetez un exemplaire imprimé en riso en envoyant un e-mail à info@constantvzw.org**

D’octobre 2015 à décembre 2016 une session de travail, des expositions et beaucoup d’autres activités ont eu lieu sous l’entête *Objets en Commun*. La culture des fablabs et de la communauté des makers, la conversion d’objets du numérique au physique et toutes les questions qui tournent autour - de la géopolitique jusqu’aux licences et les infrastructures - faisaient partie de ce projet de recherche.
Tout cela a été condensé dans une publication.
</div>

Peggy: Een Object als Gemeengoed {lang=nl}
===========================
<div class="summary">
Peggy is een publicatie en een meubel, waarvan je de plannen vindt in dezelfde publicatie. Peggy krijgt hier gezelschap van vragen en bedenkingen rond de fablab- en makercultuur, verzameld tijdens collectieve residenties in Madrid in 2015 en 2016.
</div>

<div class="text">
**Download de publicatie**: http://constantvzw.org/documents/oic/Peggy.publication.pdf

**Gedrukt exemplaar: 5€ (excl verzendingskosten)**
**Zin in een risoprint exemplaar? Stuur dan een e-mail naar info@constantvzw.org**

Van oktober 2015 tot en met december 2016 had er een werksessie, een tentoonstelling en nog veel meer plaats onder de noemer van *Objecten als Gemeengoed*. Fablab- en maker-cultuur, de vertaling van digitale objecten naar de fysieke wereld en de mogelijke problematieken daaromtrent - van het geopolitieke tot licenties tot infrastructuur -, maakten deel uit van dit onderzoek.
De resultaten werden gedistilleerd in een publicatie.
</div>
