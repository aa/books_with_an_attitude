---
title: "Woorden uit de Berenkuil / Mots de la Cage aux Ours"
---

<div class="year">2012</div>
<div class="link"><a class="link" href="http://lalangueschaerbeekoise.be">http://lalangueschaerbeekoise.be</a></div>
<div class="category">Projects</div>

Woorden uit de Berenkuil / Mots de la Cage aux Ours {lang=en}
===========================
<div class="summary">
A collection of texts, images and spoken words used by different groups around the Place Verboekhoven, aka La Cage aux Ours. The words are also part of the online sound dictionary of La Langue Schaerbeekoise/De Schaerbeekse Taal, a project that took place under the Contrat de Quartier Durable Navez-Portaels between 2009 and 2012.
</div>

<div class="text">
**Download the book**: http://deschaarbeeksetaal.be/media/livre_boek.pdf
**English version of the texts**: http://deschaarbeeksetaal.be/media/translation.pdf

**Printed copy: 12€ (excl shipping costs)**
**Interested to order a copy? Please email info@constantvzw.org**

*Mots de la Cage aux Ours/Woorden uit de Berenkuil* is a collection of spoken words used by different groups around the Place Verboekhoven, aka La Cage aux Ours. The words are also part of the online sound dictionary of La Langue Schaerbeekoise / De Schaerbeekse Taal, a project that took place under the Contrat de Quartier Durable Navez-Portaels between 2009 and 2012. The project aimed to promote social cohesion between different communities while reflecting the linguistic wealth around the Cage aux Ours.

Arabic, Turkish, Berber, Dutch, Brussels, Swahili, Spanish, Polish are just a selection of the variety of languages that season the French, the current language of the neighborhood. The Dictionary of Language Schaerbeekoise fed on these influences, it is open, flexible and organic. The selection of words was based on the intuition of the three artists coordinators of the project: Clémentine Delahaut, Peter Westenberg and An Mertens.

With contributions by: Isabelle Doucet (urbanist), Fatima Zibouh (politologue), Myriam Stoffen (Zinneke Parade), Judith Van Istendael (comic artist), In Koli Jean Bofane (writer), Jérémie Piolat (philosopher), Milady Renoir (writer, performer), Jamal Youssfi (Compagnie des Nouveaux Disparus & festival Mimouna). 
Lay-out: OSP, Ludivine Loiseau, Alexandre Leray
Images: Peter Westenberg, Pablo Castilla, Clémentine Delahaut
Maps: Pierre Marchand

ISBN: 978-9-0811-4590-9

*With the support of: La Région de Bruxelles-Capitale and la Commune de Schaerbeek*
</div>

Woorden uit de Berenkuil / Mots de la Cage aux Ours {lang=fr}
===========================
<div class="summary">
Une collection de textes et images, et un dictionnaire hors du commun qui reprend des mots de la langue parlée dans le quartier de la Place Verboekhoven à Schaerbeek entre 2009 et 2012.
</div>

<div class="text">
**Téléchargez le livre**: http://deschaarbeeksetaal.be/media/livre_boek.pdf
**Exemplaire imprimé : 12€ (excl frais de livraison)**
**Envie de commander un exemplaire ? Merci d'envoyer un e-mail à info@constantvzw.org**

*Mots de la Cage aux Ours/Woorden uit de Berenkuil* est un dictionnaire hors du commun qui reprend des mots de la langue parlée dans le quartier de la Place Verboekhoven à Schaerbeek. 
Le choix des mots a été dicté par l’intuition de trois artistes de Constant – Clémentine Delahaut, Peter Westenberg, An Mertens - lors de leurs rencontres avec des habitants du quartier entre 2009 et 2012, dans le cadre du Contrat de Quartier Navez-Portaels. Chaque mot est accompagné de la phrase enregistrée où le mot a été entendu pour la première fois, et la plupart des mots – pas tous – sont expliqués à l’aide d’une définition apportée par la personne qui nous a offert le mot. Malgré l’intention de créer un dictionnaire, la nature humaine, les bons récits et le déroulement de l’entretien l’ont souvent emporté sur la forme, ce qui fait de Tsjehiler ! Mots de la Cage aux Ours une curiosité dans le genre des dictionnaires. Vendim?

Avec des contributions de: Isabelle Doucet (urbaniste), Fatima Zibouh (politologue), Myriam Stoffen (Zinneke Parade), Judith Van Istendael (dessinatrice BD), In Koli Jean Bofane (écrivain), Jérémie Piolat (philosophe), Milady Renoir (écrivaine, performer), Jamal Youssfi (Compagnie des Nouveaux Disparus & festival Mimouna). 
Mise-en-page: OSP, Ludivine Loiseau, Alexandre Leray
Images: Peter Westenberg, Pablo Castilla, Clémentine Delahaut
Cartes: Pierre Marchand

ISBN: 978-9-0811-4590-9

*Avec le support de: La Région de Bruxelles-Capitale and la Commune de Schaerbeek*
</div>

Woorden uit de Berenkuil / Mots de la Cage aux Ours {lang=nl}
===========================
<div class="summary">
Een verzameling van teksten, beelden en een woordenboek met woorden die tussen 2009 en 2012 werden uitgesproken door bewoners in de buurt van het Verboekhovenplein in Schaarbeek.
</div>

<div class="text">
**Download het boek**: http://deschaarbeeksetaal.be/media/livre_boek.pdf
**Gedrukt exemplaar: 12€ (excl verzendingskosten)**
**Zin om  een exemplaar te bestellen? Stuur dan een email naar info@constantvzw.org**

*Mots de la Cage aux Ours/Woorden uit de Berenkuil* is een verzameling van woorden uit de spreektaal op en rond het Verboekhovenplein in Schaarbeek. Geen nostalgie naar het Brussels dialect, maar een weerspiegeling van de taalkundige mengelmoes die er leeft.
De keuze van de woorden werd bepaald door de intuïtie van drie kunstenaars van Constant – Clémentine Delahaut, Peter Westenberg, An Mertens - tijdens ontmoetingen met bewoners en gebruikers van de Berenkuil tussen 2009 en 2012 in het kader van het Wijkcontract Navez-Portaels. Voor elk woord is de zin opgenomen waarin het woord voor het eerst werd gehoord. Bij de meeste woorden vind je ook een definitie, zoals ze werd gegeven door de bewoner. Niet bij alle woorden: ondanks de intentie om een woordenboek te maken, wonnen de menselijke aard, de sappige verhalen en de flow van het gesprek het vaak van de vorm. Dat maakt van het boek ook een curiosum in het genre van het woordenboek. Vendim? Pipas! 

Met bijdragen van: Isabelle Doucet (urbaniste), Fatima Zibouh (politologe), Myriam Stoffen (Zinneke Parade), Judith Van Istendael (striptekenaar), In Koli Jean Bofane (schrijver), Jérémie Piolat (filosoof), Milady Renoir (schrijfster, performer), Jamal Youssfi (Compagnie des Nouveaux Disparus & festival Mimouna). 
Lay-out: OSP, Ludivine Loiseau, Alexandre Leray
Beelden: Peter Westenberg, Pablo Castilla, Clémentine Delahaut
Kaarten: Pierre Marchand

ISBN: 978-9-0811-4590-9

*Met de steun van: Het Brussels Hoofdstedelijk Gewest en de gemeente Schaarbeek*
</div>
