---
title: "Puerto Cookbook"
---

<div class="year">2011</div>
<div class="link"><a class="link" href="http://ospublish.constantvzw.org/puerto">http://ospublish.constantvzw.org/puerto</a></div>
<div class="category">Projects</div>

Puerto Cookbook {lang=en}
===========================
<div class="summary">
Puerto is a hospitable place in the centre of Brussels where homeless people can find help if they want to live independently. Every Monday they are welcome to come and eat a three course meal, prepared by a volunteer, at an affordable price. This book collects more than 70 easy, delicious and cheap recipes that are the delights of the volunteers and Puerto guests.
</div>

<div class="text">
Puerto is a hospitable place in the centre of Brussels where homeless people can find help if they want to live independently. Every Monday they are welcome to come and eat a three course meal, prepared by a volunteer, at an affordable price. 

Together with these volunteers and Puerto guests, [Open Source Publishing](http://osp.kitchen/) wrote, designed, translated and illustrated a precious cookbook with more than 70 easy, delicious and cheap recipes. 
All benefits went to Puerto.
</div>

Puerto Livre de Cuisine {lang=fr}
===========================
<div class="summary">
Puerto est un centre d’accueil au coeur de Bruxelles pour les sans domicile fixe. Chaque lundi, les occupants peuvent manger un repas composé de trois plats, préparé par un bénévole, à un prix abordable. Ce livre reprend plus de 70 recettes faciles, délicieuses et bon marché, une sélection faite par les bénévoles et les habitués.
</div>

<div class="text">
Puerto est un centre d’accueil au coeur de Bruxelles pour les sans domicile fixe. Chaque lundi, les occupants peuvent manger un repas composé de trois plats, préparé par un bénévole, à un prix abordable.

Avec certains d’entre eux, [Open Source Publishing](http://osp.kitchen/) a écrit, mis en page, traduit et illustré un singulier livre de plus de 70 recettes faciles, délicieuses et bon marché.
Tous les bénéfices sont reversés à Puerto.
</div>

Puerto Kookboek {lang=nl}
===========================
<div class="summary">
Puerto is een gastvrije plek in het centrum van Brussel waar thuislozen kunnen aankloppen als ze zelfstandig willen wonen. Elke maandagavond eten de bewoners er een driegangenmaaltijd die door een vrijwilliger voor een budgetprijs wordt gekookt. Dit boek verzamelt meer dan 70 gemakkelijke, lekkere en voordelige recepten, een selectie van vrijwilligers en Puerto-gasten.
</div>

<div class="text">
Puerto is een gastvrije plek in het centrum van Brussel waar thuislozen kunnen aankloppen als ze zelfstandig willen wonen. Elke maandagavond eten de bewoners er een driegangenmaaltijd die door een vrijwilliger voor een budgetprijs wordt gekookt. 

Samen met hen schreef, ontwierp, vertaalde en illustreerde [Open Source Publishing](http://osp.kitchen/) een bijzonder kookboek.
Een boek met meer dan 70 gemakkelijke, lekkere en voordelige recepten.
De opbrengst kwam natuurlijk ten goede aan Puerto.
</div>
