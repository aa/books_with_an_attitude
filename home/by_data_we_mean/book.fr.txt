title: 
By Data We Mean
----
year: 
2011
----
category: Verbindingen/Jonctions
----
link: http://vj12.constantvzw.org
----
summary: *By Data We Mean* est une publication en ligne suite à l’édition de [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). Réalisée par Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray et Gijs de Heij.
----
text:

*By Data We Mean* est une publication en ligne suite à l’édition de [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). Réalisée par Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray et Gijs de Heij.

Cette publication rassemble les différents textes qui ont été produits pour les conférences et les présentations de V/J12. Elle est aussi une plateforme pour de nouvelles expériences. Quelles possibilités adviennent lorsque des textes sont publiés sous une licence libre ? Que se passe-t-il lorsque deux textes apparemment très différents l’un de l’autre, mais qui révèlent de nombreux points de contact à une deuxième lecture, sont soudain mis en relation, mixés, pétris l’un dans l’autre et acquièrent une nouvelle forme et un sens nouveau ? Chaque texte n'est-il pas ambigu et ouvert à une réinterprétation ? Et comment la littérature grise qui encadre la production textuelle influence sa lecture et son décodage ?


