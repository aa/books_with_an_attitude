---
title: "By Data We Mean"
---

<div class="year">2011</div>
<div class="link"><a class="link" href="http://vj12.constantvzw.org">http://vj12.constantvzw.org</a></div>
<div class="category">Verbindingen/Jonctions</div>

By Data We Mean {lang=en}
===========================
<div class="summary">
*By Data We Mean* is an online publication following [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). A creation by Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray and Gijs de Heij.
</div>

<div class="text">
*By Data We Mean* is an online publication following [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). A creation by Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray and Gijs de Heij.

This publication collects the different texts that were produced for the lectures and presentation of VJ12. It also constitutes a platform for experiments. What becomes possible when texts are released under a free license? What happens when two texts apparently very different from each other, but with many points of contact at a second reading, are brought together, mixed, smashed, and suddenly acquire a completely different form and meaning? Isn’t every text ambiguous and open to different interpretations? And how can the grey literature surrounding textual production influence the process of reading and decoding it?
</div>

By Data We Mean {lang=fr}
===========================
<div class="summary">
*By Data We Mean* est une publication en ligne suite à l’édition de [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). Réalisée par Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray et Gijs de Heij.
</div>

<div class="text">
*By Data We Mean* est une publication en ligne suite à l’édition de [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). Réalisée par Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray et Gijs de Heij.

Cette publication rassemble les différents textes qui ont été produits pour les conférences et les présentations de V/J12. Elle est aussi une plateforme pour de nouvelles expériences. Quelles possibilités adviennent lorsque des textes sont publiés sous une licence libre ? Que se passe-t-il lorsque deux textes apparemment très différents l’un de l’autre, mais qui révèlent de nombreux points de contact à une deuxième lecture, sont soudain mis en relation, mixés, pétris l’un dans l’autre et acquièrent une nouvelle forme et un sens nouveau ? Chaque texte n'est-il pas ambigu et ouvert à une réinterprétation ? Et comment la littérature grise qui encadre la production textuelle influence sa lecture et son décodage ?
</div>

By Data We Mean {lang=nl}
===========================
<div class="summary">
*By Data We Mean* is een online publicatie als resultaat van [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). Een creatie van Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray en Gijs de Heij.
</div>

<div class="text">
*By Data We Mean* is een online publicatie als resultaat van [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). Een creatie van Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray en Gijs de Heij. 

De publicatie verzamelt teksten die geschreven zijn voor lezingen en presentaties tijdens V/J12. Het is ook een experiment met het potentieel van teksten die onder een vrije licentie worden gepubliceerd. Wat gebeurt er wanneer twee teksten, die op het eerste gezicht erg verschillend zijn, maar tijdens een tweede lezing een reeks gemeenschappelijke elementen vertonen, worden samengebracht en gemixed, en een heel verschillende vorm en betekenis krijgen? Is niet elke tekst ambiguë en open voor interpretatie? En hoe kan de grijze literatuur die tekstproductie omringt, het leesproces beïnvloeden en decoderen?
</div>
