title: 
By Data We Mean
----
year: 
2011
----
category: Verbindingen/Jonctions
----
link: http://vj12.constantvzw.org
----
summary: *By Data We Mean* is an online publication following [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). A creation by Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray and Gijs de Heij.
----
text:

*By Data We Mean* is an online publication following [Verbindingen/Jonctions 12](http://constantvzw.org/vj12/). A creation by Donatella Portoghese, Nicolas Malevé, Stephanie Villayphiou, Alex Leray and Gijs de Heij.

This publication collects the different texts that were produced for the lectures and presentation of VJ12. It also constitutes a platform for experiments. What becomes possible when texts are released under a free license? What happens when two texts apparently very different from each other, but with many points of contact at a second reading, are brought together, mixed, smashed, and suddenly acquire a completely different form and meaning? Isn’t every text ambiguous and open to different interpretations? And how can the grey literature surrounding textual production influence the process of reading and decoding it?


