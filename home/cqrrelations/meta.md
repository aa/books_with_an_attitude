---
title: "Cqrrelations"
---

<div class="year">2016</div>
<div class="link"><a class="link" href="http://www.cqrrelations.constantvzw.org/0x0/">http://www.cqrrelations.constantvzw.org/0x0/</a></div>
<div class="category">Worksession</div>

Cqrrelations {lang=en}
===========================
<div class="summary">
This publication is an attempt to collect the different stages of the experiments and the encounters that happened at the collective residency Cqrrelations in January 2015, in a way that stays consistent with the techniques and the approaches involved in the event itself. For this reason the publication is mainly consisting of the autoindex function of the Apache webserver, which by default lists all the files inside a web folder.
</div>

<div class="text">
This website collects all kinds of traces and materials produced before, during and after the Cqrrelations worksession, organized by Constant in 2015 in Bruxelles. 
Cqrrelations invited data travellers, writers, numbergeeks, programmers, artists, mathematicians, storytellers, and other tech creative souls to explore the world of digital non-relations, desanalysis, blurry categorisations and crummylations in the Big Data that shapes our daily reality and language.

This notebook is an aggregation of different types of content from the server(s) running during the worksession: the content of the file server where files were shared in-between participants, re-ordered by project; the "dump" of the Etherpad installation used to take notes during the worksession; the sound recordings of different presentations that happened during the two weeks; and the video recording of 'Big Data & Discrimination', the public event organised in collaboration with [Vlaams Nederlands Huis deBuren](http://deburen.eu/) and [CPDP](http://www.cpdpconferences.org/).

This publication is an attempt to collect the different stages of the experiments and the encounters that happen at a collective residency, in a way that stays consistent with the techniques and the approaches involved in the event itself. For this reason the website is mainly consisting of the autoindex function of the Apache webserver, which by default lists all the files inside a web folder.
</div>

Cqrrelations {lang=fr}
===========================
<div class="summary">
Cette publication vise à montrer les différentes phases d'expérimentations et de rencontres qui ont eu lieu lors de la résidence collective Cqrrelations en janvier 2015, et ceci, dans la mesure du possible, en accord avec les technologies et méthodes qui ont été utilisées lors de l'événement. Voilà pourquoi la publication consiste en grande partie de la fonction d'auto-indexation du serveur web Apache, qui organise tous les documents par défaut dans un dossier.
</div>

<div class="text">
Ce site web est une collection de traces et matériaux qui ont été produits avant, pendant et après Cqrrelations, la session de travail organisé par Constant en 2015 à deBuren à Bruxelles. 
Lors de cette résidence Constant a invité des explorateurs de données, des écrivains, des amateurs de chiffres, des programmeurs, des artistes, des mathématiciens, des conteurs et d’autres âmes techno-créatives à venir découvrir le monde des non-relations digitales, des désanalyses, des catégorisations floues et d’autre crummylations présentes dans les Big Data qui donnent de plus en plus de forme à nos réalités et languages quotidiens.

Ce cahier de notes donne un aperçu des différents types de contenus localisés sur le(s) serveur(s) pendant la session de travail: le dossier utilisé par les participants pour partager leurs documents, organisé par projet; le "dump" de l'installation Etherpad qui a été utilisée pour prendre des notes lors de la session de travail; des enregistrements sonores des multiples présentations; et des enregistrements vidéo de 'Big Data & Discrimination', l'événement public organisé en collaboration avec [Vlaams Nederlands Huis deBuren](http://deburen.eu/) et [CPDP](http://www.cpdpconferences.org/). 

Cette publication vise à montrer les différentes phases d'expérimentations et de rencontres qui ont lieu lors d'une résidence collective, et ceci, dans la mesure du possible, en accord avec les technologies et méthodes qui ont été utilisées lors de l'événement. Voilà pourquoi le site web consiste en grande partie de la fonction d'auto-indexation du serveur web Apache, qui organise tous les documents par défaut dans un dossier.
</div>

Cqrrelations {lang=nl}
===========================
<div class="summary">
Deze publicatie toont de verschillende stadia van experimenten en ontmoetingen die plaatsvonden tijdens de collectieve residentie Cqrrelaties in januari 2015. Ze gebruikt zoveel mogelijk de technieken en methodes die gebruikt zijn tijdens het evenement. Daarom bestaat de publicatie vooral uit de autoindex-functie van de Apache webserver, die alle documenten standaard in een webfolder rangschikt.
</div>

<div class="text">
Deze website is een verzameling van sporen en materiaal dat aangemaakt werd voor, tijdens en na Cqrrelaties, een werksessie die Constant in 2015 organiseerde in deBuren in Brussel. 
Cqrrelaties nodigde datareizigers uit, schrijvers, cijfergeeks, programmeurs, kunstenaars, wiskundigen, verhalenvertellers en andere techcreatieve duizendpoten voor een ontdekkingsreis langs de non-relaties, desanalyses, vage categoriseringen en crymmylaties in de Big Data die onze dagdagelijkse omgeving en taal mee vormgeven. 

Dit notitieboek geeft een overzicht van de verschillende soorten inhoud die zich op de server(s) bevond tijdens de werksessie: de folder waarin deelnemers documenten deelden, georganiseerd per project; de "dump" van de Etherpad installatie die werd gebruikt om aantekeningen te maken tijdens de werksessie; geluidsopnames van de verschillende presentaties die plaatsvonden tijdens die week; en de video-opnames van 'Big Data & Discrimination', het publieke evenment georganiseerd in samenwerking met [Vlaams Nederlands Huis deBuren](http://deburen.eu/) en [CPDP](http://www.cpdpconferences.org/).

Deze publicatie toont de verschillende stadia van experimenten en ontmoetingen die plaatsvinden tijdens een collectieve residentie, en dit zoveel mogelijk in overeenstemming met de technieken en methodes die gebruikt zijn tijdens het evenement. Daarom bestaat de website vooral uit de autoindex-functie van de Apache webserver, die alle documenten standaard in een webfolder rangschikt.
</div>
