---
title: "Cyberfeminisme / Cyberféminisme"
---

<div class="year">2017</div>
<div class="link"><a class="link" href="http://cyberf.constantvzw.org/book/">http://cyberf.constantvzw.org/book/</a></div>
<div class="category">Projects</div>

Cyberfeminisme / Cyberféminisme {lang=en}
===========================
<div class="summary">
At the occasion of Donna Haraway visiting Brussels in March 2017, Constant made a facsimile edition of the bilingual (NL/FR) publication that was launched in 2001.
</div>

<div class="text">
**Download facsimile [51MB]**: http://video.constantvzw.org/Cyberfeminisme/cyberfeminisme.pdf
**Download materials**: http://video.constantvzw.org/Cyberfeminisme/

"This is a book of translations - because translation permits dissemination - of certain selected texts which seem to us to pose important questions, and which enable us to conceptualise our actions and our position in the future, in technology, in society, and in the network. Questions such as: what is feminism in the age of technology? How do we position ourselves in relation to biotechnology? What action can we take against globalisation? How do we gauge the impact of our creative work - of our images, liberated from the nostalgic baggage of the words "auteur" and "oeuvre", etc." (introduction, 2001)

At the occasion of Donna Haraway visiting Brussels in March 2017, Constant made a facsimile edition of the bilingual (NL/FR) publication that was launched in 2001.
</div>

Cyberfeminisme / Cyberféminisme {lang=fr}
===========================
<div class="summary">
A l’occasion de la visite de Donna Haraway à Bruxelles en mars 2017, Constant a réalisé une édition fac-similé de la publication bilingue publiée en 2001.
</div>

<div class="text">
**Téléchargez le fac-similé [51MB] :** http://video.constantvzw.org/Cyberfeminisme/cyberfeminisme.pdf
**Téléchargez les materiaux :** http://video.constantvzw.org/Cyberfeminisme/

"Ce livre est un livre de traductions. Traduire c’est-à-dire transmettre certain textes, des textes qui nous semblaient poser les questions, créer les images de notre action et de notre position dans l’avenir, dans la technologie, dans la société, dans le réseau : - qu’est ce que le féminisme à l’ère de la technologie ?, - comment nous positionnons-nous par rapport à la biotechnologie ?, - quelles peuvent être nos actions contre la globalisation ?, - quelle est la force de nos créations ?, de nos images hors de la nostalgie de l’auteur et de l’œuvre ?, etc." (introduction, 2001)

A l’occasion de la visite de Donna Haraway à Bruxelles en mars 2017, Constant a réalisé une édition fac-similé de la publication bilingue publiée en 2001.
</div>

Cyberfeminisme / Cyberféminisme {lang=nl}
===========================
<div class="summary">
Ter gelegenheid van het bezoek van Donna Haraway aan Brussel in maart 2017 maakte Constant een facsimile-editie van de tweetalige publicatie (NL/FR) die werd uitgegeven in 2001.
</div>

<div class="text">
**Download facsimile [51MB]**: http://video.constantvzw.org/Cyberfeminisme/cyberfeminisme.pdf
**Download materiaal**: http://video.constantvzw.org/Cyberfeminisme/

"Dit is een boek met vertalingen. Vertalen in de betekenis van het samen- en overbrengen van teksten, die volgens ons relevante vragen stellen en beelden creëren van onze werkzaamheden en onze positie in de toekomst, binnen de maatschappij, binnen het netwerk/ Wat is de betekenis van het feminisme in het technologische tijdperk? Welke houding nemen wij aan ten opzichte van de biotechnologie? Welke acties kunnen wij ondernemen om de toenemende globalisatie tegen te gaan? Waarin situeert zich de kracht van onze creaties? Van onze beelden zonder de nostalgische notie van de auteur en zijn werk? enz." (inleiding, 2001)

Ter gelegenheid van het bezoek van Donna Haraway aan Brussel in maart 2017 maakte Constant een facsimile-editie van de tweetalige publicatie (NL/FR) die werd uitgegeven in 2001.
</div>
