title: 
Cyberfeminisme / Cyberféminisme
----
year: 
2017
----
category: Projets
----
link: http://cyberf.constantvzw.org/book/
----
summary: A l’occasion de la visite de Donna Haraway à Bruxelles en mars 2017, Constant a réalisé une édition fac-similé de la publication bilingue publiée en 2001.
----
text:

**Téléchargez le fac-similé [51MB] :** http://video.constantvzw.org/Cyberfeminisme/cyberfeminisme.pdf
**Téléchargez les materiaux :** http://video.constantvzw.org/Cyberfeminisme/

"Ce livre est un livre de traductions. Traduire c’est-à-dire transmettre certain textes, des textes qui nous semblaient poser les questions, créer les images de notre action et de notre position dans l’avenir, dans la technologie, dans la société, dans le réseau : - qu’est ce que le féminisme à l’ère de la technologie ?, - comment nous positionnons-nous par rapport à la biotechnologie ?, - quelles peuvent être nos actions contre la globalisation ?, - quelle est la force de nos créations ?, de nos images hors de la nostalgie de l’auteur et de l’œuvre ?, etc." (introduction, 2001)

A l’occasion de la visite de Donna Haraway à Bruxelles en mars 2017, Constant a réalisé une édition fac-similé de la publication bilingue publiée en 2001.








