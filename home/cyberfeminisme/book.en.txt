title: 
Cyberfeminisme / Cyberféminisme
----
year: 
2017
----
category: Projects
----
link: http://cyberf.constantvzw.org/book/
----
summary: At the occasion of Donna Haraway visiting Brussels in March 2017, Constant made a facsimile edition of the bilingual (NL/FR) publication that was launched in 2001.
----
text:

**Download facsimile [51MB]**: http://video.constantvzw.org/Cyberfeminisme/cyberfeminisme.pdf
**Download materials**: http://video.constantvzw.org/Cyberfeminisme/

"This is a book of translations - because translation permits dissemination - of certain selected texts which seem to us to pose important questions, and which enable us to conceptualise our actions and our position in the future, in technology, in society, and in the network. Questions such as: what is feminism in the age of technology? How do we position ourselves in relation to biotechnology? What action can we take against globalisation? How do we gauge the impact of our creative work - of our images, liberated from the nostalgic baggage of the words "auteur" and "oeuvre", etc." (introduction, 2001)

At the occasion of Donna Haraway visiting Brussels in March 2017, Constant made a facsimile edition of the bilingual (NL/FR) publication that was launched in 2001.








