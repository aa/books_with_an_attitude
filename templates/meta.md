---
title: "{{title_en}}"
---

{% if year %}<div class="year">{{year}}</div>{% endif %}
{% if link %}<div class="link"><a class="link" href="{{link}}">{{link}}</a></div>{% endif %}
{% if category %}<div class="category">{{category}}</div>{% endif %}

{{title_en}} {lang=en}
===========================
<div class="summary">
{{summary_en}}
</div>

<div class="text">
{{text_en}}
</div>

{{title_fr}} {lang=fr}
===========================
<div class="summary">
{{summary_fr}}
</div>

<div class="text">
{{text_fr}}
</div>

{{title_nl}} {lang=nl}
===========================
<div class="summary">
{{summary_nl}}
</div>

<div class="text">
{{text_nl}}
</div>